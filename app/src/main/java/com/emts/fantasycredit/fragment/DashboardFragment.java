package com.emts.fantasycredit.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.DashboardCardSwipeAdapter;
import com.emts.fantasycredit.adapter.TeamCardSwipeAdapter;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.yuyakaido.android.cardstackview.CardStackView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Admin on 3/21/2018.
 */

public class DashboardFragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    CoordinatorLayout fixtureHolder;
    ProgressBar progressBar;
    TextView errorText;
    DashboardCardSwipeAdapter adapter;
    ArrayList<Object> gameList;
    String upcomingList, liveList, pastList;
    Spinner spinCategory;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress_bar);
        errorText = view.findViewById(R.id.error_text);
        fixtureHolder = view.findViewById(R.id.lay_fixture);

        viewPager = view.findViewById(R.id.view_pager);

        tabLayout = view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        spinCategory = view.findViewById(R.id.spin_category);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_view, getResources().getStringArray(R.array.fantacySports));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinCategory.setAdapter(spinnerArrayAdapter);

        setImageTask(view, null);

        if (NetworkUtils.isInNetwork(getActivity())) {
            getFixtureTask();
        } else {
            errorText.setText(R.string.error_no_internet);
            errorText.setVisibility(View.VISIBLE);
        }
    }

    private void getFixtureTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        JSONObject postParam = new JSONObject();
        volleyHelper.addVolleyRequestListeners(Api.getInstance().getFixtureUrl, Request.Method.GET, postParam,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONObject embeddedObj = res.getJSONObject("_embedded");
                            JSONArray fixtureArray = embeddedObj.getJSONArray("fixtures");
                            JSONObject fixtureObj = fixtureArray.getJSONObject(2);

                            JSONArray upComingArray = fixtureObj.getJSONArray("upcoming");
                            upcomingList = upComingArray.toString();
                            JSONArray liveArray = fixtureObj.getJSONArray("live");
                            liveList = liveArray.toString();
                            JSONArray pastArray = fixtureObj.getJSONArray("past");
                            pastList = pastArray.toString();
                            addTabs(viewPager);
                            fixtureHolder.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            Logger.e("getfixturetask ex", e.getMessage() + " ");
                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);

                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getFixtureTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "getFixtureTask");
    }


    private void addTabs(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        Fragment fragment = new UpcomingLiveFinishedFrag();
        Bundle bundle = new Bundle();
        bundle.putString("tab_name", "upcoming");
        bundle.putString("upcomingList", upcomingList);
        fragment.setArguments(bundle);

        Fragment fragment1 = new UpcomingLiveFinishedFrag();
        Bundle bundle1 = new Bundle();
        bundle1.putString("tab_name", "live");
        bundle1.putString("live", liveList);
        fragment1.setArguments(bundle1);

        Fragment fragment2 = new UpcomingLiveFinishedFrag();
        Bundle bundle2 = new Bundle();
        bundle2.putString("tab_name", "past");
        bundle2.putString("past", pastList);
        fragment2.setArguments(bundle2);


        adapter.addFrag(fragment, "Upcoming");
        adapter.addFrag(fragment1, "Live");
        adapter.addFrag(fragment2, "Finished");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setImageTask(View view, final ArrayList<String> images1) {
        final SliderLayout imageSlider = view.findViewById(R.id.slider);
        final PagerIndicator pagerIndicator = view.findViewById(R.id.custom_indicator);

        imageSlider.stopAutoCycle();
        imageSlider.setVisibility(View.GONE);

        final ArrayList<Integer> images = new ArrayList<>();
        images.add(R.drawable.matches_banner);
        for (int i = 0; i < images.size(); i++) {
            DefaultSliderView defaultSlider = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSlider
                    .image(images.get(i))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop);

            imageSlider.addSlider(defaultSlider);
        }

        if (images.size() > 1) {
            // play from first image, when all images loaded
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        imageSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                        imageSlider.setCustomIndicator(pagerIndicator);
//                        imageSlider.setCurrentPosition(0, false);
//                        imageSlider.setCurrentPosition(0, false); //this hangs and gives ANR
                        //so instead of going to first go to last and move to next
                        imageSlider.setCurrentPosition(images.size() - 1, false);
                        imageSlider.moveNextPosition();
                        pagerIndicator.onPageSelected(0);

                        imageSlider.setDuration(4000);
                        imageSlider.setVisibility(View.VISIBLE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    imageSlider.startAutoCycle();
                                } catch (Exception e) {
                                }
                            }
                        }, 4400);
                    } catch (Exception e) {
                        Logger.e("reflection ex", e.getMessage() + " ");
                    }
                }
            }, 900);

        } else {
            imageSlider.setVisibility(View.VISIBLE);
            pagerIndicator.setVisibility(View.GONE);
            imageSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
            imageSlider.stopAutoCycle();
        }
    }
}
