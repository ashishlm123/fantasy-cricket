package com.emts.fantasycredit.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.GameAdapterNew;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.model.Game;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class UpcomingLiveFinishedFrag extends Fragment {
    RecyclerView gameListView;
    GameAdapterNew adapter;
    ArrayList<Game> gameList;
    ProgressBar progressBar;
    TextView errorText;
    String upcomingList, liveList, pastList;
    Bundle bundle;
    Handler mainHandler;
    ScheduledExecutorService service;
    LinearLayoutManager linearLayoutManager;
    long serverTime;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        bundle = this.getArguments();
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainHandler = new Handler(Looper.getMainLooper());
        service = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gameList = new ArrayList<>();

        progressBar = view.findViewById(R.id.progress_bar);
        errorText = view.findViewById(R.id.tv_error);
        gameListView = view.findViewById(R.id.recycle_list);
        adapter = new GameAdapterNew(getActivity(), gameList);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        gameListView.setLayoutManager(linearLayoutManager);
        gameListView.setAdapter(adapter);
        gameListView.setItemAnimator(null);
        gameListView.setNestedScrollingEnabled(false);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        String currentDate = sdf.format(new Date());
        try {
            Date startTime = sdf.parse(currentDate);
            serverTime = startTime.getTime();
            adapter.setServerTime(serverTime);
        } catch (ParseException e) {
            Logger.e("date ex", e.getMessage() + " ");
        }


        if (bundle != null) {
            String tabName = bundle.getString("tab_name");
            assert tabName != null;
            switch (tabName) {
                case "upcoming":
                    upcomingList = bundle.getString("upcomingList");

                    try {
                        JSONArray upcomingArray = new JSONArray(upcomingList);
                        if (upcomingArray.length() == 0) {
                            errorText.setText("No upcoming matches at the moment");
                            errorText.setVisibility(View.VISIBLE);
                            gameListView.setVisibility(View.GONE);
                        } else {
                            for (int i = 0; i < upcomingArray.length(); i++) {
                                JSONObject eachObj = upcomingArray.getJSONObject(i);
                                Game gameMdl = new Game();
                                gameMdl.setLeagueId(eachObj.getString("leagueid"));
                                gameMdl.setFixtureId(eachObj.getString("fixture_id"));
                                gameMdl.setTeam1name(eachObj.getString("teamaname"));
                                gameMdl.setTeam1Logo(eachObj.getString("teamalogo"));
                                gameMdl.setTeam2name(eachObj.getString("teambname"));
                                gameMdl.setTeam2Logo(eachObj.getString("teamblogo"));
                                gameMdl.setLeagueDate(eachObj.getString("live_in"));
                                gameMdl.setLeagueName(eachObj.getString("leauge"));
                                gameMdl.setTeam1ShortCode(eachObj.getString("teamshortcode_a"));
                                gameMdl.setTeam2ShortCode(eachObj.getString("teamshortcode_b"));
                                gameList.add(gameMdl);
                            }

//                            if (service != null && !service.isShutdown()) {
//                                service.scheduleAtFixedRate(runnable,
//                                        0, 1000, TimeUnit.MILLISECONDS);
//                            }
                            adapter.notifyDataSetChanged();
                            gameListView.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        Logger.e(" json ex", e.getMessage() + " ");
                    }
                    break;

                case "live":
                    adapter.setLive(true);
                    liveList = bundle.getString("live");
                    try {
                        JSONArray liveArray = new JSONArray(liveList);
                        if (liveArray.length() == 0) {
                            errorText.setText("No Live matches at the moment");
                            errorText.setVisibility(View.VISIBLE);
                            gameListView.setVisibility(View.GONE);
                        } else {
                            for (int i = 0; i < liveArray.length(); i++) {
                                JSONObject eachObj = liveArray.getJSONObject(i);
                                Game gameMdl = new Game();
                                gameMdl.setLeagueId(eachObj.getString("leagueid"));
                                gameMdl.setFixtureId(eachObj.getString("fixture_id"));
                                gameMdl.setTeam1name(eachObj.getString("teamaname"));
                                gameMdl.setTeam1Logo(eachObj.getString("teamalogo"));
                                gameMdl.setTeam2name(eachObj.getString("teambname"));
                                gameMdl.setTeam2Logo(eachObj.getString("teamblogo"));
                                gameMdl.setLeagueDate(eachObj.getString("live_in"));
                                gameMdl.setLeagueName(eachObj.getString("leauge"));
                                gameMdl.setTeam1ShortCode(eachObj.getString("teamshortcode_a"));
                                gameMdl.setTeam2ShortCode(eachObj.getString("teamshortcode_b"));
                                gameList.add(gameMdl);
                            }
                            adapter.notifyDataSetChanged();
                            gameListView.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        Logger.e(" json ex", e.getMessage() + " ");
                    }
                    break;

                case "past":
                    adapter.setFinished(true);
                    pastList = bundle.getString("past");
                    try {
                        JSONArray pastArray = new JSONArray(pastList);
                        if (pastArray.length() == 0) {
                            errorText.setText("No finished matches at the moment");
                            errorText.setVisibility(View.VISIBLE);
                            gameListView.setVisibility(View.GONE);
                        } else {
                            for (int i = 0; i < pastArray.length(); i++) {
                                JSONObject eachObj = pastArray.getJSONObject(i);
                                Game gameMdl = new Game();
                                gameMdl.setLeagueId(eachObj.getString("leagueid"));
                                gameMdl.setFixtureId(eachObj.getString("fixture_id"));
                                gameMdl.setTeam1name(eachObj.getString("teamaname"));
                                gameMdl.setTeam1Logo(eachObj.getString("teamalogo"));
                                gameMdl.setTeam2name(eachObj.getString("teambname"));
                                gameMdl.setTeam2Logo(eachObj.getString("teamblogo"));
                                gameMdl.setLeagueDate(eachObj.getString("live_in"));
                                gameMdl.setLeagueName(eachObj.getString("leauge"));
                                gameMdl.setTeam1ShortCode(eachObj.getString("teamshortcode_a"));
                                gameMdl.setTeam2ShortCode(eachObj.getString("teamshortcode_b"));
                                gameList.add(gameMdl);
                            }
                            adapter.notifyDataSetChanged();
                            gameListView.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        Logger.e(" json ex", e.getMessage() + " ");
                    }
                    break;
            }
        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            mainHandler.post(new Runnable() {
                final int firstVisible = linearLayoutManager.findFirstVisibleItemPosition();
                final int lastVisible = linearLayoutManager.findLastVisibleItemPosition();

                @Override
                public void run() {
                    serverTime = serverTime + 1000;


//                    adapter.setServerTime(serverTime);
                    for (int i = firstVisible; i <= lastVisible; i++) {
//                        adapter.notifyItemChanged(i);
                        View itemView = linearLayoutManager.findViewByPosition(i);
                        if (itemView != null) {
                            TextView tvTimer = itemView.findViewById(R.id.tv_day);
                            showRemainingTime(getTime(gameList.get(i).getLeagueDate()), tvTimer);
                        }

                    }
                }
            });
        }
    };

    private void showRemainingTime(long endDate, TextView tvDay) {
        if (endDate == 0) {
            return;
        }

        if (serverTime >= endDate) {
            return;
        }
        long elapsedTime = Math.abs(endDate - serverTime);

        long days, hours, minute, second;
        days = TimeUnit.MILLISECONDS.toDays(elapsedTime);
        elapsedTime -= TimeUnit.DAYS.toMillis(days);
        hours = TimeUnit.MILLISECONDS.toHours(elapsedTime);
        elapsedTime -= TimeUnit.HOURS.toMillis(hours);
        minute = TimeUnit.MILLISECONDS.toMinutes(elapsedTime);
        elapsedTime -= TimeUnit.MINUTES.toMillis(minute);
        second = TimeUnit.MILLISECONDS.toSeconds(elapsedTime);
        boolean last5minute = days == 0 && hours == 0 && minute < 5;

//        SpannableString text = new SpannableString("  " + (int) days + "day" + " : " + String.format("%02d", (int) hours) + "h"
//                + " : " + String.format("%02d", (int) minute) + "m" + " : "
//                + String.format("%02d", (int) second) + "s");
//        text.setSpan(new ImageSpan(getActivity(), R.drawable.ic_item_timer), 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
//        tvDay.setText(text);

        tvDay.setText((int) days + "day" + " : " + String.format("%02d", (int) hours) + "h"
                + " : " + String.format("%02d", (int) minute) + "m" + " : "
                + String.format("%02d", (int) second) + "s");

    }

    private long getTime(String endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date endTime = sdf.parse(endDate);
            return endTime.getTime();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (service != null && !service.isShutdown()) {
            service.scheduleAtFixedRate(runnable,
                    0, 1000, TimeUnit.MILLISECONDS);
        } else {
            service = null;
            service = Executors.newSingleThreadScheduledExecutor();
            service.scheduleAtFixedRate(runnable,
                    0, 1000, TimeUnit.MILLISECONDS);
        }
    }

    @Override
    public void onDestroy() {
        if (service != null) {
            service.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        if (service != null) {
            service.shutdown();
        }
        super.onDetach();
    }

    @Override
    public void onStop() {
        if (service != null) {
            service.shutdown();
        }
        super.onStop();
    }
}
