package com.emts.fantasycredit.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.emts.fantasycredit.model.Game;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.GameAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 3/21/2018.
 */

public class PublicLeagueFragment extends Fragment {
    RecyclerView gameListView;
    GameAdapter adapter;
    ArrayList<Game> gameList;
    boolean isLeague = false;
    boolean isUpcoming = false;
    ProgressBar progressBar;
    TextView errorText;
    String upcomingList, liveList, finishedList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            isLeague = bundle.getBoolean("is_league");
            isUpcoming = bundle.getBoolean("is_upcoming");
            if (isUpcoming) {
                upcomingList = bundle.getString("upcomingList");
            } else if (isLeague) {
                liveList = bundle.getString("liveList");
            } else {
                finishedList = bundle.getString("finishedList");
                Logger.e("finlist", finishedList + " ");
            }
        }
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gameList = new ArrayList<>();
        progressBar = view.findViewById(R.id.progress_bar);
        errorText = view.findViewById(R.id.tv_error);
        gameListView = view.findViewById(R.id.recycle_list);
        adapter = new GameAdapter(getActivity(), gameList);
        adapter.setMyLeague(isLeague);
        adapter.setUpComing(isUpcoming);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        gameListView.setLayoutManager(linearLayoutManager);
        gameListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        if (isUpcoming) {
            try {
                JSONArray upComingArray = new JSONArray(upcomingList);
                if (upComingArray.length() == 0) {
                    errorText.setText("No upcoming matches at the moment");
                    errorText.setVisibility(View.VISIBLE);
                    gameListView.setVisibility(View.INVISIBLE);
                } else {
                    gameListView.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                Logger.e(" upcoming ex", e.getMessage() + " ");
            }
        } else if (isLeague) {
            try {
                JSONArray liveArray = new JSONArray(liveList);
                if (liveArray.length() == 0) {
                    errorText.setText("No live matches at the moment");
                    errorText.setVisibility(View.VISIBLE);
                    gameListView.setVisibility(View.INVISIBLE);
                } else {
                    gameListView.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                Logger.e(" upcoming ex", e.getMessage() + " ");
            }
        } else {
            try {
                if (finishedList != null) {
                    JSONArray finishedArray = new JSONArray(finishedList);
                    if (finishedArray.length() == 0) {
                        errorText.setText("No finished match at the moment");
                        errorText.setVisibility(View.VISIBLE);
                        gameListView.setVisibility(View.INVISIBLE);
                    } else {
                        gameListView.setVisibility(View.VISIBLE);
                    }
                } else {
                    errorText.setText("No match at the moment");
                    errorText.setVisibility(View.VISIBLE);
                    gameListView.setVisibility(View.INVISIBLE);
                }
            } catch (JSONException e) {
                Logger.e(" upcoming ex", e.getMessage() + " ");
            }
        }


//        if (NetworkUtils.isInNetwork(getActivity())) {
//            getGameListTask();
//        } else {
//            errorText.setText(getString(R.string.error_no_internet));
//            errorText.setVisibility(View.VISIBLE);
//
//        }


    }


}
