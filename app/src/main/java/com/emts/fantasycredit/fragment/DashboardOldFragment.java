//package com.emts.fantasycredit.fragment;
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentPagerAdapter;
//import android.support.v4.app.FragmentStatePagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.emts.fantasycredit.R;
//import com.emts.fantasycredit.adapter.DashboardCardSwipeAdapter;
//import com.emts.fantasycredit.adapter.TeamCardSwipeAdapter;
//import com.emts.fantasycredit.helper.Logger;
//import com.yuyakaido.android.cardstackview.CardStackView;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by Admin on 3/21/2018.
// */
//
//public class DashboardOldFragment extends Fragment {
//    private TabLayout tabLayout;
//    private ViewPager viewPager;
//    DashboardCardSwipeAdapter adapter;
//    ArrayList<Object> gameList;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.fragment_dashboard, container, false);
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        viewPager = view.findViewById(R.id.view_pager);
//        addTabs(viewPager);
//
//        tabLayout = view.findViewById(R.id.tabs);
//        tabLayout.setupWithViewPager(viewPager);
//
//        gameList = new ArrayList<>();
//        gameList.add(new Object());
//        gameList.add(new Object());
//        gameList.add(new Object());
//        gameList.add(new Object());
//        gameList.add(new Object());
//        gameList.add(new Object());
//        gameList.add(new Object());
//        adapter = new DashboardCardSwipeAdapter(getActivity(), gameList);
//
//        CardStackView cardStackView = view.findViewById(R.id.card_stack_view);
//        cardStackView.setAdapter(adapter);
//    }
//
//
//    private void addTabs(ViewPager viewPager) {
//        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
//        adapter.addFrag(new PublicLeagueFragment(), "Public League");
//        adapter.addFrag(new PublicLeagueFragment(), "Private League");
//        viewPager.setAdapter(adapter);
//    }
//
//    class ViewPagerAdapter extends FragmentStatePagerAdapter {
//        private final List<Fragment> mFragmentList = new ArrayList<>();
//        private final List<String> mFragmentTitleList = new ArrayList<>();
//
//        public ViewPagerAdapter(FragmentManager manager) {
//            super(manager);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            return mFragmentList.get(position);
//        }
//
//        @Override
//        public int getCount() {
//            return mFragmentList.size();
//        }
//
//        public void addFrag(Fragment fragment, String title) {
//            mFragmentList.add(fragment);
//            mFragmentTitleList.add(title);
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            return mFragmentTitleList.get(position);
//        }
//    }
//
//
//}
