package com.emts.fantasycredit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.emts.fantasycredit.R;

public class InviteCodeActivity extends AppCompatActivity {
    boolean isRegister;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_code);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        Button btnInviteCode = findViewById(R.id.btn_enter_code);
        final EditText etInviteCode = findViewById(R.id.et_invite_code);

        final Intent intent = getIntent();

        if (intent != null) {
            isRegister = intent.getBooleanExtra("isRegister", false);

            if (isRegister) {
                toolbarTitle.setText("Enter Code");
                btnInviteCode.setText("Enter Code");
            } else {
                toolbarTitle.setText("Invite Code");
                btnInviteCode.setText("Join Contest");
            }
        }

        btnInviteCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inviteCode = etInviteCode.getText().toString().trim();
                TextView tvErrText = findViewById(R.id.tv_error_text);
                if (!TextUtils.isEmpty(inviteCode)) {
                    tvErrText.setVisibility(View.GONE);
                    if (isRegister) {
                        Intent intent1 = new Intent();
                        intent1.putExtra("inviteCode", inviteCode);
                        setResult(RESULT_OK, intent1);
                        onBackPressed();
                    } else {

                    }
                } else {
                    tvErrText.setVisibility(View.VISIBLE);
                }

            }
        });

    }
}
