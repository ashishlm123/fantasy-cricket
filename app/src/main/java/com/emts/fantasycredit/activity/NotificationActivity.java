package com.emts.fantasycredit.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.NotificationAdapter;
import com.emts.fantasycredit.model.NotificationModel;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity {
    NotificationAdapter todaysNotAdapter, olderNotAdapter;
    ArrayList<NotificationModel> todaysNotLists, oldNotLists;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView pageTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        pageTitle.setText("Your Notifications");

        todaysNotLists = new ArrayList<>();
        oldNotLists = new ArrayList<>();

        RecyclerView rvTodaysNotLists, rvOldNotLists;

        rvTodaysNotLists = findViewById(R.id.todays_notification_listings);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        rvTodaysNotLists.setLayoutManager(linearLayoutManager1);
        todaysNotAdapter = new NotificationAdapter(getApplicationContext(), todaysNotLists);
        rvTodaysNotLists.setAdapter(todaysNotAdapter);
        rvTodaysNotLists.setNestedScrollingEnabled(false);

        rvOldNotLists = findViewById(R.id.older_notification_listings);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getApplicationContext());
        rvOldNotLists.setLayoutManager(linearLayoutManager2);
        olderNotAdapter = new NotificationAdapter(getApplicationContext(), oldNotLists);
        rvOldNotLists.setAdapter(olderNotAdapter);
        rvOldNotLists.setNestedScrollingEnabled(false);

    }
}
