package com.emts.fantasycredit.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.ReferralAdapter;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Config;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.emts.fantasycredit.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Admin on 3/26/2018.
 */

public class ReferralActivity extends AppCompatActivity {
    RecyclerView registeredListView, unRegisteredListView;
    ArrayList<User> referralList = new ArrayList<>();
    ReferralAdapter adapter;
    ProgressBar progressBar;
    TextView errorText;
    ScrollView referralHolder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referal);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Referral Tracking");
        progressBar = findViewById(R.id.progress_bar);
        errorText = findViewById(R.id.error_text);
        referralHolder = findViewById(R.id.lay_referral);
        registeredListView = findViewById(R.id.registered_referral_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        registeredListView.setLayoutManager(layoutManager);
        adapter = new ReferralAdapter(referralList, ReferralActivity.this);
        registeredListView.setAdapter(adapter);


        if (NetworkUtils.isInNetwork(ReferralActivity.this)) {
            getReferralTrackingTask();
        } else {
            errorText.setText(R.string.error_no_internet);
            errorText.setVisibility(View.GONE);
        }

    }

    private void getReferralTrackingTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(ReferralActivity.this);
        final JSONObject postParam = new JSONObject();
        try {
            postParam.put("userid", "4");
        } catch (JSONException e) {
            Logger.e(" getReferralTrackingTask json ex", e.getMessage() + " ");
        }
        vHelper.addVolleyRequestListeners(Api.getInstance().referralTrackingUrl, Request.Method.POST, postParam,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        Logger.printLongLog("res", response + " ");
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray userArray = res.getJSONArray("data");
                                for (int i = 0; i < userArray.length(); i++) {
                                    JSONObject eachObj = userArray.getJSONObject(i);
                                    User user = new User();
                                    user.setUserEmail(eachObj.getString("user_email"));
                                    user.setUserName(eachObj.getString("user_name"));
                                    user.setUserId(eachObj.getString("id"));

                                    // active=1 then verified and if active=0 then unverified
                                    if (eachObj.getString("active").equals("1")) {
                                        user.setStatus(Config.VERIFIED);
                                    } else if (eachObj.getString("active").equals("0")) {
                                        user.setStatus(Config.UNVERIFIED);
                                    }
                                    referralList.add(user);
                                }
                                adapter.notifyDataSetChanged();
                                registeredListView.setVisibility(View.VISIBLE);
                                referralHolder.setVisibility(View.VISIBLE);

                            } else {
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                                registeredListView.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            Logger.e("json ex", e.getMessage() + " ");
                        }


                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getReferralTrackingTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "getReferralTrackingTask");
    }
}
