package com.emts.fantasycredit.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.CaptainVCAdapter;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.PreferenceHelper;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.emts.fantasycredit.model.Game;
import com.emts.fantasycredit.model.LeagueModel;
import com.emts.fantasycredit.model.PlayerModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PickCapViceCapActivity extends AppCompatActivity {
    ArrayList<PlayerModel> playerList, selectedPlayers;
    String selPlayersIds = "";
    String selCapID = "";
    String selVCapId = "";
    Game gameMdl;
    LeagueModel leagueModel;
    Handler mainHandler;
    ScheduledExecutorService service;
    TextView tvTimer;
    PreferenceHelper prefsHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_pick_cap_vice_cap);
        service = Executors.newSingleThreadScheduledExecutor();
        mainHandler = new Handler(Looper.getMainLooper());

        Intent intent = getIntent();
        if (intent != null) {
            playerList = (ArrayList<PlayerModel>) intent.getSerializableExtra("playerList");
            gameMdl = (Game) intent.getSerializableExtra("game");
            leagueModel = (LeagueModel) intent.getSerializableExtra("league");
        }

        final Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Pick C & VC");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TextView tvTeams = findViewById(R.id.tv_game_team_country);
        tvTeams.setText(gameMdl.getTeam1ShortCode().toUpperCase() + " VS " + gameMdl.getTeam2ShortCode().toUpperCase());

        tvTimer = findViewById(R.id.tv_game_timer);

        if (service != null) {
            service.scheduleAtFixedRate(runnable,
                    0, 1000, TimeUnit.MILLISECONDS);
        }

        RecyclerView playerListView = findViewById(R.id.rv_team_players);
        playerListView.setLayoutManager(new LinearLayoutManager(PickCapViceCapActivity.this));
        playerListView.setNestedScrollingEnabled(false);


        final Button btnCreateTeam = findViewById(R.id.btn_create_team);
        btnCreateTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    createTeamTask();
                } else {
                    AlertUtils.showSnack(getBaseContext(), toolbar, getResources().getString(R.string.error_no_internet));
                }
            }
        });

        //temp arrays
        ArrayList<PlayerModel> wicketArray = new ArrayList<>();
        ArrayList<PlayerModel> batsmenArray = new ArrayList<>();
        ArrayList<PlayerModel> allRounderArray = new ArrayList<>();
        ArrayList<PlayerModel> bowlerArray = new ArrayList<>();
        for (int i = 0; i < playerList.size(); i++) {
            PlayerModel playerModel = playerList.get(i);
            if (playerModel.getIsSelected()) {
                if (playerModel.getPlayerType().equalsIgnoreCase(PlayerModel.BATSMAN)) {
                    batsmenArray.add(playerModel);
                } else if (playerModel.getPlayerType().equalsIgnoreCase(PlayerModel.BOWLER)) {
                    bowlerArray.add(playerModel);
                } else if (playerModel.getPlayerType().equalsIgnoreCase(PlayerModel.ALL_ROUNDER)) {
                    allRounderArray.add(playerModel);
                } else if (playerModel.getPlayerType().equalsIgnoreCase(PlayerModel.WICKET_KEEPER)) {
                    wicketArray.add(playerModel);
                }
            }
        }

        selectedPlayers = new ArrayList<>();
        //use a dummy playerModel with id -9803681065 as player id to indicate the item view type
        // player type header
        //wicket keepers
        PlayerModel wicketHeaderLayout = new PlayerModel();
        wicketHeaderLayout.setPlayerId(CaptainVCAdapter.TYPE_HEADER_DECIDER);
        wicketHeaderLayout.setPlayerType(PlayerModel.WICKET_KEEPER);
        selectedPlayers.add(wicketHeaderLayout);
        selectedPlayers.addAll(wicketArray);
        //batsman
        PlayerModel batsmanHeaderLayout = new PlayerModel();
        batsmanHeaderLayout.setPlayerId(CaptainVCAdapter.TYPE_HEADER_DECIDER);
        batsmanHeaderLayout.setPlayerType(PlayerModel.BATSMAN);
        selectedPlayers.add(batsmanHeaderLayout);
        selectedPlayers.addAll(batsmenArray);
        //all rounder
        PlayerModel allRounderHeaderLayout = new PlayerModel();
        allRounderHeaderLayout.setPlayerId(CaptainVCAdapter.TYPE_HEADER_DECIDER);
        allRounderHeaderLayout.setPlayerType(PlayerModel.ALL_ROUNDER);
        selectedPlayers.add(allRounderHeaderLayout);
        selectedPlayers.addAll(allRounderArray);
        //bowler
        PlayerModel bowlerHeaderLayout = new PlayerModel();
        bowlerHeaderLayout.setPlayerId(CaptainVCAdapter.TYPE_HEADER_DECIDER);
        bowlerHeaderLayout.setPlayerType(PlayerModel.BOWLER);
        selectedPlayers.add(bowlerHeaderLayout);
        selectedPlayers.addAll(bowlerArray);

        CaptainVCAdapter adapter = new CaptainVCAdapter(PickCapViceCapActivity.this, selectedPlayers);
        playerListView.setAdapter(adapter);

        adapter.setRecyclerViewItemClickListerner(new CaptainVCAdapter.RecyclerViewItemClickListerner() {
            @Override
            public void onRecyclerViewItemClickListener(View view, int pos) {
                boolean isCaptain = false, isViceCaptain = false;
                for (int i = 0; i < selectedPlayers.size(); i++) {
                    PlayerModel player = selectedPlayers.get(i);
                    if (player.getIsSelected()) {
                        selPlayersIds = selPlayersIds + player.getPlayerId() + ",";
                    }
                    if (player.isCaptain()) {
                        isCaptain = true;
                        selCapID = player.getPlayerId();
                    }
                    if (player.isVC()) {
                        isViceCaptain = true;
                        selVCapId = player.getPlayerId();
                    }
//                    if (isCaptain && isViceCaptain) {
//                        break;
//                    }
                }

                if (isCaptain && isViceCaptain) {
                    btnCreateTeam.setVisibility(View.VISIBLE);
                } else {
                    selPlayersIds = "";
                    btnCreateTeam.setVisibility(View.GONE);
                }
            }
        });

    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            mainHandler.post(new Runnable() {

                @Override
                public void run() {
                    showRemainingTime();
                }
            });
        }
    };


    public void createTeamTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(PickCapViceCapActivity.this, "Please Wait...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        JSONObject postParams = new JSONObject();
        Logger.e("selected player id", selPlayersIds.substring(0, selPlayersIds.length() - 1));

        try {
            postParams = new JSONObject("{\"league_type\" : \"Daily-Combined-Leagues\", \"league_players\" : \"437,438,455,148,152,453,440,151,443,163,460\", \"series_id\" : \"9\", \"fixture_id\" : \"59\", \"league_id\" : \"0\", \"league_cap\" : \"453\", \"league_vcap\" : \"438\",\"userid\":\"190\"}");
        } catch (JSONException ex) {
            Logger.e("cara sjsada ", ex.getMessage());
        }


//TODO real time implementation
//        try {
//            postParams.put("userid", prefsHelper.getUserId());
//            postParams.put("league_type", "Daily-Combined-Leagues");
//            postParams.put("league_players", selPlayersIds.substring(0, selPlayersIds.length() - 1));
//            postParams.put("series_id", "9");
//            postParams.put("fixture_id", gameMdl.getFixtureId());
//            postParams.put("league_id", leagueModel.getLeagueId());
//            postParams.put("league_cap", selCapID);
//            postParams.put("league_vcap", selVCapId);
//        } catch (JSONException ex) {
//            Logger.e("createTeamTask jsonparams ex", ex.getMessage());
//        }

        vHelper.addVolleyRequestListeners(Api.getInstance().createTeamUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        //TODO
                        AlertUtils.showAlertMessage(PickCapViceCapActivity.this, false, "Success", res.getString("message"),
                                "OK", "", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        if (isPositiveButton) {
                                            Intent intent = new Intent();
                                            intent.putExtra("isCreated", true);
                                            setResult(RESULT_OK, intent);
                                            onBackPressed();
                                        }
                                    }
                                });
                    } else {
                        //TODO
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                pDialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("createTeamTask error res", e.getMessage() + " ");
                }
                AlertUtils.showSnack(PickCapViceCapActivity.this, tvTimer,
                        errorMsg);
            }
        }, "createTeamTask");
    }

    private void showRemainingTime() {
        long serverTime = 0, endDate = 0;
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        String currentDate = sdf.format(new Date());
        try {
            Date startTime = sdf.parse(currentDate);
            serverTime = startTime.getTime();
            Date endTime = sdf.parse(gameMdl.getLeagueDate());
            endDate = endTime.getTime();
        } catch (ParseException e) {
            Logger.e("date ex", e.getMessage() + " ");
        }


        long elapsedTime = Math.abs(endDate - serverTime);

        long days, hours, minute, second;
        days = TimeUnit.MILLISECONDS.toDays(elapsedTime);
        elapsedTime -= TimeUnit.DAYS.toMillis(days);
        hours = TimeUnit.MILLISECONDS.toHours(elapsedTime);
        elapsedTime -= TimeUnit.HOURS.toMillis(hours);
        minute = TimeUnit.MILLISECONDS.toMinutes(elapsedTime);
        elapsedTime -= TimeUnit.MINUTES.toMillis(minute);
        second = TimeUnit.MILLISECONDS.toSeconds(elapsedTime);
        boolean last5minute = days == 0 && hours == 0 && minute < 5;

//        if (last5minute) {
//            tvHour.setTextColor(Color.RED);
//        } else {
//            tvHour.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//        }
        tvTimer.setText(String.format("%02d", (int) hours) + "h"
                + " : " + String.format("%02d", (int) minute) + "m" + " : " + String.format("%02d", (int) second) + "s");

    }

    @Override
    public void onDestroy() {
        if (service != null) {
            service.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onStop() {
        if (service != null) {
            service.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (service != null) {
            service.shutdown();
        }
        super.onBackPressed();
    }
}
