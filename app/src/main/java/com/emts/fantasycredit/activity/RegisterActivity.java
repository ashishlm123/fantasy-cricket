package com.emts.fantasycredit.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.PreferenceHelper;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin on 3/20/2018.
 */

public class RegisterActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    CallbackManager callbackManager;
    GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_SIGN_IN = 121;
    PreferenceHelper prefsHelper;
    Toolbar toolbar;

    int REQUEST_CODE = 1011;

    String inviteCode = "";
    boolean isRegisterPage;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefsHelper = PreferenceHelper.getInstance(getApplicationContext());
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        String fBId = prefsHelper.getString(PreferenceHelper.FB_APP_ID, getString(R.string.facebook_app_id));
        FacebookSdk.setApplicationId(fBId);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }

        setContentView(R.layout.activity_register);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Register & Play");

        final TextView errorFullName = findViewById(R.id.error_fullname);
        final EditText etEmail = findViewById(R.id.et_email);
        final EditText etFullName = findViewById(R.id.et_fullname);
        final EditText etPassword = findViewById(R.id.et_password);
        final EditText etConfirmPassword = findViewById(R.id.et_confirm_password);
        final TextView errorMail = findViewById(R.id.error_email);
        final TextView errorPassword = findViewById(R.id.error_password);
        final TextView errorConfirmPassword = findViewById(R.id.error_confirm_password);
        final Button btnRegister = findViewById(R.id.btn_register);
        final EditText etInviteCode = findViewById(R.id.et_invite_code);
        TextView tvTnc = findViewById(R.id.tv_tnc);
        final TextView tvLogin = findViewById(R.id.tv_login);
        LinearLayout btnFb = findViewById(R.id.lay_fb);
        LinearLayout btnGoogle = findViewById(R.id.lay_google);
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });
        tvTnc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), TncActivity.class));
            }
        });


        etPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                visiblePassword(etPassword, event);
                return false;
            }
        });

        etConfirmPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                visiblePassword(etConfirmPassword, event);
                return false;
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                boolean isValid = true;

                ColorStateList textColor = ((TextView) findViewById(R.id.tv_text)).getTextColors();

                if (TextUtils.isEmpty(etFullName.getText().toString())) {
                    isValid = false;
                    errorFullName.setText(R.string.empty_field);
                    errorFullName.setTextColor(getResources().getColor(R.color.appRedLight));
                    errorFullName.setVisibility(View.VISIBLE);
                } else {
                    errorFullName.setVisibility(View.GONE);
                }

                if (TextUtils.isEmpty(etEmail.getText().toString())) {
                    isValid = false;
                    errorMail.setText(R.string.empty_field);
                    errorMail.setTextColor(getResources().getColor(R.color.appRedLight));
                } else {

                    if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
                        isValid = false;
                        errorMail.setText(R.string.error_email);
                        errorMail.setTextColor(getResources().getColor(R.color.appRedLight));
                    } else {
                        errorMail.setText(R.string.no_spam_we_promise);
                        errorMail.setTextColor(textColor);
                    }
                }

                if (TextUtils.isEmpty(etPassword.getText().toString())) {
                    isValid = false;
                    errorPassword.setText(R.string.empty_field);
                    errorPassword.setTextColor(getResources().getColor(R.color.appRedLight));
                } else {
                    if (etPassword.getText().toString().trim().length() < 8) {
                        isValid = false;
                        errorPassword.setText(R.string.pass_length);
                        errorPassword.setTextColor(getResources().getColor(R.color.appRedLight));
                    } else {
                        if (!containAlphanumeric(etPassword.getText().toString().trim())) {
                            isValid = false;
                            errorPassword.setText(R.string.pasnm_alpha);
                            errorPassword.setTextColor(getResources().getColor(R.color.appRedLight));
                        } else {
                            errorPassword.setText(R.string.minimum_8_characters_with_1_number_symbol);
                            errorPassword.setTextColor(textColor);
                        }
                    }

                }
                if (TextUtils.isEmpty(etConfirmPassword.getText().toString())) {
                    isValid = false;
                    errorConfirmPassword.setText(R.string.empty_field);
                    errorConfirmPassword.setTextColor(getResources().getColor(R.color.appRedLight));
                } else {
                    if (!etPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
                        isValid = false;
                        errorConfirmPassword.setText(R.string.confrm_same);
                        errorConfirmPassword.setTextColor(getResources().getColor(R.color.appRedLight));
                    } else {
                        errorConfirmPassword.setText(R.string.confirm_your_secret_password);
                        errorConfirmPassword.setTextColor(textColor);
                    }
                }

                if (isValid) {
                    if (NetworkUtils.isInNetwork(RegisterActivity.this)) {
                        normalRegistrationTask(etFullName.getText().toString().trim(), etEmail.getText().toString().trim(),
                                etPassword.getText().toString(), etConfirmPassword.getText().toString().trim(), etInviteCode.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(RegisterActivity.this, tvLogin, getString(R.string.error_no_internet));
                    }
                } else {
                    AlertUtils.showSnack(RegisterActivity.this, tvLogin, getString(R.string.fill_req_info));
                }
            }
        });

        btnFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(RegisterActivity.this)) {
                    fbTask();
                } else {
                    AlertUtils.showSnack(RegisterActivity.this, tvLogin, getString(R.string.error_no_internet));
                }
            }
        });

        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(RegisterActivity.this)) {
                    googleSignIn();
                } else {
                    AlertUtils.showSnack(RegisterActivity.this, tvLogin, getString(R.string.error_no_internet));
                }
            }
        });
    }

    private void normalRegistrationTask(String fullName, String email, String password, String confirmPassword, String inviteCode) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(RegisterActivity.this, "Please wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(RegisterActivity.this);
        JSONObject postJson = new JSONObject();
        try {
            postJson.put("emailid", email);
            postJson.put("password", password);
            postJson.put("confirmpassword", confirmPassword);
            postJson.put("fullname", fullName);
            if (!TextUtils.isEmpty(inviteCode)) {
                postJson.put("invite_by", inviteCode);
            }

        } catch (JSONException e) {
            Logger.e("normalRegistrationTask jsonParams ex", e.getMessage() + " ");
        }
        vHelper.addVolleyRequestListeners(Api.getInstance().registrationUrl, Request.Method.POST,
                postJson, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showAlertMessage(RegisterActivity.this, "Success", res.getString("Message"),
                                        "OK", "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                if (isPositiveButton) {
                                                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    startActivity(intent);
                                                }
                                            }
                                        });
                            } else {
                                AlertUtils.showAlertMessage(RegisterActivity.this, "Error", res.getString("Message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("normalRegistrationTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("normalRegistrationTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(RegisterActivity.this, toolbar,
                                errorMsg);
                    }
                }, "normalRegistrationTask");

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void fbTask() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();

        LoginManager.getInstance().logInWithReadPermissions(RegisterActivity.this, (Arrays.asList("email",
                "public_profile")));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Logger.e("facebook login response", loginResult.toString() + " &&\n "
                                + loginResult.getAccessToken() + "\n" + loginResult.getRecentlyGrantedPermissions()
                                + "\n" + loginResult.getRecentlyDeniedPermissions() + "\n");

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
//                                        Logger.e("fbTask res", object + " ");
//                                        Logger.e("graph response error", response.getError().getErrorMessage() + "\n"
//                                                + response.getRequest() + "\n" + response.getJSONObject());
                                        // Application code
                                        try {
                                            String id = object.optString("id");
                                            String name = object.optString("name");
                                            String first_name = object.optString("first_name");
                                            String last_name = object.optString("last_name");
//                                            String city = object.getString("hometown");
//                                            String location = object.getJSONObject("location").getString("name");
                                            String age = object.optString("age_range");
                                            String picture = object.optString("picture");
                                            String email = object.optString("email"); //Get null value here
                                            String gender = object.optString("gender");


                                            Logger.e("Facebook Data", id + "\n" + name + "\n" + first_name
                                                    + "\n" + last_name + "\n" + email + "\n" + age + "\n" + picture);
                                            LoginManager.getInstance().logOut();

                                            String birthday = "";

                                            Intent intent = new Intent(getApplicationContext(), NavMainActivityV2.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
//
//                                            registerWithFbTask(id, name, email, birthday, false);

                                        } catch (Exception e) {
                                            LoginManager.getInstance().logOut();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,first_name,last_name,email,gender," +
                                "age_range");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Logger.e("cancell", "called");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Logger.e("error", exception.getMessage() + "\n" + exception.getCause());
                    }
                });

    }

    private void registerWithFbTask(final String fbId, final String fbFirst_name, final String fbEmail,
                                    final String fbBirthday, final boolean isRegister) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = new HashMap<>();
        postParams.put("id", fbId);
        postParams.put("email", fbEmail);
        postParams.put("birthday", fbBirthday);
        postParams.put("first_name", fbFirst_name);
        postParams.put("name", fbFirst_name);
        String pushToken = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(pushToken)) {
            postParams.put("push_id", pushToken);
        }


//        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, "Logging");
//        pDialog.show();

//        vHelper.addVolleyRequestListeners(Api.getInstance().fbRegisterUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
//            @Override
//            public void onSuccess(String response) {
//                pDialog.dismiss();
//                try {
//                    JSONObject res = new JSONObject(response);
//                    if (res.getBoolean("status")) {
//
//
//                    }
//                } catch (JSONException e) {
//                    Logger.e("loginTask json ex", e.getMessage() + "");
//                }
//            }
//
//            @Override
//            public void onError(String errorResponse, VolleyError error) {
//                pDialog.dismiss();
//
//                try {
//                    JSONObject errorObj = new JSONObject(errorResponse);
//                    Toast.makeText(getApplicationContext(), errorObj.getString("message"), Toast.LENGTH_LONG).show();
//                } catch (Exception e) {
//                    if (error instanceof NetworkError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof ServerError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof AuthFailureError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof ParseError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof TimeoutError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//        }, "registerWithFbTask");

    }

    // [START signOut]
    private void signOut() {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            // [START_EXCLUDE]
                            Logger.e("google signOut", status.getStatus().getStatusMessage() + " ** "
                                    + status.isSuccess());
                            // [END_EXCLUDE]
                        }
                    });
        }
    }


    private void googleSignIn() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, GOOGLE_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Logger.e("google sign in conn failed", connectionResult.getErrorMessage() + "");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.e("requestCode", String.valueOf(requestCode));
        Logger.e("resultCode", String.valueOf(resultCode));
        Logger.e("data", data + "");
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Logger.e("handelSignInResult", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            String userName = acct.getDisplayName();
            String id = acct.getId();
            String f_name = acct.getGivenName();
            String email = acct.getEmail();
            String birthday = "";

            Intent intent = new Intent(getApplicationContext(), NavMainActivityV2.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
//

//            registerWithGoogleTask(id, f_name, email, birthday, false);

//            Uri photoUri = acct.getPhotoUrl();
//            if (photoUri != null) {
//                intent.putExtra("google_pp", acct.getPhotoUrl().toString());
//            }


            signOut();
//            startActivity(intent);


        } else {
            // Signed out, show unauthenticated UI.
            signOut();
        }

    }

    private void registerWithGoogleTask(final String id, final String userName, final String email,
                                        String birthday, final boolean isRegister) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = new HashMap<>();
        postParams.put("id", id);
        postParams.put("email", email);
        postParams.put("birthday", birthday);
        postParams.put("first_name", userName);
        postParams.put("name", userName);
        String pushToken = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(pushToken)) {
            postParams.put("push_id", pushToken);
        }

        Intent intent = new Intent(getApplicationContext(), NavMainActivityV2.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, "Logging");
//        pDialog.show();

//        vHelper.addVolleyRequestListeners(Api.getInstance().googleRegisterUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
//            @Override
//            public void onSuccess(String response) {
//                pDialog.dismiss();
//                try {
//                    JSONObject res = new JSONObject(response);
//                    if (res.getBoolean("status")) {
//
//                    } else {
//
//                    }
//                } catch (JSONException e) {
//                    Logger.e("registerWithGoogleTask json ex", e.getMessage() + "");
//                }
//            }
//
//            @Override
//            public void onError(String errorResponse, VolleyError error) {
//                pDialog.dismiss();
//
//                try {
//                    JSONObject errorObj = new JSONObject(errorResponse);
//                    Toast.makeText(getApplicationContext(), errorObj.getString("message"), Toast.LENGTH_LONG).show();
//                } catch (Exception e) {
//                    if (error instanceof NetworkError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof ServerError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof AuthFailureError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof ParseError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof TimeoutError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//        }, "registerWithGoogleTask");

    }

    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(this).cancelRequest("registerWithFbTask");
        VolleyHelper.getInstance(this).cancelRequest("registerWithGoogleTask");
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(RegisterActivity.this);
        mGoogleApiClient.disconnect();
    }

    //checking if the password is alphanumeric or not
    public boolean containAlphanumeric(final String str) {
        Pattern pattern;
        Matcher matcher;
//        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[@#$%^&+=()!?<>';:|`~*/]).{5,13}$";
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z]).{5,13}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(str);

        return matcher.matches();
    }

    private void visiblePassword(EditText etPassword, MotionEvent event) {
        // set integer 0 , 1, 2, 3 for left, top, right and bottom respectively
        final int DRAWABLE_RIGHT = 2;

        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (event.getRawX() >= (etPassword.getRight() - etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                if (etPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }
                etPassword.setSelection(etPassword.getText().length());

            }
        }
    }

}
