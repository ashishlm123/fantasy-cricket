package com.emts.fantasycredit.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.PlayerDetailAdapter;
import com.emts.fantasycredit.adapter.PlayerStatisticsAdapter;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.emts.fantasycredit.model.PlayerModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PlayerActivity extends AppCompatActivity {
    PlayerModel player;
    Toolbar toolbar;

    PlayerStatisticsAdapter playerStatisticsAdapter;
    RecyclerView rvPlayerStatistics;
    ArrayList<PlayerModel> playerStatisticLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        String fixtureId = "";
        Intent intent = getIntent();
        if (intent != null) {
            player = (PlayerModel) intent.getSerializableExtra("player");
            fixtureId = intent.getStringExtra("fixtureId");
        }

        toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(player.getPlayerName());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        playerStatisticLists = new ArrayList<>();

        ImageView profileImg = findViewById(R.id.profile_icon);
        TextView tvCredits = findViewById(R.id.tv_credits);
        TextView tvTotalPoints = findViewById(R.id.tv_total_points);
        TextView tvBatType = findViewById(R.id.tv_bat_type);
        TextView tvFlagType = findViewById(R.id.tv_flag_type);
        TextView tvHandType = findViewById(R.id.tv_hand_type);
        TextView tvDob = findViewById(R.id.tv_date);

        rvPlayerStatistics = findViewById(R.id.player_points_history);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvPlayerStatistics.setLayoutManager(linearLayoutManager);
        playerStatisticsAdapter = new PlayerStatisticsAdapter(PlayerActivity.this, playerStatisticLists);
        rvPlayerStatistics.setAdapter(playerStatisticsAdapter);
        rvPlayerStatistics.setNestedScrollingEnabled(false);

        playerStatisticLists.add(player);
        playerStatisticLists.add(player);
        playerStatisticLists.add(player);
        playerStatisticLists.add(player);
        playerStatisticLists.add(player);
        playerStatisticLists.add(player);
        playerStatisticLists.add(player);
        playerStatisticLists.add(player);
        playerStatisticLists.add(player);
        playerStatisticLists.add(player);
        playerStatisticLists.add(player);
        playerStatisticsAdapter.notifyDataSetChanged();

        if (NetworkUtils.isInNetwork(getBaseContext())) {
            playerDetailTask(fixtureId);
        } else {
            AlertUtils.showSnack(getBaseContext(), toolbar, getResources().getString(R.string.error_no_internet));
        }
    }

    private void playerDetailTask(String fixtureId) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(PlayerActivity.this, "Please Wait...");
        pDialog.show();

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        JSONObject postParams = new JSONObject();

        try {
//            postParams.put("pid", player.getPlayerId());
//            postParams.put("team_id", player.getPlayingTeamId());
//            postParams.put("fixture_id", fixtureId);
//            postParams.put("series_id", 9);

            postParams.put("pid", "707");
            postParams.put("team_id", "6");
            postParams.put("fixture_id", "52");
            postParams.put("series_id", "9");
        } catch (JSONException ex) {
            Logger.e("playerDetailTask jsonparams ex: ", ex.getMessage());
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().playerStatistics, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.showSnack(PlayerActivity.this, toolbar, res.getString("message"));
                    } else {
                        AlertUtils.showSnack(PlayerActivity.this, toolbar, res.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("playerDetailTask json ex: ", e.getMessage());
                }

            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                pDialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("playerDetailTask error res", e.getMessage() + " ");
                }
                AlertUtils.showSnack(PlayerActivity.this, toolbar,
                        errorMsg);
            }
        }, "playerDetailTask");
    }

}
