package com.emts.fantasycredit.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.emts.fantasycredit.model.CheckableItem;
import com.emts.fantasycredit.adapter.LeagueFeeAdapter;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.TeamCardSwipeAdapter;
import com.yuyakaido.android.cardstackview.CardStackView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2018-03-21.
 */

public class CreateLeague extends AppCompatActivity {
    ArrayList<Object> gameList;
    TeamCardSwipeAdapter adapter;
    ArrayList<CheckableItem> feeList, leagueList;
    LinearLayout leagueHolder;
    TextView errorText;
    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_league);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView customToolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        customToolbarTitle.setText("Create Private League");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        feeList = new ArrayList<>();
        leagueList = new ArrayList<>();
        feeList.add(new CheckableItem());
        feeList.add(new CheckableItem());
        feeList.add(new CheckableItem());
        feeList.add(new CheckableItem());
        leagueList.addAll(feeList);

        gameList = new ArrayList<>();
        gameList.add(new Object());
        gameList.add(new Object());
        gameList.add(new Object());
        gameList.add(new Object());
        gameList.add(new Object());
        gameList.add(new Object());
        gameList.add(new Object());
        adapter = new TeamCardSwipeAdapter(this, gameList);

        CardStackView cardStackView = findViewById(R.id.card_stack_view);
        cardStackView.setAdapter(adapter);


        leagueHolder = findViewById(R.id.lay_league);
        progressBar = findViewById(R.id.progress_bar);
        errorText = findViewById(R.id.tv_error);

        RecyclerView rvFee = findViewById(R.id.recyclerFee);
        rvFee.setLayoutManager(new GridLayoutManager(this, 4));
        rvFee.setNestedScrollingEnabled(false);
        RecyclerView rvLeague = findViewById(R.id.recyclerGame);
        rvLeague.setNestedScrollingEnabled(false);
        rvLeague.setLayoutManager(new GridLayoutManager(this, 4));

        rvFee.setAdapter(new LeagueFeeAdapter(this, feeList));
        rvLeague.setAdapter(new LeagueFeeAdapter(this, leagueList));

        EditText tvLeagueName = findViewById(R.id.et_league_name);
        Button btnPostLeague = findViewById(R.id.btn_post_league);
        leagueHolder.setVisibility(View.VISIBLE);

//        if (NetworkUtils.isInNetwork(CreateLeague.this)) {
//            getLeagueDetailTask();
//        } else {
//            errorText.setText(getString(R.string.error_no_internet));
//            errorText.setVisibility(View.VISIBLE);
//        }

        btnPostLeague.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(CreateLeague.this)) {
                    postLeagueTask();
                } else {
                    AlertUtils.showSnack(CreateLeague.this, errorText, getString(R.string.error_no_internet));
                }
            }
        });

    }

    private void postLeagueTask() {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(CreateLeague.this, "Please wait");
        progressDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(CreateLeague.this);
        JSONObject postParam = new JSONObject();

        vHelper.addVolleyRequestListeners(Api.getInstance().createLeague, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {


                            } else {

                            }
                        } catch (JSONException e) {
                            Logger.e("postLeagueTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressDialog.dismiss();
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            Toast.makeText(getApplicationContext(), errorObj.getString("message"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }, "postLeagueTask");
    }

    private void getLeagueDetailTask() {
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(CreateLeague.this);
        JSONObject postParam = new JSONObject();

        vHelper.addVolleyRequestListeners(Api.getInstance().createLeague, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        leagueHolder.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {


                            } else {

                            }
                        } catch (JSONException e) {
                            Logger.e("getLeagueDetailTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorText.setText(errorObj.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                errorText.setText(getString(R.string.error_no_internet));
                            } else if (error instanceof ServerError) {
                                errorText.setText(getString(R.string.error_server));
                            } else if (error instanceof AuthFailureError) {
                                errorText.setText(getString(R.string.error_authFailureError));
                            } else if (error instanceof ParseError) {
                                errorText.setText(getString(R.string.error_parse_error));
                            } else if (error instanceof TimeoutError) {
                                errorText.setText(getString(R.string.error_time_out));
                            }
                            errorText.setVisibility(View.VISIBLE);
                        }
                    }
                }, "getLeagueDetailTask");

    }
}
