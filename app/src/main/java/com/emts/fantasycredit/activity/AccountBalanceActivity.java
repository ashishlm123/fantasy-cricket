package com.emts.fantasycredit.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.AccountTransactionHistoryAdapter;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.emts.fantasycredit.model.TransactionModel;
import com.emts.fantasycredit.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by User on 2018-03-22.
 */

public class AccountBalanceActivity extends AppCompatActivity {
    ArrayList<TransactionModel> transactionsLists;
    AccountTransactionHistoryAdapter adapter;
    LinearLayout layAccountHolder, layTransHistoryHolder;
    TextView errorText, tvTransErrortext, tvBalance, tvUnUtilized, tvWining, tvBonus;
    ProgressBar progressBar;

    RecyclerView rvTransHistoryListings;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_account_balance);

        transactionsLists = new ArrayList<>();

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView customToolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        customToolbarTitle.setText("Account Balance");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        tvBalance = findViewById(R.id.tv_balance);
        tvUnUtilized = findViewById(R.id.tv_unutilized);
        tvWining = findViewById(R.id.tvWinnings);
        tvBonus = findViewById(R.id.tv_bonus);
        Button btnVerify = findViewById(R.id.btnVerify);
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), VerifyAccountActivity.class));
            }
        });

        rvTransHistoryListings = findViewById(R.id.trans_history_listings);
        rvTransHistoryListings.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AccountTransactionHistoryAdapter(this, transactionsLists);
        rvTransHistoryListings.setAdapter(adapter);
        rvTransHistoryListings.setNestedScrollingEnabled(false);

        layAccountHolder = findViewById(R.id.lay_account);
        layTransHistoryHolder = findViewById(R.id.lay_trans_holder);

        progressBar = findViewById(R.id.progress_bar);
        errorText = findViewById(R.id.error_text);
        tvTransErrortext = findViewById(R.id.tv_err_no_transcations);

        Button btnAddCash = findViewById(R.id.btnAddCash);
        btnAddCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertUtils.showToast(AccountBalanceActivity.this, "TO:DO");
            }
        });

        if (NetworkUtils.isInNetwork(AccountBalanceActivity.this)) {
            getAccountDetailTask();
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }

    }

    private void getAccountDetailTask() {
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);
        layAccountHolder.setVisibility(View.GONE);
        layTransHistoryHolder.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(AccountBalanceActivity.this);
        JSONObject postParam = new JSONObject();
        try {
            postParam.put("userid", "4");
        } catch (JSONException ex) {
            Logger.e("getAccountDetailTask jsonparams ex", ex.getMessage());
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().accountBalanceInfoUrl, Request.Method.POST, postParam,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        layAccountHolder.setVisibility(View.VISIBLE);
                        layTransHistoryHolder.setVisibility(View.VISIBLE);
                        errorText.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject data = res.getJSONObject("data");
                                JSONObject userBasicDetail = data.getJSONObject("user_basic_detail");
                                User userModel = new User();
                                userModel.setUserId(userBasicDetail.getString("uid"));
                                userModel.setUserName(userBasicDetail.getString("user_name"));
                                String userImg = userBasicDetail.getString("user_image");
                                if (!TextUtils.isEmpty(userImg) && !userImg.equals("null")) {
                                    userModel.setUserImage(userImg);
                                }
                                userModel.setUserEmail(userBasicDetail.getString("user_email"));
                                userModel.setUserInviteCode(data.getString("invite_code"));

                                tvBalance.setText(getString(R.string.rupee_char) + " " + data.getString("total_balance"));
                                tvUnUtilized.setText(getString(R.string.rupee_char) + " " + data.getString("UnutilizedAmount"));
                                tvBonus.setText(getString(R.string.rupee_char) + " " + data.getString("UserReferralAmount"));
                                tvWining.setText(getString(R.string.rupee_char) + " " + data.getString("UserWinningAmount"));

                                JSONArray transArray = data.getJSONArray("transactionHistory");
                                if (transArray.length() == 0) {
                                    tvTransErrortext.setText("No transaction made yet.");
                                    layTransHistoryHolder.setVisibility(View.VISIBLE);
                                    tvTransErrortext.setVisibility(View.VISIBLE);
                                    rvTransHistoryListings.setVisibility(View.GONE);
                                } else {
                                    for (int i = 0; i < transArray.length(); i++) {
                                        JSONObject eachTransaction = transArray.getJSONObject(i);
                                        TransactionModel transModel = new TransactionModel();
                                        transModel.setTransId(eachTransaction.getString("transcation_id"));
                                        transModel.setUserId(eachTransaction.getString("uid"));
                                        transModel.setTransType(eachTransaction.getString("transcation_type"));
                                        transModel.setTransAmt(eachTransaction.getString("amount"));
                                        transModel.setTransMatchID(eachTransaction.getString("match_id"));
                                        transModel.setTransTime(eachTransaction.getString("transcation_time"));
                                        transModel.setTransGameType(eachTransaction.getString("game_type"));
                                        transModel.setUserBalance(eachTransaction.getString("balance"));
                                        transModel.setTransDebitCredit(eachTransaction.getString("transcation_type"));
                                        transModel.setTransLeagueID(eachTransaction.getString("league_id_for"));
                                        transModel.setTransLeagueName(eachTransaction.getString("leagues_Name"));
                                        transModel.setTransTypeText(eachTransaction.getString("transaction_type_txt"));

                                        transactionsLists.add(transModel);
                                    }
                                    adapter.notifyDataSetChanged();
                                    layTransHistoryHolder.setVisibility(View.VISIBLE);
                                    rvTransHistoryListings.setVisibility(View.VISIBLE);
                                    tvTransErrortext.setVisibility(View.GONE);
                                }

                            } else {

                            }
                        } catch (JSONException e) {
                            Logger.e("getAccountDetailTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        layAccountHolder.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getAccountDetailTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);

                    }
                }, "getAccountDetailTask");
    }

    private void addCashTask() {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(AccountBalanceActivity.this, "Please wait");
        progressDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(AccountBalanceActivity.this);
        JSONObject postParam = new JSONObject();


        vHelper.addVolleyRequestListeners("", Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {


                            } else {

                            }
                        } catch (JSONException e) {
                            Logger.e("postLeagueTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("postLeagueTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(AccountBalanceActivity.this, errorText,
                                errorMsg);
                    }

                }, "postLeagueTask");
    }
}
