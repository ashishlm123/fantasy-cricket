package com.emts.fantasycredit.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.MyTeamAdapter;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.emts.fantasycredit.model.Game;
import com.emts.fantasycredit.model.LeagueModel;
import com.emts.fantasycredit.model.Team;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MyTeamListActivity extends AppCompatActivity {
    ArrayList<Team> myTeams;
    Game gameMdl;
    LeagueModel leagueModel;
    Toolbar toolbar;

    LinearLayout teamHolder;
    RecyclerView rvTeamLists;
    TextView errorText, tvTimer;
    ProgressBar progressBar;
    MyTeamAdapter myTeamAdapter;


    Handler mainHandler;
    ScheduledExecutorService service;

    boolean isMyTeam;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myteam_list);
        service = Executors.newSingleThreadScheduledExecutor();
        mainHandler = new Handler(Looper.getMainLooper());

        final Intent intent = getIntent();
        if (intent != null) {
            gameMdl = (Game) intent.getSerializableExtra("game");
            leagueModel = (LeagueModel) intent.getSerializableExtra("league");
            isMyTeam = intent.getBooleanExtra("isMyTeamButton", false);
        } else {
            return;
        }
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("My Team");

        myTeams = new ArrayList<>();
        teamHolder = findViewById(R.id.team_holder);
        rvTeamLists = findViewById(R.id.my_team_list);
        errorText = findViewById(R.id.error_text);
        progressBar = findViewById(R.id.progress_bar);

        LinearLayout btnHolder = findViewById(R.id.btn_holder);
        Button btnCreateTeam = findViewById(R.id.btn_create_team);
        final Button btnJoinContest = findViewById(R.id.btn_join_contest);

        LinearLayoutManager layoutManager = new LinearLayoutManager(MyTeamListActivity.this);
        rvTeamLists.setLayoutManager(layoutManager);
        myTeamAdapter = new MyTeamAdapter(MyTeamListActivity.this, myTeams);
        rvTeamLists.setAdapter(myTeamAdapter);
        myTeamAdapter.setMyTeamButtonClicked(isMyTeam);
        rvTeamLists.setNestedScrollingEnabled(false);
        rvTeamLists.setItemAnimator(null);

        myTeams.add(new Team());
        myTeams.add(new Team());
        myTeams.add(new Team());
        myTeams.add(new Team());
        myTeams.add(new Team());
        myTeams.add(new Team());
        myTeams.add(new Team());
        myTeams.add(new Team());
        rvTeamLists.setVisibility(View.VISIBLE);
        myTeamAdapter.notifyDataSetChanged();

        if (!isMyTeam) {
            teamHolder.setVisibility(View.GONE);
            btnHolder.setVisibility(View.VISIBLE);
        } else {
            btnHolder.setVisibility(View.GONE);
            teamHolder.setVisibility(View.VISIBLE);
            TextView team1Name = findViewById(R.id.team1_name);
            TextView team2Name = findViewById(R.id.team2_name);
            team1Name.setText(gameMdl.getTeam1ShortCode().toUpperCase());
            team2Name.setText(gameMdl.getTeam2ShortCode().toUpperCase());

            tvTimer = findViewById(R.id.tv_timer);
            if (service != null) {
                service.scheduleAtFixedRate(runnable,
                        0, 1000, TimeUnit.MILLISECONDS);
            }
        }

        myTeamAdapter.setTeamSelectListener(new MyTeamAdapter.TeamSelectListener() {
            @Override
            public void isTeamSelected(int pos, boolean isSelected) {
                if (isSelected) {
                    btnJoinContest.setEnabled(true);
                    btnJoinContest.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                } else {
                    btnJoinContest.setEnabled(false);
                    btnJoinContest.setBackgroundColor(getResources().getColor(R.color.transparent_black));
                }
            }
        });

        btnCreateTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getApplicationContext(), CreateTeamActivity.class);
                intent1.putExtra("game", gameMdl);
                intent1.putExtra("league", leagueModel);
                startActivity(intent1);
            }
        });

        btnJoinContest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO join contest
                AlertUtils.showToast(MyTeamListActivity.this, "TO:DO");
            }
        });

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            getMyTeamListTask();
        } else {
            teamHolder.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            AlertUtils.showSnack(MyTeamListActivity.this, toolbar, getResources().getString(R.string.error_no_internet));
            errorText.setText(getResources().getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            mainHandler.post(new Runnable() {

                @Override
                public void run() {
                    showRemainingTime();
                }
            });
        }
    };

    private void showRemainingTime() {
        long serverTime = 0, endDate = 0;
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        String currentDate = sdf.format(new Date());
        try {
            Date startTime = sdf.parse(currentDate);
            serverTime = startTime.getTime();
            Date endTime = sdf.parse(gameMdl.getLeagueDate());
            endDate = endTime.getTime();
        } catch (ParseException e) {
            Logger.e("date ex", e.getMessage() + " ");
        }


        long elapsedTime = Math.abs(endDate - serverTime);

        long days, hours, minute, second;
        days = TimeUnit.MILLISECONDS.toDays(elapsedTime);
        elapsedTime -= TimeUnit.DAYS.toMillis(days);
        hours = TimeUnit.MILLISECONDS.toHours(elapsedTime);
        elapsedTime -= TimeUnit.HOURS.toMillis(hours);
        minute = TimeUnit.MILLISECONDS.toMinutes(elapsedTime);
        elapsedTime -= TimeUnit.MINUTES.toMillis(minute);
        second = TimeUnit.MILLISECONDS.toSeconds(elapsedTime);
        boolean last5minute = days == 0 && hours == 0 && minute < 5;

//        if (last5minute) {
//            tvHour.setTextColor(Color.RED);
//        } else {
//            tvHour.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//        }
        tvTimer.setText(String.format("%02d", (int) hours) + "h"
                + " : " + String.format("%02d", (int) minute) + "m" + " : " + String.format("%02d", (int) second) + "s");

    }

    private void getMyTeamListTask() {
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);
        VolleyHelper vHelper = VolleyHelper.getInstance(MyTeamListActivity.this);
        JSONObject postParam = new JSONObject();
        try {
            postParam.put("leaguetype", "Daily-Combined-Leagues");
            postParam.put("userid", "4");
            postParam.put("fixtureid", "46");
            postParam.put("leagueid", "41");
            postParam.put("teamid", "39");
        } catch (JSONException e) {
            Logger.e("json ex", e.getMessage() + " ");
        }
        vHelper.addVolleyRequestListeners(Api.getInstance().getMyTeamsUrl, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                teamHolder.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        //TODO my team listings
                    } else {
                        teamHolder.setVisibility(View.GONE);
                        errorText.setText(res.getString(res.getString("message")));
                        errorText.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Logger.e("getMyTeamListTask json ex", e.getMessage() + " ");
                }

            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Logger.e("getMyTeamListTask error res", e.getMessage() + " ");
                }
                AlertUtils.showSnack(MyTeamListActivity.this, toolbar,
                        errorMsg);
            }
        }, "getMyTeamListTask");

    }
}
