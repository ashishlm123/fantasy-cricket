package com.emts.fantasycredit.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.PreferenceHelper;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by Admin on 3/20/2018.
 */

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    CallbackManager callbackManager;
    GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_SIGN_IN = 121;
    PreferenceHelper prefsHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(getApplicationContext());
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        String fBId = prefsHelper.getString(PreferenceHelper.FB_APP_ID, getString(R.string.facebook_app_id));
        FacebookSdk.setApplicationId(fBId);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
        setContentView(R.layout.activity_login);

//        ImageView imgBack = findViewById(R.id.img_bck_arrow);
//        imgBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });

        TextView tvRegisterHere = findViewById(R.id.tv_register_here);
        tvRegisterHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });

        TextView tvForgotPassword = findViewById(R.id.tv_forgot_passwrd);
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
            }
        });

        final EditText etEmail = findViewById(R.id.et_email);
        final EditText etPassword = findViewById(R.id.et_password);
        final TextView errorEmail = findViewById(R.id.tv_error_mail);
        final TextView errorPassword = findViewById(R.id.tv_error_password);
        LinearLayout btnFb = findViewById(R.id.lay_fb);
        LinearLayout btnGoogle = findViewById(R.id.lay_google);

        Button btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = true;

                if (TextUtils.isEmpty(etEmail.getText().toString())) {
                    isValid = false;
                    errorEmail.setText(R.string.empty_field);
                    errorEmail.setVisibility(View.VISIBLE);
                } else {
                    if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
                        isValid = false;
                        errorEmail.setText(R.string.error_email);
                        errorEmail.setVisibility(View.VISIBLE);
                    } else {
                        errorEmail.setVisibility(View.GONE);
                    }
                }

                if (TextUtils.isEmpty(etPassword.getText().toString())) {
                    isValid = false;
                    errorPassword.setText(R.string.empty_field);
                    errorPassword.setVisibility(View.VISIBLE);
                } else {
                    errorPassword.setVisibility(View.GONE);
                }

                if (isValid) {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        loginTask(view, etEmail.getText().toString().trim(), etPassword.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(LoginActivity.this, view, getString(R.string.error_no_internet));
                    }
                } else {
                    AlertUtils.showSnack(LoginActivity.this, view, getString(R.string.fill_req_info));
                }
            }
        });
        btnFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    fbTask();
                } else {
                    AlertUtils.showSnack(LoginActivity.this, view, getString(R.string.error_no_internet));
                }
            }
        });
        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                    googleSignIn();
                } else {
                    AlertUtils.showSnack(LoginActivity.this, view, getString(R.string.error_no_internet));
                }
            }
        });
    }

    private void loginTask(final View view, String email, String password) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, "Please Wait...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        JSONObject postParams = new JSONObject();

        try {
            postParams.put("username", email);
            postParams.put("password", password);
        } catch (JSONException ex) {
            Logger.e("loginTask jsonaparams ex", ex.getMessage());
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().loginUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                Intent intent = new Intent(getApplicationContext(), NavMainActivityV2.class);
                                prefsHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).apply();

                                JSONArray dataArray = res.getJSONArray("data");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject eachObject = dataArray.getJSONObject(i);
                                    prefsHelper.edit().putString(PreferenceHelper.APP_USER_ID, eachObject.getString("id")).apply();
                                    prefsHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, eachObject.getString("user_email")).apply();
                                    prefsHelper.edit().putString(PreferenceHelper.APP_USER_NAME, eachObject.getString("user_name")).apply();
                                    prefsHelper.edit().putString(PreferenceHelper.APP_USER_IMG, eachObject.getString("user_name")).apply();

                                    String dob = eachObject.getString("user_dob");
                                    if (!TextUtils.isEmpty(dob) && !dob.equals("null")) {
                                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_DOB, dob).apply();
                                    } else {
                                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_DOB, "").apply();
                                    }

                                    String phnNo = eachObject.getString("phone_num");
                                    if (!TextUtils.isEmpty(phnNo) && !phnNo.equals("null")) {
                                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_MOBILE, phnNo).apply();
                                    } else {
                                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_MOBILE, "").apply();
                                    }
                                }

                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else {
                                AlertUtils.showAlertMessage(LoginActivity.this, "Error!", res.getString("message"));
                            }
                        } catch (JSONException e) {
                            pDialog.dismiss();
                            Logger.e("loginTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("normalRegistrationTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(LoginActivity.this, view, errorMsg);
                    }
                }, "loginTask");
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void fbTask() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();

        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, (Arrays.asList("email",
                "public_profile")));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Logger.e("facebook login response", loginResult.toString() + " &&\n "
                                + loginResult.getAccessToken() + "\n" + loginResult.getRecentlyGrantedPermissions()
                                + "\n" + loginResult.getRecentlyDeniedPermissions() + "\n");

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
//                                        Logger.e("fbTask res", object + " ");
//                                        Logger.e("graph response error", response.getError().getErrorMessage() + "\n"
//                                                + response.getRequest() + "\n" + response.getJSONObject());
                                        // Application code
                                        try {
                                            String id = object.optString("id");
                                            String name = object.optString("name");
                                            String first_name = object.optString("first_name");
                                            String last_name = object.optString("last_name");
//                                            String city = object.getString("hometown");
//                                            String location = object.getJSONObject("location").getString("name");
                                            String age = object.optString("age_range");
                                            String picture = object.optString("picture");
                                            String email = object.optString("email"); //Get null value here
                                            String gender = object.optString("gender");


                                            Logger.e("Facebook Data", id + "\n" + name + "\n" + first_name
                                                    + "\n" + last_name + "\n" + email + "\n" + age + "\n" + picture);
                                            LoginManager.getInstance().logOut();

                                            String birthday = "";

                                            Intent intent = new Intent(getApplicationContext(), NavMainActivityV2.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
//
//                                            registerWithFbTask(id, name, email, birthday, false);

                                        } catch (Exception e) {
                                            LoginManager.getInstance().logOut();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,first_name,last_name,email,gender," +
                                "age_range");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Logger.e("cancell", "called");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Logger.e("error", exception.getMessage() + "\n" + exception.getCause());
                    }
                });

    }

    private void registerWithFbTask(final String fbId, final String fbFirst_name, final String fbEmail,
                                    final String fbBirthday, final boolean isRegister) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        JSONObject postParams = new JSONObject();
//        postParams.put("id", fbId);
//        postParams.put("email", fbEmail);
//        postParams.put("birthday", fbBirthday);
//        postParams.put("first_name", fbFirst_name);
//        postParams.put("name", fbFirst_name);
//        String pushToken = FirebaseInstanceId.getInstance().getToken();
//        if (!TextUtils.isEmpty(pushToken)) {
//            postParams.put("push_id", pushToken);
//        }


//        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, "Logging");
//        pDialog.show();

//        vHelper.addVolleyRequestListeners(Api.getInstance().fbRegisterUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
//            @Override
//            public void onSuccess(String response) {
//                pDialog.dismiss();
//                try {
//                    JSONObject res = new JSONObject(response);
//                    if (res.getBoolean("status")) {
//
//
//                    }
//                } catch (JSONException e) {
//                    Logger.e("loginTask json ex", e.getMessage() + "");
//                }
//            }
//
//            @Override
//            public void onError(String errorResponse, VolleyError error) {
//                pDialog.dismiss();
//
//                try {
//                    JSONObject errorObj = new JSONObject(errorResponse);
//                    Toast.makeText(getApplicationContext(), errorObj.getString("message"), Toast.LENGTH_LONG).show();
//                } catch (Exception e) {
//                    if (error instanceof NetworkError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof ServerError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof AuthFailureError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof ParseError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof TimeoutError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//        }, "registerWithFbTask");

    }

    // [START signOut]
    private void signOut() {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            // [START_EXCLUDE]
                            Logger.e("google signOut", status.getStatus().getStatusMessage() + " ** "
                                    + status.isSuccess());
                            // [END_EXCLUDE]
                        }
                    });
        }
    }


    private void googleSignIn() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, GOOGLE_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Logger.e("google sign in conn failed", connectionResult.getErrorMessage() + "");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Logger.e("handelSignInResult", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            String userName = acct.getDisplayName();
            String id = acct.getId();
            String f_name = acct.getGivenName();
            String email = acct.getEmail();
            String birthday = "";

            Intent intent = new Intent(getApplicationContext(), NavMainActivityV2.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
//

//            registerWithGoogleTask(id, f_name, email, birthday, false);

//            Uri photoUri = acct.getPhotoUrl();
//            if (photoUri != null) {
//                intent.putExtra("google_pp", acct.getPhotoUrl().toString());
//            }


            signOut();
//            startActivity(intent);


        } else {
            // Signed out, show unauthenticated UI.
            signOut();
        }

    }

    private void registerWithGoogleTask(final String id, final String userName, final String email,
                                        String birthday, final boolean isRegister) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        JSONObject postParams = new JSONObject();
//        postParams.put("id", id);
//        postParams.put("email", email);
//        postParams.put("birthday", birthday);
//        postParams.put("first_name", userName);
//        postParams.put("name", userName);
//        String pushToken = FirebaseInstanceId.getInstance().getToken();
//        if (!TextUtils.isEmpty(pushToken)) {
//            postParams.put("push_id", pushToken);
//        }

        Intent intent = new Intent(getApplicationContext(), NavMainActivityV2.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, "Logging");
//        pDialog.show();

//        vHelper.addVolleyRequestListeners(Api.getInstance().googleRegisterUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
//            @Override
//            public void onSuccess(String response) {
//                pDialog.dismiss();
//                try {
//                    JSONObject res = new JSONObject(response);
//                    if (res.getBoolean("status")) {
//
//                    } else {
//
//                    }
//                } catch (JSONException e) {
//                    Logger.e("registerWithGoogleTask json ex", e.getMessage() + "");
//                }
//            }
//
//            @Override
//            public void onError(String errorResponse, VolleyError error) {
//                pDialog.dismiss();
//
//                try {
//                    JSONObject errorObj = new JSONObject(errorResponse);
//                    Toast.makeText(getApplicationContext(), errorObj.getString("message"), Toast.LENGTH_LONG).show();
//                } catch (Exception e) {
//                    if (error instanceof NetworkError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof ServerError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof AuthFailureError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof ParseError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
//                    } else if (error instanceof TimeoutError) {
//                        Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//        }, "registerWithGoogleTask");

    }

    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(this).cancelRequest("registerWithTwitter");
        VolleyHelper.getInstance(this).cancelRequest("loginTask");
        VolleyHelper.getInstance(this).cancelRequest("registerWithFbTask");
        VolleyHelper.getInstance(this).cancelRequest("registerWithGoogleTask");
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(LoginActivity.this);
        mGoogleApiClient.disconnect();
    }


}
