//package com.emts.fantasycredit.activity;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.view.GravityCompat;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.emts.fantasycredit.R;
//import com.emts.fantasycredit.fragment.DashboardOldFragment;
//import com.emts.fantasycredit.fragment.PublicLeagueFragment;
//import com.emts.fantasycredit.helper.AlertUtils;
//import com.emts.fantasycredit.helper.Logger;
//
//import nl.psdcompany.duonavigationdrawer.views.DuoDrawerLayout;
//import nl.psdcompany.duonavigationdrawer.widgets.DuoDrawerToggle;
//
///**
// * Created by User on 2018-03-20.
// */
//
//public class NavMainActivity extends AppCompatActivity {
//    Toolbar toolbar;
//    FragmentManager fm;
//    DuoDrawerLayout drawerLayout;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        setContentView(R.layout.activity_nav_main);
//
//        toolbar = findViewById(R.id.toolbar);
//        initDrawer();
//        toolbar.setNavigationIcon(R.drawable.dashbord);
//        fm = getSupportFragmentManager();
//        Fragment fragment = new DashboardOldFragment();
//        fm.beginTransaction().replace(R.id.content_frame, fragment)
//                .commit();
//    }
//
//    private void initDrawer() {
//        //drawer menu fields
//        ImageView userImage = findViewById(R.id.userImg);
//        TextView userName = findViewById(R.id.userName);
//        TextView userEmail = findViewById(R.id.userEmail);
//
//        //menu img
//        ImageView imgProfile = findViewById(R.id.imgProfile);
//        imgProfile.setOnClickListener(navMenuClickListener);
//        ImageView imgAmount = findViewById(R.id.imgAmount);
//        imgAmount.setOnClickListener(navMenuClickListener);
//        ImageView imgSetting = findViewById(R.id.imgSetting);
//        imgSetting.setOnClickListener(navMenuClickListener);
//
//        //menus
//        LinearLayout holderFeed = findViewById(R.id.holder_feed);
//        ((ImageView) holderFeed.findViewById(R.id.menuImage)).setImageResource(R.drawable.feed);
//        ((TextView) holderFeed.findViewById(R.id.menuTitle)).setText("Feed");
//        holderFeed.setOnClickListener(navMenuClickListener);
//        LinearLayout holderTeam = findViewById(R.id.holder_team);
//        ((ImageView) holderTeam.findViewById(R.id.menuImage)).setImageResource(R.drawable.team);
//        ((TextView) holderTeam.findViewById(R.id.menuTitle)).setText("Team");
//        holderTeam.setOnClickListener(navMenuClickListener);
//        LinearLayout holderDashboard = findViewById(R.id.holder_dashboard);
//        ((ImageView) holderDashboard.findViewById(R.id.menuImage)).setImageResource(R.drawable.dashbordhomeicon);
//        ((TextView) holderDashboard.findViewById(R.id.menuTitle)).setText("Dashboard");
//        holderDashboard.setOnClickListener(navMenuClickListener);
//        LinearLayout holderView = findViewById(R.id.holder_view);
//        ((ImageView) holderView.findViewById(R.id.menuImage)).setImageResource(R.drawable.view);
//        ((TextView) holderView.findViewById(R.id.menuTitle)).setText("View");
//        holderView.setOnClickListener(navMenuClickListener);
//        LinearLayout holderCreate = findViewById(R.id.holder_create);
//        ((ImageView) holderCreate.findViewById(R.id.menuImage)).setImageResource(R.drawable.create);
//        ((TextView) holderCreate.findViewById(R.id.menuTitle)).setText("Create");
//        holderCreate.setOnClickListener(navMenuClickListener);
//        LinearLayout holderLeague = findViewById(R.id.holder_league);
//        ((ImageView) holderLeague.findViewById(R.id.menuImage)).setImageResource(R.drawable.myleagus);
//        ((TextView) holderLeague.findViewById(R.id.menuTitle)).setText("My Leagues");
//        holderLeague.setOnClickListener(navMenuClickListener);
//        LinearLayout holderLogout = findViewById(R.id.holder_logout);
//        holderLogout.setOnClickListener(navMenuClickListener);
//
//        drawerLayout = findViewById(R.id.drawer);
//        DuoDrawerToggle drawerToggle = new DuoDrawerToggle(this, drawerLayout, toolbar,
//                R.string.navigation_drawer_open,
//                R.string.navigation_drawer_close);
//
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();
//    }
//
//    View.OnClickListener navMenuClickListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View view) {
//            Intent intent = null;
//            Fragment fragment = null;
//            Bundle bundle = new Bundle();
//            switch (view.getId()) {
//                case R.id.imgProfile:
//                    break;
//                case R.id.imgAmount:
//                    startActivity(new Intent(getApplicationContext(), AccountBalanceActivity.class));
//                    break;
//                case R.id.imgSetting:
//                    startActivity(new Intent(getApplicationContext(), SettingActivity.class));
//                    break;
//                case R.id.holder_feed:
//                    break;
//                case R.id.holder_team:
//                    break;
//                case R.id.holder_dashboard:
//                    fragment = new DashboardOldFragment();
//                    break;
//                case R.id.holder_view:
//                    break;
//                case R.id.holder_create:
//                    startActivity(new Intent(getApplicationContext(), CreateLeague.class));
//                    break;
//                case R.id.holder_league:
//                    Intent intent1 = new Intent(getApplicationContext(), HistoryActivity.class);
//                    intent1.putExtra("is_league", true);
//                    startActivity(intent1);
//                    break;
//                case R.id.holder_logout:
//                    break;
//            }
//            if (fragment != null) {
//                fm.beginTransaction().replace(R.id.content_frame, fragment)
//                        .commit();
//                drawerLayout.closeDrawer();
//            } else {
//
//            }
//        }
//    };
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//
//    }
//}
