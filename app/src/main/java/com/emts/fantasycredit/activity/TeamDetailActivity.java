package com.emts.fantasycredit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.PlayerDetailAdapter;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.model.Game;
import com.emts.fantasycredit.model.PlayerModel;

import java.util.ArrayList;

/**
 * Created by Admin on 3/23/2018.
 */

public class TeamDetailActivity extends AppCompatActivity {
    ArrayList<PlayerModel> playerList;
    RecyclerView playerListView;
    PlayerDetailAdapter adapter;

    Game gameMdl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_detail);

        Intent intent = getIntent();
        if (intent != null) {
            gameMdl = (Game) intent.getSerializableExtra("finishedLeague");
        } else {
            return;
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("My Team Statistics");
        TextView toolbarSubTitle = toolbar.findViewById(R.id.custom_toolbar_subtitle);
        toolbarSubTitle.setText(" 837 PTS");
        toolbarSubTitle.setVisibility(View.VISIBLE);

        playerList = new ArrayList<>();
        playerListView = findViewById(R.id.member_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        playerListView.setLayoutManager(linearLayoutManager);
        adapter = new PlayerDetailAdapter(TeamDetailActivity.this, playerList);
        adapter.setRecyclerViewItemClickListener(new PlayerDetailAdapter.RecyclerViewItemClickListener() {
            @Override
            public void onRecyclerViewItemClickListener(View view, int position) {
                AlertUtils.showToast(TeamDetailActivity.this, "TO:DO");
            }
        });
        playerListView.setAdapter(adapter);

        for (int i = 0; i < 6; i++) {
            PlayerModel user1 = new PlayerModel();
            user1.setPlayerType(PlayerModel.BATSMAN);
            user1.setPlayerName("Bibek Ultimate Batsman");
            user1.setPlayingTeam("SRH");
            user1.setFantasyPoints("245");
            playerList.add(user1);
        }
        adapter.notifyDataSetChanged();
    }


}
