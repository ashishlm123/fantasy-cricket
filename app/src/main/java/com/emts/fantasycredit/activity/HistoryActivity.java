package com.emts.fantasycredit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.fragment.PublicLeagueFragment;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 3/22/2018.
 */

public class HistoryActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    boolean isJoinedContest, isLeague;
    ProgressBar progressBar;
    TextView errorText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        Intent intent = getIntent();
        if (intent != null) {
            isLeague = intent.getBooleanExtra("is_league", false);
            isJoinedContest = intent.getBooleanExtra("is_joined_contest", false);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        viewPager = findViewById(R.id.view_pager);
        progressBar = findViewById(R.id.progress_bar);
        errorText = findViewById(R.id.error_text);
        tabLayout = findViewById(R.id.tabs);

        if (isLeague) {
            toolbarTitle.setText("My Leagues");
            if (NetworkUtils.isInNetwork(HistoryActivity.this)) {
                getGameListTask();
            } else {
                errorText.setText(getString(R.string.error_no_internet));
                errorText.setVisibility(View.VISIBLE);
            }
        } else {
            toolbarTitle.setText("History");
            addHistoryTabs(viewPager);
        }

        tabLayout.setupWithViewPager(viewPager);
    }

    private void addHistoryTabs(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Fragment fragment = new PublicLeagueFragment();
        Bundle bundle = new Bundle();
        bundle.putString("tab_name", "all");
        fragment.setArguments(bundle);

        Fragment fragment2 = new PublicLeagueFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putString("tab_name", "won");
        fragment2.setArguments(bundle2);

        Fragment fragment3 = new PublicLeagueFragment();
        Bundle bundle3 = new Bundle();
        bundle3.putString("tab_name", "Lost");
        fragment3.setArguments(bundle3);

        adapter.addFrag(fragment, "All");
        adapter.addFrag(fragment2, "Won");
        adapter.addFrag(fragment3, "Lost");
        viewPager.setAdapter(adapter);
    }

    private void addLeagueTabs(ViewPager viewPager,
                               JSONArray upcomingArray,
                               JSONArray liveArray,
                               JSONArray finishedArray) {


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Fragment fragment = new PublicLeagueFragment();
        Bundle bundle = new Bundle();
        bundle.putString("tab_name", "Live");
        bundle.putBoolean("is_league", false);
        bundle.putBoolean("is_upcoming", true);
        bundle.putString("upcomingList", upcomingArray.toString());
        fragment.setArguments(bundle);

        Fragment fragment2 = new PublicLeagueFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putBoolean("is_league", true);
        bundle2.putBoolean("is_upcoming", false);
        bundle2.putString("liveList", liveArray.toString());
        fragment2.setArguments(bundle2);

        Fragment fragment3 = new PublicLeagueFragment();
        Bundle bundle3 = new Bundle();
        bundle3.putBoolean("is_league", false);
        bundle3.putBoolean("is_upcoming", false);
        bundle3.putString("finishedList", finishedArray.toString());
        fragment3.setArguments(bundle3);

        adapter.addFrag(fragment, "UpComing");
        adapter.addFrag(fragment2, "Live");
        adapter.addFrag(fragment3, "Finished");
        viewPager.setAdapter(adapter);

        if (isJoinedContest) {
            viewPager.setCurrentItem(1);
        }

    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void getGameListTask() {
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(HistoryActivity.this);
        JSONObject postParam = new JSONObject();
        try {
            postParam.put("userid", "190");
        } catch (JSONException e) {
            Logger.e("json exp", e.getMessage() + " ");
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().getMyLeagueUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject eachObj = res.getJSONObject("data");
                                JSONArray upComingArray = eachObj.getJSONArray("upcoming");
                                JSONArray liveArray = eachObj.getJSONArray("live");
                                JSONArray finishedArray = eachObj.getJSONArray("past");
                                addLeagueTabs(viewPager, upComingArray, liveArray, finishedArray);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getGameListTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "getGameListTask");
    }

}

