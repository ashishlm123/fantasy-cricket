package com.emts.fantasycredit.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 3/20/2018.
 */

public class ForgotPasswordActivity extends AppCompatActivity {
    TextView tvErrorEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        final TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Forgot Password");

        final EditText etEmail = findViewById(R.id.et_email);
        tvErrorEmail = findViewById(R.id.tv_error_mail);

        Button btnSend = findViewById(R.id.btn_send);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etEmail.getText().toString().trim();
                if (validation(email)) {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        forgotPasswordTask(etEmail.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(ForgotPasswordActivity.this, tvErrorEmail, getString(R.string.error_no_internet));
                    }
                } else {
                    AlertUtils.showSnack(ForgotPasswordActivity.this, tvErrorEmail, getString(R.string.fill_req_info));
                }
            }
        });

    }

    private boolean validation(String email) {
        boolean isValid = true;

        if (TextUtils.isEmpty(email)) {
            isValid = false;
            tvErrorEmail.setText(R.string.empty_field);
            tvErrorEmail.setVisibility(View.VISIBLE);
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                isValid = false;
                tvErrorEmail.setText(R.string.error_email);
                tvErrorEmail.setVisibility(View.VISIBLE);
            } else {
                tvErrorEmail.setVisibility(View.GONE);
            }
        }
        return isValid;
    }

    private void forgotPasswordTask(String eMail) {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(ForgotPasswordActivity.this, "Please Wait");
        dialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(ForgotPasswordActivity.this);
        JSONObject postParams = new JSONObject();

        try {
            postParams.put("email", "shiva@sciflare.com");
        } catch (JSONException ex) {
            Logger.e("forgotPasswordTask jsonParams ex", ex.getMessage() + " ");
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().forgotPasswordUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showAlertMessage(ForgotPasswordActivity.this, "Success", res.getString("message"),
                                        "OK", "", null);
                            } else {
                                AlertUtils.showAlertMessage(ForgotPasswordActivity.this, "Success", res.getString("message"),
                                        "OK", "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("forgotPasswordTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        dialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("forgotPasswordTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(ForgotPasswordActivity.this, tvErrorEmail, errorMsg);
                    }
                }, "forgotPasswordTask");
    }
}
