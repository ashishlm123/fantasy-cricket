package com.emts.fantasycredit.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.PreferenceHelper;
import com.emts.fantasycredit.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePasswordActivity extends AppCompatActivity {
    TextView tvErrOldPswd, tvErrNewPswd, tvErrConPswd;

    @SuppressLint("ClickableViewAccessibility")
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Change Password");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvErrOldPswd = findViewById(R.id.err_current_pswd);
        tvErrNewPswd = findViewById(R.id.err_new_pswd);
        tvErrConPswd = findViewById(R.id.err_con_pswd);

        final EditText edtCurrentPswd, edtNewPswd, edtConfirmPswd;
        edtCurrentPswd = findViewById(R.id.et_current_password);
        edtNewPswd = findViewById(R.id.et_new_password);
        edtConfirmPswd = findViewById(R.id.et_con_new_password);

        edtCurrentPswd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                visiblePassword(edtCurrentPswd, event);
                return false;
            }
        });

        edtNewPswd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                visiblePassword(edtNewPswd, event);
                return false;
            }
        });

        edtConfirmPswd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                visiblePassword(edtConfirmPswd, event);
                return false;
            }
        });

        Button btnChngPswd = findViewById(R.id.btn_chng_pswd);
        btnChngPswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation(edtCurrentPswd.getText().toString().trim(), edtNewPswd.getText().toString().trim(),
                        edtConfirmPswd.getText().toString().trim())) {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        changePasswordTask(edtCurrentPswd.getText().toString().trim(), edtNewPswd.getText().toString().trim(),
                                edtConfirmPswd.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(getApplicationContext(), view, getResources().getString(R.string.error_no_internet));
                    }
                }
            }
        });
    }

    private void changePasswordTask(String oldPswd, String newPswd, String conPswd) {
        AlertUtils.hideInputMethod(ChangePasswordActivity.this, tvErrConPswd);
        final ProgressDialog dialog = AlertUtils.showProgressDialog(ChangePasswordActivity.this, "Please Wait...");
        dialog.show();
        VolleyHelper volleyHelper = VolleyHelper.getInstance(this);
        JSONObject postParams = new JSONObject();
        try {
            postParams.put("userid", PreferenceHelper.getInstance(ChangePasswordActivity.this).getUserId());
            postParams.put("old_password", oldPswd);
            postParams.put("password1", newPswd);
            postParams.put("password2", conPswd);
        } catch (JSONException ex) {
            Logger.e("changePasswordTask jsonParams ex", ex.getMessage() + " ");
        }

        volleyHelper.addVolleyRequestListeners(Api.getInstance().changePasswordUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                dialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.showAlertMessage(ChangePasswordActivity.this, "Success", res.getString("message"),
                                "Ok", "", null);
                    } else {
                        AlertUtils.showAlertMessage(ChangePasswordActivity.this, "Error", res.getString("message"),
                                "Ok", "", null);
                    }
                } catch (JSONException ex) {
                    Logger.e("changePasswordTask json ex", ex.getMessage() + " ");

                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                dialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("normalRegistrationTask error res", e.getMessage() + " ");
                }
                AlertUtils.showSnack(ChangePasswordActivity.this, tvErrConPswd, errorMsg);
            }
        }, "changePasswordTask");
    }

    public boolean validation(String oldPswd, String newPswd, String conPswd) {
        boolean isValid = true;
        //For Password
        if (oldPswd.equals("")) {
            tvErrOldPswd.setVisibility(View.VISIBLE);
            tvErrOldPswd.setText("This field is required...");
            isValid = false;
        } else {
            tvErrOldPswd.setVisibility(View.GONE);
        }


        if (newPswd.equals("")) {
            tvErrNewPswd.setVisibility(View.VISIBLE);
            tvErrNewPswd.setText("This field is required...");
            isValid = false;
        } else {
//            if (newPassword.getText().toString().trim().length() >= 6) {
            if (newPswd.equals(conPswd)) {
                tvErrNewPswd.setVisibility(View.GONE);
            } else {
                tvErrNewPswd.setText("Password do not match");
                tvErrNewPswd.setVisibility(View.VISIBLE);
                isValid = false;
            }
        }

        return isValid;
    }

    private void visiblePassword(EditText etPassword, MotionEvent event) {
        // set integer 0 , 1, 2, 3 for left, top, right and bottom respectively
        final int DRAWABLE_RIGHT = 2;

        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (event.getRawX() >= (etPassword.getRight() - etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                if (etPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }
                etPassword.setSelection(etPassword.getText().length());

            }
        }
    }


}
