package com.emts.fantasycredit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emts.fantasycredit.R;
import com.emts.fantasycredit.fragment.DashboardFragment;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.PreferenceHelper;
import com.squareup.picasso.Picasso;
import com.yarolegovich.slidingrootnav.SlideGravity;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;


/**
 * Created by User on 2018-03-23.
 */

public class NavMainActivityV2 extends AppCompatActivity {
    Toolbar toolbar;
    TextView toolbarTitle;
    FragmentManager fm;
    private SlidingRootNav slidingRootNav;
    PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_nav_main_v2);


        preferenceHelper = PreferenceHelper.getInstance(NavMainActivityV2.this);
        toolbar = findViewById(R.id.toolbar);
        initDrawer();
        toolbar.setNavigationIcon(null);
        LinearLayout menuHolder = toolbar.findViewById(R.id.holder_menu);
        menuHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (slidingRootNav.isMenuOpened()) {
                    slidingRootNav.closeMenu(true);
                } else {
                    slidingRootNav.openMenu(true);
                }
            }
        });
        toolbar.inflateMenu(R.menu.toolbar_logo_menu);
        toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        fm = getSupportFragmentManager();
        Fragment fragment = new DashboardFragment();
        fm.beginTransaction().replace(R.id.content_frame, fragment)
                .commit();
//        toolbarTitle.setText("Dashboard");
    }


    private void initDrawer() {
        slidingRootNav = new SlidingRootNavBuilder(this)
                .withMenuLayout(R.layout.layout_nav_menu)
                .withDragDistance(200) //Horizontal translation of a view. Default == 180dp
                .withRootViewScale(0.7f) //Content view's scale will be interpolated between 1f and 0.7f. Default == 0.65f;
                .withRootViewElevation(10) //Content view's elevation will be interpolated between 0 and 10dp. Default == 8.
                .withRootViewYTranslation(4) //Content view's translationY will be interpolated between 0 and 4. Default == 0
                .withMenuOpened(false) //Initial menu opened/closed state. Default == false
                .withMenuLocked(false) //If true, a user can't open or close the menu. Default == false.
                .withGravity(SlideGravity.LEFT) //If LEFT you can swipe a menu from left to right, if RIGHT - the direction is opposite.
//                .withSavedState(savedInstanceState) //If you call the method, layout will restore its opened/closed state
                .withContentClickableWhenMenuOpened(false) //Pretty self-descriptive. Builder Default == true
                .inject();


        //drawer menu fields
        ImageView userImage = findViewById(R.id.userImg);
        TextView userName = findViewById(R.id.userName);
        TextView userEmail = findViewById(R.id.userEmail);
        if (preferenceHelper.isLogin()) {
            userImage.setVisibility(View.GONE);
            userEmail.setText(preferenceHelper.getUserEmail());
            userName.setText(preferenceHelper.getString(PreferenceHelper.APP_USER_NAME, " "));
//            Picasso.with(NavMainActivityV2.this).
//                    load(preferenceHelper.getString(PreferenceHelper.APP_USER_IMG, "")).into(userImage);
        } else {
            userImage.setVisibility(View.GONE);
        }
        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NavMainActivityV2.this, UserDetailsActivity.class));
            }
        });
        userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NavMainActivityV2.this, UserDetailsActivity.class));
            }
        });

        //menu img
        ImageView imgProfile = findViewById(R.id.imgProfile);
        TextView tvProfileBadge = findViewById(R.id.tv_profile_badge);
        tvProfileBadge.setVisibility(View.GONE);
        imgProfile.setOnClickListener(navMenuClickListener);

        ImageView imgAmount = findViewById(R.id.imgAmount);
        TextView tvAmtBadge = findViewById(R.id.tv_amt_badge);
        tvAmtBadge.setVisibility(View.GONE);
        imgAmount.setOnClickListener(navMenuClickListener);

        ImageView imgSetting = findViewById(R.id.imgSetting);
        imgSetting.setOnClickListener(navMenuClickListener);

        //holder_login and holder_logout view
        LinearLayout holderLogin = findViewById(R.id.holder2);
        RelativeLayout holderLogoutView = findViewById(R.id.holder_logout_view);
        Button btnLogin = holderLogoutView.findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        Button btnSignUp = holderLogoutView.findViewById(R.id.btn_signup);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });

        if (preferenceHelper.isLogin()) {
            holderLogin.setVisibility(View.VISIBLE);
            holderLogoutView.setVisibility(View.GONE);
        } else {
            holderLogin.setVisibility(View.GONE);
            holderLogoutView.setVisibility(View.VISIBLE);
        }

        //menus
        LinearLayout holderFeed = findViewById(R.id.holder_feed);
        ((ImageView) holderFeed.findViewById(R.id.menuImage)).setImageResource(R.drawable.feed);
        ((TextView) holderFeed.findViewById(R.id.menuTitle)).setText("Feed");
        holderFeed.setOnClickListener(navMenuClickListener);

        LinearLayout holderTeam = findViewById(R.id.holder_team);
        ((ImageView) holderTeam.findViewById(R.id.menuImage)).setImageResource(R.drawable.team);
        ((TextView) holderTeam.findViewById(R.id.menuTitle)).setText("Team");
        holderTeam.setOnClickListener(navMenuClickListener);

        LinearLayout holderDashboard = findViewById(R.id.holder_dashboard);
        ((ImageView) holderDashboard.findViewById(R.id.menuImage)).setImageResource(R.drawable.dashbordhomeicon);
        ((TextView) holderDashboard.findViewById(R.id.menuTitle)).setText("DashBoard");

        LinearLayout holderReferralTracking = findViewById(R.id.holder_referral_tracking);
        ((ImageView) holderReferralTracking.findViewById(R.id.menuImage)).setImageResource(R.drawable.team);
        ((TextView) holderReferralTracking.findViewById(R.id.menuTitle)).setText("Referral Tracking");
        holderReferralTracking.setOnClickListener(navMenuClickListener);
        holderDashboard.setOnClickListener(navMenuClickListener);

        LinearLayout holderView = findViewById(R.id.holder_view);
        ((ImageView) holderView.findViewById(R.id.menuImage)).setImageResource(R.drawable.view);
        ((TextView) holderView.findViewById(R.id.menuTitle)).setText("View");
        holderView.setOnClickListener(navMenuClickListener);

        LinearLayout holderCreate = findViewById(R.id.holder_create);
        ImageView imgCreate = holderCreate.findViewById(R.id.menuImage);
        imgCreate.setImageResource(R.drawable.ic_add_box_black_24dp);
        ((TextView) holderCreate.findViewById(R.id.menuTitle)).setText("Create");
        holderCreate.setOnClickListener(navMenuClickListener);

        LinearLayout holderLeague = findViewById(R.id.holder_league);
        ((ImageView) holderLeague.findViewById(R.id.menuImage)).setImageResource(R.drawable.myleagus);
        ((TextView) holderLeague.findViewById(R.id.menuTitle)).setText("My Leagues");
        holderLeague.setOnClickListener(navMenuClickListener);

        LinearLayout holderLogout = findViewById(R.id.holder_logout);
        if (preferenceHelper.isLogin()) {
            holderLogout.setVisibility(View.VISIBLE);
        } else {
            holderLogout.setVisibility(View.GONE);
        }
        holderLogout.setOnClickListener(navMenuClickListener);

//        drawerLayout = findViewById(R.id.drawer);
//        DuoDrawerToggle drawerToggle = new DuoDrawerToggle(this, drawerLayout, toolbar,
//                R.string.navigation_drawer_open,
//                R.string.navigation_drawer_close);
//
//        drawerLayout.setDrawerListener(drawerToggle);
//        drawerToggle.syncState();
    }

    View.OnClickListener navMenuClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = null;
            Fragment fragment1 = null;
            Bundle bundle = new Bundle();
            switch (view.getId()) {
                case R.id.imgProfile:
                    if (preferenceHelper.isLogin()) {
                        startActivity(new Intent(getApplicationContext(), UserDetailsActivity.class));
                    } else {
                        AlertUtils.showToast(getApplicationContext(), "Please login to view your profile");
                    }
                    break;
                case R.id.imgAmount:
                    if (preferenceHelper.isLogin()) {
                        startActivity(new Intent(getApplicationContext(), AccountBalanceActivity.class));
                    } else {
                        AlertUtils.showToast(getApplicationContext(), "Please login to view your account balance");
                    }
                    break;
                case R.id.imgSetting:
                    startActivity(new Intent(getApplicationContext(), SettingActivity.class));
                    break;
                case R.id.holder_feed:
                    AlertUtils.showToast(NavMainActivityV2.this, "TODO");
                    break;
                case R.id.holder_team:
                    AlertUtils.showToast(NavMainActivityV2.this, "TODO");
//                    startActivity(new Intent(getApplicationContext(), MyTeamListActivity.class));
                    break;
                case R.id.holder_dashboard:
                    toolbarTitle.setText("Spartan11");
                    fragment1 = new DashboardFragment();
                    break;
                case R.id.holder_referral_tracking:
                    if (preferenceHelper.isLogin()) {
                        startActivity(new Intent(getApplicationContext(), ReferralActivity.class));
                    } else {
                        AlertUtils.showToast(getApplicationContext(), "Please login to view this page.");
                    }
                    break;
                case R.id.holder_view:
                    Intent intent2 = new Intent(getApplicationContext(), HistoryActivity.class);
                    intent2.putExtra("is_league", false);
                    startActivity(intent2);
                    break;
                case R.id.holder_create:
                    if (preferenceHelper.isLogin()) {
                        startActivity(new Intent(getApplicationContext(), CreateLeague.class));
                    } else {
                        AlertUtils.showToast(getApplicationContext(), "Please login to view this page.");
                    }
                    break;
                case R.id.holder_league:
                    if (preferenceHelper.isLogin()) {
                        Intent intent1 = new Intent(getApplicationContext(), HistoryActivity.class);
                        intent1.putExtra("is_league", true);
                        startActivity(intent1);
                    } else {
                        AlertUtils.showToast(getApplicationContext(), "Please login to view this page.");
                    }
                    break;
                case R.id.holder_logout:
                    preferenceHelper.edit().clear().commit();
                    Intent intent3 = new Intent(getApplicationContext(), MainActivity.class);
                    intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent3);
                    break;
            }
            if (fragment1 != null) {
                fm.beginTransaction().replace(R.id.content_frame, fragment1)
                        .commit();
                slidingRootNav.closeMenu(true);
            }
        }
    };

}
