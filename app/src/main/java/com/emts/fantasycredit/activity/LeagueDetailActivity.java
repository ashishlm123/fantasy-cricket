package com.emts.fantasycredit.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;


import com.emts.fantasycredit.model.User;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.PlayerAdapter;

import java.util.ArrayList;

/**
 * Created by Admin on 3/22/2018.
 */

public class LeagueDetailActivity extends AppCompatActivity {
    RecyclerView playerListView;
    PlayerAdapter adapter;
    ArrayList<User> playerList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_league_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("League Score");

        playerList = new ArrayList<>();
        playerListView = findViewById(R.id.player_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        playerListView.setLayoutManager(layoutManager);
        adapter = new PlayerAdapter(LeagueDetailActivity.this, playerList);
        playerListView.setAdapter(adapter);


        for (int i = 0; i < 6; i++) {
            User user1 = new User();
            playerList.add(user1);
        }
        adapter.notifyDataSetChanged();

    }
}
