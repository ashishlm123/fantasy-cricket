package com.emts.fantasycredit.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class MakeOwnContestActvity extends AppCompatActivity {
    int contestSize;
    int winninPrize;
    TextView tvEntryFee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_own_contest_actvity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Make your own contest");

        TextView tvTeamVsTeam = findViewById(R.id.tv_team_vs_team);
        TextView tvGameTimer = findViewById(R.id.tv_timer);

        final EditText edtContestName = findViewById(R.id.edt_contest_name);
        final EditText edtTotalWinningPrize = findViewById(R.id.edt_total_winning_amt);
        final EditText edtContestSize = findViewById(R.id.edt_contest_size);

        tvEntryFee = findViewById(R.id.tv_entryfee);

        edtTotalWinningPrize.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int len;
                if (charSequence.toString().isEmpty()) {
                    len = 0;
                } else {
                    len = Integer.parseInt(charSequence.toString());
                }

                if (len > 0 && len <= 10000) {
                    winninPrize = len;
                }

                condition();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtContestSize.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int len;
                if (charSequence.toString().isEmpty()) {
                    len = 0;
                } else {
                    len = Integer.parseInt(charSequence.toString());
                }

                if (len > 2 && len <= 100) {
                    contestSize = len;
                }

                condition();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        Button btnCreate = findViewById(R.id.btn_create_contests_invite_friends);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((Integer.parseInt(edtContestSize.getText().toString().trim()) > 2 &&
                        (Integer.parseInt(edtContestSize.getText().toString().trim())) <= 100)
                        && (Integer.parseInt(edtTotalWinningPrize.getText().toString().trim()) > 0
                        && (Integer.parseInt(edtTotalWinningPrize.getText().toString().trim())) <= 10000)) {
                    if (NetworkUtils.isInNetwork(MakeOwnContestActvity.this)) {
                        createContestTask(view);
                    } else {
                        AlertUtils.showSnack(MakeOwnContestActvity.this, edtContestName,
                                getResources().getString(R.string.error_no_internet));
                    }
                } else {
                    AlertUtils.showSnack(MakeOwnContestActvity.this, edtContestName, "Enter Valid Information");
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void condition() {
        if ((contestSize > 2 && contestSize <= 100) && (winninPrize > 0 && winninPrize <= 10000)) {
            setEntryFee(winninPrize, contestSize);
        } else {
            tvEntryFee.setText(getResources().getString(R.string.entry_fee_per_team) + " : ");
        }
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void setEntryFee(int winPrize, int conSize) {
        double entryFee = (winPrize / conSize) * 1.1;
        tvEntryFee.setText(getResources().getString(R.string.entry_fee_per_team) + " : " + String.format("%.1f", entryFee));
    }

    private void createContestTask(final View view) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(MakeOwnContestActvity.this, "Please Wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        JSONObject postObject = new JSONObject();

        try {
            postObject.put("", "");
        } catch (JSONException ex) {
            Logger.e("createContestTask jsonparams ex", ex.getMessage());
        }

        vHelper.addVolleyRequestListeners("", Request.Method.POST, postObject, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.show();
                try {
                    JSONObject res = new JSONObject(response);
                    //todo
                } catch (JSONException e) {
                    Logger.e("createContestTask jsonex ex", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                pDialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("createContestTask error res", e.getMessage() + " ");
                }
                AlertUtils.showSnack(MakeOwnContestActvity.this, view, errorMsg);
            }
        }, "createContestTask");
    }
}
