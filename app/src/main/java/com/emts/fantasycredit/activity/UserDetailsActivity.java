package com.emts.fantasycredit.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.emts.fantasycredit.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UserDetailsActivity extends AppCompatActivity {
    ProgressBar progressBar;
    ScrollView userDetailHolder;
    TextView errorText, errUserName, errEm, errDOB;
    EditText etName, etEmail, etPassword, etDob, etMobile, etTeam, etAddress, etCity, etPin,
            etState, etCountry;
    Button btnMale, btnFemale, btnUpdateProfile;
    Switch switchNotification;
    private Calendar cal;
    private int day, month, year;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Personal Detail");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressBar = findViewById(R.id.progress_bar);
        errorText = findViewById(R.id.error_text);
        userDetailHolder = findViewById(R.id.user_detail_holder);

        init();

        if (NetworkUtils.isInNetwork(UserDetailsActivity.this)) {
            getUserDetailTask();
        } else {
            errorText.setText(R.string.error_no_internet);
            errorText.setVisibility(View.VISIBLE);
        }

    }

    private void init() {
        etName = findViewById(R.id.et_name);
        etDob = findViewById(R.id.et_dob);
        etMobile = findViewById(R.id.et_verify_mobile_num);
        etEmail = findViewById(R.id.et_email);
        etAddress = findViewById(R.id.et_address);
        etCity = findViewById(R.id.et_city);
        etPassword = findViewById(R.id.et_password);
        etCountry = findViewById(R.id.et_country);
        etState = findViewById(R.id.et_state);
        etPin = findViewById(R.id.edt_pin_code);
        btnUpdateProfile = findViewById(R.id.btn_update_profile);
        errUserName = findViewById(R.id.error_name);
        errEm = findViewById(R.id.error_email);
        errDOB = findViewById(R.id.error_dob);

        etDob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    datePicker();
                }
                return true;
            }
        });

        btnUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    updateProfileTask();
                } else {
                    AlertUtils.showSnack(UserDetailsActivity.this, errorText, "Please fill all the required field");
                }
            }
        });

    }

    public void datePicker() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                day = dayOfMonth;
                month = monthOfYear + 1;
                UserDetailsActivity.this.year = year;
                etDob.setText(String.format("%02d", dayOfMonth) + "/" +
                        String.format("%02d", (monthOfYear + 1)) + "/" + year);
            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(this, listener, year, month, day);
        dpDialog.show();
    }

    private void updateProfileTask() {
        AlertUtils.hideInputMethod(UserDetailsActivity.this, errorText);
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(UserDetailsActivity.this, " Updating profile...");
        progressDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(UserDetailsActivity.this);
        JSONObject postParam = new JSONObject();
        try {
            postParam.put("userid", "4");
            postParam.put("username", etName.getText().toString().trim());
            postParam.put("phonenumber", etMobile.getText().toString().trim());
            postParam.put("date", day);
            postParam.put("month", month);
            postParam.put("year", year);
        } catch (JSONException e) {
            Logger.e("json ex", e.getMessage() + " ");
        }
        vHelper.addVolleyRequestListeners(Api.getInstance().updateUserProfileUrl, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONArray dataArray = new JSONArray();
                        AlertUtils.showAlertMessage(UserDetailsActivity.this, "Success Message",
                                res.getString("message"), "Ok", "", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        UserDetailsActivity.this.finish();
                                    }
                                });

                    } else
                        AlertUtils.showAlertMessage(UserDetailsActivity.this, "Error Message",
                                res.getString("message"), "Ok", "", null);
                } catch (JSONException e) {
                    Logger.e("json ex", e.getMessage() + " ");
                }

            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressDialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("updateProfileTask error res", e.getMessage() + " ");
                }
                AlertUtils.showSnack(UserDetailsActivity.this, errorText, errorMsg);
            }
        }, "updateProfileTask");


    }

    public boolean validation() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etName.getText().toString().trim())) {
            isValid = false;
            errUserName.setVisibility(View.VISIBLE);
            errUserName.setText("This field is required...");
        } else {
            errUserName.setVisibility(View.GONE);
        }

        //For Email Address
        if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
            isValid = false;
            errEm.setVisibility(View.VISIBLE);
            errEm.setText("This Field is required...");
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                errEm.setVisibility(View.GONE);
            } else {
                isValid = false;
                errEm.setText("Invalid email address.");
                errEm.setVisibility(View.VISIBLE);
            }
        }

        //For DOB
        if (TextUtils.isEmpty(etDob.getText().toString())) {
            isValid = false;
            errDOB.setVisibility(View.VISIBLE);
            errDOB.setText("This Field is required...");
        } else {
            errDOB.setVisibility(View.GONE);
        }

        return isValid;
    }

    private void getUserDetailTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(UserDetailsActivity.this);
        JSONObject postParam = new JSONObject();
        try {
            postParam.put("userid", "4");
        } catch (JSONException e) {
            Logger.e("json ex", e.getMessage() + " ");
        }
        vHelper.addVolleyRequestListeners(Api.getInstance().getUserDetailUrl, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);

                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONArray dataArray = res.getJSONArray("data");

                        JSONObject eachObj = dataArray.getJSONObject(0);
                        etEmail.setText(eachObj.getString("user_email"));
                        etName.setText(eachObj.getString("user_name"));
                        etMobile.setText(eachObj.getString("phone_num"));
                        etDob.setText(eachObj.getString("user_dob"));

                        String dob = eachObj.getString("user_dob");
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            Date date = format.parse(dob);
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);
                            day = cal.get(Calendar.DAY_OF_MONTH);
                            month = cal.get(Calendar.MONTH) + 1;
                            year = cal.get(Calendar.YEAR);
                        } catch (ParseException e) {
                            Logger.e("date ex", e.getMessage() + " ");
                        }

                        userDetailHolder.setVisibility(View.VISIBLE);
                    } else {
                        errorText.setText(res.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                        userDetailHolder.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Logger.e("json ex", e.getMessage() + " ");
                }

            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("getUserDetailTask error res", e.getMessage() + " ");
                }
                errorText.setText(errorMsg);
                errorText.setVisibility(View.VISIBLE);
            }
        }, "getUserDetailTask");
    }
}
