package com.emts.fantasycredit.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.PlayerDetailAdapter;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.PreferenceHelper;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.emts.fantasycredit.model.Game;
import com.emts.fantasycredit.model.LeagueModel;
import com.emts.fantasycredit.model.PlayerModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Admin on 3/23/2018.
 */

public class CreateTeamActivity extends AppCompatActivity {
    private int TEAM_NEXT_REQUEST_CODE = 998;

    RecyclerView playerListView;
    ArrayList<PlayerModel> playerList, filterList;
    PlayerDetailAdapter adapter;
    TextView tvWicketSel, tvBatSelect, tvAllRounderSel, tvBallSel, errorText, tvPickInfo,
            tvSeriesInfo, tvTotalPlayerSelected, tvCreditsLeft;
    ImageView ivWicket, ivBat, ivAllRounder, ivBall;
    RelativeLayout wicketBg, batBg, allRounderBg, ballBg, prevSelectedBg;
    LinearLayout playerHolder;
    ProgressBar progressBar;
    Button btnNext;
    Game gameMdl;
    LeagueModel leagueModel;
    PreferenceHelper prefHelper;
    String currentTypeSelection = PlayerModel.WICKET_KEEPER;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_team);

        Intent intent = getIntent();
        if (intent != null) {
            gameMdl = (Game) intent.getSerializableExtra("game");
            leagueModel = (LeagueModel) intent.getSerializableExtra("league");
        }

        prefHelper = PreferenceHelper.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Create Team");

        //header infos
        ImageView imgTeam1 = findViewById(R.id.img_team1);
        ImageView imgTeam2 = findViewById(R.id.img_team2);
        TextView tvTeam1 = findViewById(R.id.tv_team1);
        TextView tvTeam2 = findViewById(R.id.tv_team2);
        TextView teamTitle = findViewById(R.id.game_title);
        if (!TextUtils.isEmpty(gameMdl.getTeam1Logo())) {
            Glide.with(CreateTeamActivity.this).load(gameMdl.getTeam1Logo()).into(imgTeam1);
        }
        if (!TextUtils.isEmpty(gameMdl.getTeam2Logo())) {
            Glide.with(CreateTeamActivity.this).load(gameMdl.getTeam2Logo()).into(imgTeam2);
        }
        tvTeam1.setText(gameMdl.getTeam1ShortCode().toUpperCase());
        tvTeam2.setText(gameMdl.getTeam2ShortCode().toUpperCase());
        teamTitle.setText(gameMdl.getTeam1ShortCode().toUpperCase() + " VS " + gameMdl.getTeam2ShortCode().toUpperCase());

        tvSeriesInfo = findViewById(R.id.tvSeriesInfo);

        playerList = new ArrayList<>();
        filterList = new ArrayList<>();

        playerListView = findViewById(R.id.player_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        playerListView.setLayoutManager(linearLayoutManager);
        adapter = new PlayerDetailAdapter(CreateTeamActivity.this, filterList);
        adapter.setCreateTeam(true);
        adapter.setRecyclerViewItemClickListener(new PlayerDetailAdapter.RecyclerViewItemClickListener() {
            @Override
            public void onRecyclerViewItemClickListener(View view, int position) {
                if (view.getId() == R.id.img_add) {
                    updatePlayerSelected(position);
                } else if (view.getId() == R.id.player_image || view.getId() == R.id.tv_player_name) {
                    Intent intent1 = new Intent(getApplicationContext(), PlayerActivity.class);
                    intent1.putExtra("player", playerList.get(position));
                    intent1.putExtra("fixtureId", gameMdl.getFixtureId());
                    startActivity(intent1);
                }
            }
        });
        playerListView.setAdapter(adapter);
        playerListView.setNestedScrollingEnabled(false);

        playerHolder = findViewById(R.id.player_holder);
        progressBar = findViewById(R.id.progress);
        errorText = findViewById(R.id.error_text);

        initPlayerDepartView();

        if (NetworkUtils.isInNetwork(CreateTeamActivity.this)) {
            getPlayerListTask();
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }

    private void updateToDataList(PlayerModel playerModel, boolean isSelected) {
        int indexOf = playerList.indexOf(playerModel);
        playerList.get(indexOf).setIsSelected(isSelected);
    }

    private void updatePlayerSelected(int position) {
        int totalPlayerSelected = 0;
        int creditsUsed = 0;
        int selectionInCurrentDepart = 0;
        for (int i = 0; i < playerList.size(); i++) {
            if (playerList.get(i).getIsSelected()) {
                totalPlayerSelected++;

                if (playerList.get(i).getPlayerType().equalsIgnoreCase(currentTypeSelection)) {
                    selectionInCurrentDepart++;
                }
            }
        }
        boolean wasSelected = filterList.get(position).getIsSelected();

        if (totalPlayerSelected == 11 && !wasSelected) {
            AlertUtils.showSnack(CreateTeamActivity.this, playerListView, "Total of 11 players can be selected");
            return;
        }

        TextView tvSelected = tvWicketSel;
        if (currentTypeSelection.equalsIgnoreCase(PlayerModel.WICKET_KEEPER)) {
            tvSelected = tvWicketSel;
            if (selectionInCurrentDepart == 1 && !wasSelected) {
                AlertUtils.showSnack(CreateTeamActivity.this, playerListView, "Only 1 wicket keeper can be selected");
                return;
            }
        } else if (currentTypeSelection.equalsIgnoreCase(PlayerModel.BATSMAN)) {
            tvSelected = tvBatSelect;
            if (selectionInCurrentDepart == 5 && !wasSelected) {
                AlertUtils.showSnack(CreateTeamActivity.this, playerListView, "Upto 5 batsman can be selected");
                return;
            }
        } else if (currentTypeSelection.equalsIgnoreCase(PlayerModel.ALL_ROUNDER)) {
            tvSelected = tvAllRounderSel;
            if (selectionInCurrentDepart == 2 && !wasSelected) {
                AlertUtils.showSnack(CreateTeamActivity.this, playerListView, "Only 2 batsman can be selected");
                return;
            }
        } else if (currentTypeSelection.equalsIgnoreCase(PlayerModel.BOWLER)) {
            tvSelected = tvBallSel;
            if (selectionInCurrentDepart == 5 && !wasSelected) {
                AlertUtils.showSnack(CreateTeamActivity.this, playerListView, "Upto 5 bowler can be selected");
                return;
            }
        }

        if (!wasSelected) {
            totalPlayerSelected = totalPlayerSelected + 1;
            selectionInCurrentDepart = selectionInCurrentDepart + 1;
        } else {
            totalPlayerSelected = totalPlayerSelected - 1;
            selectionInCurrentDepart = selectionInCurrentDepart - 1;
        }

        tvTotalPlayerSelected.setText(totalPlayerSelected + "/11");
        tvSelected.setText(String.valueOf(selectionInCurrentDepart));

//        updateToDataList(filterList.get(position), !wasSelected);
//        filterList.get(position).setIsSelected(!wasSelected);
        if (wasSelected) {
            updateToDataList(filterList.get(position), false);
            filterList.get(position).setIsSelected(false);
        } else {
            updateToDataList(filterList.get(position), true);
            filterList.get(position).setIsSelected(true);
        }
        adapter.notifyItemChanged(position);

        if (totalPlayerSelected == 11) {
            btnNext.setEnabled(true);
            btnNext.setBackgroundResource(R.drawable.bg_btn_accent1);
        } else {
            btnNext.setEnabled(false);
            btnNext.setBackgroundResource(R.drawable.bg_rounded_disable);
        }

        //todo credit minus and set
    }

    private void initPlayerDepartView() {
        tvPickInfo = findViewById(R.id.tv_pick_user);

        final RelativeLayout wicketHolder = findViewById(R.id.wicket_holder);
        wicketBg = wicketHolder.findViewById(R.id.lay_circle);
        ivWicket = wicketHolder.findViewById(R.id.img_type);
        ivWicket.setImageResource(R.drawable.wicket);
        tvWicketSel = wicketHolder.findViewById(R.id.tv_selected_no);
        wicketHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectType(wicketHolder);
            }
        });

        final RelativeLayout batHolder = findViewById(R.id.bat_holder);
        batBg = batHolder.findViewById(R.id.lay_circle);
        ivBat = batHolder.findViewById(R.id.img_type);
        ivBat.setImageResource(R.drawable.bat);
        tvBatSelect = batHolder.findViewById(R.id.tv_selected_no);
        batHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectType(batHolder);
            }
        });

        final RelativeLayout allRounderHolder = findViewById(R.id.bat_n_ball_holder);
        allRounderBg = allRounderHolder.findViewById(R.id.lay_circle);
        ivAllRounder = allRounderHolder.findViewById(R.id.img_type);
        ivAllRounder.setImageResource(R.drawable.batnball);
        tvAllRounderSel = allRounderHolder.findViewById(R.id.tv_selected_no);
        allRounderHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectType(allRounderHolder);
            }
        });

        final RelativeLayout ballHolder = findViewById(R.id.ball_holder);
        ballBg = ballHolder.findViewById(R.id.lay_circle);
        ivBall = ballHolder.findViewById(R.id.img_type);
        ivBall.setImageResource(R.drawable.ball);
        ivBall.setPadding(16, 16, 16, 16);
        tvBallSel = ballHolder.findViewById(R.id.tv_selected_no);
        ballHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectType(ballHolder);
            }
        });

        btnNext = findViewById(R.id.bt_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PickCapViceCapActivity.class);
                intent.putExtra("playerList", playerList);
                intent.putExtra("game", gameMdl);
                intent.putExtra("league", leagueModel);
                startActivityForResult(intent, TEAM_NEXT_REQUEST_CODE);
            }
        });

        //set default selection
        selectType(wicketHolder);

        tvTotalPlayerSelected = findViewById(R.id.tv_player_no);
        tvCreditsLeft = findViewById(R.id.tv_credit_left);
    }

    private void selectType(View view) {
        if (prevSelectedBg != null) {
            switch (prevSelectedBg.getId()) {
                //todo check if select and show white else show grey dark
                case R.id.wicket_holder:
                    wicketBg.setBackgroundResource(R.drawable.bg_grey_circle);
                    ivWicket.setColorFilter(Color.WHITE);
                    tvWicketSel.setBackgroundResource(R.drawable.bg_accent_circle);
                    break;
                case R.id.bat_holder:
                    batBg.setBackgroundResource(R.drawable.bg_grey_circle);
                    ivBat.setColorFilter(Color.WHITE);
                    tvBatSelect.setBackgroundResource(R.drawable.bg_accent_circle);
                    break;
                case R.id.bat_n_ball_holder:
                    allRounderBg.setBackgroundResource(R.drawable.bg_grey_circle);
                    ivAllRounder.setColorFilter(Color.WHITE);
                    tvAllRounderSel.setBackgroundResource(R.drawable.bg_accent_circle);
                    break;
                case R.id.ball_holder:
                    ballBg.setBackgroundResource(R.drawable.bg_grey_circle);
                    ivBall.setColorFilter(Color.WHITE);
                    tvBallSel.setBackgroundResource(R.drawable.bg_accent_circle);
                    break;
            }
        }
        prevSelectedBg = (RelativeLayout) view;
        switch (view.getId()) {
            case R.id.wicket_holder:
                wicketBg.setBackgroundResource(R.drawable.bg_depart_select);
                ivWicket.setColorFilter(Color.WHITE);
                tvWicketSel.setBackgroundResource(R.drawable.bg_accent_circle);

                tvPickInfo.setText("Pick 1 Wicket Keeper");
                filterType(PlayerModel.WICKET_KEEPER);
                currentTypeSelection = PlayerModel.WICKET_KEEPER;
                break;
            case R.id.bat_holder:
                batBg.setBackgroundResource(R.drawable.bg_depart_select);
                ivBat.setColorFilter(Color.WHITE);
                tvBatSelect.setBackgroundResource(R.drawable.bg_accent_circle);

                tvPickInfo.setText("Pick min 3 or max 5 batsmen");
                filterType(PlayerModel.BATSMAN);
                currentTypeSelection = PlayerModel.BATSMAN;
                break;
            case R.id.bat_n_ball_holder:
                allRounderBg.setBackgroundResource(R.drawable.bg_depart_select);
                ivAllRounder.setColorFilter(Color.WHITE);
                tvAllRounderSel.setBackgroundResource(R.drawable.bg_accent_circle);

                tvPickInfo.setText("Pick 2 All Rounders");
                filterType(PlayerModel.ALL_ROUNDER);
                currentTypeSelection = PlayerModel.ALL_ROUNDER;
                break;
            case R.id.ball_holder:
                ballBg.setBackgroundResource(R.drawable.bg_depart_select);
                ivBall.setColorFilter(Color.WHITE);
                tvBallSel.setBackgroundResource(R.drawable.bg_accent_circle);

                tvPickInfo.setText("Pick min 3 or max 5 bowlers");
                filterType(PlayerModel.BOWLER);
                currentTypeSelection = PlayerModel.BOWLER;
                break;
        }
    }

    private void filterType(String type) {
        filterList.clear();
        for (int i = 0; i < playerList.size(); i++) {
            if (playerList.get(i).getPlayerType().equalsIgnoreCase(type)) {
                filterList.add(playerList.get(i));
            }
        }

        if (filterList.size() > 0) {
            adapter.notifyDataSetChanged();
            playerListView.setVisibility(View.VISIBLE);
            errorText.setVisibility(View.GONE);
        } else {
            errorText.setText("No players");
            errorText.setVisibility(View.VISIBLE);
            playerListView.setVisibility(View.GONE);
        }
    }

    private void getPlayerListTask() {
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);
        playerListView.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(CreateTeamActivity.this);
        JSONObject postParam = new JSONObject();
        try {
            postParam.put("leaguetype", "Daily-Combined-Leagues");
            postParam.put("userid", prefHelper.getUserId());
            postParam.put("fixtureid", gameMdl.getFixtureId());
            postParam.put("leagueid", leagueModel.getLeagueId());
        } catch (JSONException e) {
            Logger.e("getPlayerListTask postPram ex", e.getMessage() + " ");
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().getMatchInfoUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        playerHolder.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject resJson = new JSONObject(response);

                            //series info
                            JSONObject seriesInfoObj = resJson.getJSONArray("series_info").getJSONObject(0);
                            tvSeriesInfo.setText(seriesInfoObj.getString("match_game_shortname") + " - "
                                    + seriesInfoObj.getString("updated_time"));

                            JSONArray playerArray = resJson.getJSONArray("fantasy_players");
                            for (int i = 0; i < playerArray.length(); i++) {
                                JSONObject eachPlayer = playerArray.getJSONObject(i);

                                PlayerModel player = new PlayerModel();
                                player.setPlayerId(eachPlayer.getString("id"));
                                player.setPlayerName(eachPlayer.getString("player"));
                                player.setImage(eachPlayer.getString("image_src"));
                                player.setPlayingTeam(eachPlayer.getString("team"));
                                player.setPlayingTeamId(eachPlayer.getString("team_id"));
                                player.setFantasyPoints(eachPlayer.getString("fantacy_points"));
                                player.setPlayerType(eachPlayer.getString("type"));

                                playerList.add(player);
                            }
                            filterType(PlayerModel.WICKET_KEEPER);

                        } catch (JSONException e) {
                            Logger.e("getPlayerLIstTask res ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorText.setText(errorObj.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                errorText.setText(getString(R.string.error_no_internet));
                            } else if (error instanceof ServerError) {
                                errorText.setText(getString(R.string.error_server));
                            } else if (error instanceof AuthFailureError) {
                                errorText.setText(getString(R.string.error_authFailureError));
                            } else if (error instanceof ParseError) {
                                errorText.setText(getString(R.string.error_parse_error));
                            } else if (error instanceof TimeoutError) {
                                errorText.setText(getString(R.string.error_time_out));
                            }
                            errorText.setVisibility(View.VISIBLE);
                            playerListView.setVisibility(View.GONE);
                        }
                    }
                }, "getPlayerListTask");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TEAM_NEXT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                boolean isCreated = data.getBooleanExtra("isCreated", false);
                Logger.e("isCreated", String.valueOf(isCreated));
                if (isCreated) {
                    onBackPressed();
                }
            }
        }
    }
}


//                if (breadcrumbsView.getCurrentStep() == 3) {
//                    startActivity(new Intent(getApplicationContext(), MyTeamActivity.class));
//                } else {
//                    breadcrumbsView.nextStep();
//
//                    new android.os.Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            Logger.e("currentstate", breadcrumbsView.getCurrentStep() + " ");
//                            if (breadcrumbsView.getCurrentStep() == 1) {
//                                ivBat.setColorFilter(getResources().getColor(R.color.white));
//                                tvSelectedPlayer1.setBackground(getResources().getDrawable(R.drawable.bg_circle));
//                            } else if (breadcrumbsView.getCurrentStep() == 2) {
//                                ivAllRounder.setColorFilter(getResources().getColor(R.color.white));
//                                tvSelectedPlayer2.setBackground(getResources().getDrawable(R.drawable.bg_circle));
//                            } else if (breadcrumbsView.getCurrentStep() == 3) {
//                                ivBall.setColorFilter(getResources().getColor(R.color.white));
//                                tvSelectedPlayer3.setBackground(getResources().getDrawable(R.drawable.bg_circle));
//                            }
//                        }
//                    }, 800);
//
//                }

//        findViewById(R.id.bt_prev).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                breadcrumbsView.prevStep();
//
//            }
//        });


//    //Survive config changes.
//    @Override
//    public Object onRetainCustomNonConfigurationInstance() {
//        return breadcrumbsView.getCurrentStep();
//    }

//        //Survive config changes.
//        if (getLastCustomNonConfigurationInstance() == null) {
//            cacheCurrentStep = 0;
//        } else {
//            cacheCurrentStep = (Integer) getLastCustomNonConfigurationInstance();
//        }
//
//        breadcrumbsView = (BreadcrumbsView) findViewById(R.id.breadcrumbs);
//        breadcrumbsView.setCurrentStep(cacheCurrentStep);
//
//        breadcrumbsView.post(new Runnable() {
//            @Override
//            public void run() {
//                RelativeLayout dotLayout = breadcrumbsView.getDotView(0);
//                LayoutInflater inflater = LayoutInflater.from(CreateTeamActivity.this);
//                View inflatedLayout = inflater.inflate(R.layout.item_breadcrumb_view, null, false);
//                ivWicket = inflatedLayout.findViewById(R.id.img_type);
//                ivWicket.setImageResource(R.drawable.bat);
//                ivWicket.setColorFilter(getResources().getColor(R.color.black));
//                tvSelectedPlayer = inflatedLayout.findViewById(R.id.tv_selected_no);
//                dotLayout.addView(inflatedLayout);
//
//                RelativeLayout dotLayout1 = breadcrumbsView.getDotView(1);
//                LayoutInflater inflater1 = LayoutInflater.from(CreateTeamActivity.this);
//                View inflatedLayout1 = inflater1.inflate(R.layout.item_breadcrumb_view, null, false);
//                ivBat = inflatedLayout1.findViewById(R.id.img_type);
//                ivBat.setImageResource(R.drawable.wicket);
//                ivBat.setColorFilter(getResources().getColor(R.color.black));
//                tvSelectedPlayer1 = inflatedLayout1.findViewById(R.id.tv_selected_no);
//                dotLayout1.addView(inflatedLayout1);
//
//                RelativeLayout dotLayout2 = breadcrumbsView.getDotView(2);
//                LayoutInflater inflater2 = LayoutInflater.from(CreateTeamActivity.this);
//                View inflatedLayout2 = inflater2.inflate(R.layout.item_breadcrumb_view, null, false);
//                ivAllRounder = inflatedLayout2.findViewById(R.id.img_type);
//                ivAllRounder.setImageResource(R.drawable.batnball);
//                ivAllRounder.setColorFilter(getResources().getColor(R.color.black));
//                tvSelectedPlayer2 = inflatedLayout2.findViewById(R.id.tv_selected_no);
//                dotLayout2.addView(inflatedLayout2);
//
//                RelativeLayout dotLayout3 = breadcrumbsView.getDotView(3);
//                LayoutInflater inflater3 = LayoutInflater.from(CreateTeamActivity.this);
//                View inflatedLayout3 = inflater3.inflate(R.layout.item_breadcrumb_view, null, false);
//                ivBall = inflatedLayout3.findViewById(R.id.img_type);
//                ivBall.setImageResource(R.drawable.ball);
//                ivBall.setColorFilter(getResources().getColor(R.color.black));
//                tvSelectedPlayer3 = inflatedLayout3.findViewById(R.id.tv_selected_no);
//                dotLayout3.addView(inflatedLayout3);
//
//                if (breadcrumbsView.getCurrentStep() == 0) {
//                    tvSelectedPlayer.setBackground(getResources().getDrawable(R.drawable.bg_circle));
//                    ivWicket.setColorFilter(getResources().getColor(R.color.white));
//                }
//
//            }
//        });