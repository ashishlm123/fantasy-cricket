package com.emts.fantasycredit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.adapter.LeaguesAdapter;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.PreferenceHelper;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.emts.fantasycredit.model.Game;
import com.emts.fantasycredit.model.LeagueModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LeagueListsActivity extends AppCompatActivity {
    ArrayList<LeagueModel> megaLeagueLists;
    LeaguesAdapter megaLeagueAdapter;
    ProgressBar progressBar;
    TextView errorText;
    TextView tvLeagueTitle;
    Game gameMdl;
    PreferenceHelper prefHelper;
    RecyclerView rvMegaLegLstngs;
    LinearLayout layBottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leagues);

        Intent intent = getIntent();
        if (intent != null) {
            gameMdl = (Game) intent.getSerializableExtra("league");
        } else {
            return;
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Leagues");

        prefHelper = PreferenceHelper.getInstance(this);

        megaLeagueLists = new ArrayList<>();

        layBottom = findViewById(R.id.lay_bottom);
        tvLeagueTitle = findViewById(R.id.lvlList);
        progressBar = findViewById(R.id.progress_bar);
        errorText = findViewById(R.id.error_text);

        rvMegaLegLstngs = findViewById(R.id.rv_mega_league_listings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvMegaLegLstngs.setLayoutManager(linearLayoutManager);
        megaLeagueAdapter = new LeaguesAdapter(getApplicationContext(), megaLeagueLists);
        megaLeagueAdapter.setRecyclerItemClickListener(new LeaguesAdapter.RecyclerItemClickListener() {
            @Override
            public void onRecyclerItemClick(View view, int position) {
                if (prefHelper.isLogin()) {
                    Intent intent1 = new Intent(getApplicationContext(), MyTeamListActivity.class);
                    intent1.putExtra("game", gameMdl);
                    intent1.putExtra("league", megaLeagueLists.get(position));
                    startActivity(intent1);
                } else {
                    AlertUtils.showSnack(getApplicationContext(), view, "Please login to join to league");
                }
            }
        });
        rvMegaLegLstngs.setAdapter(megaLeagueAdapter);
        rvMegaLegLstngs.setNestedScrollingEnabled(false);

        RelativeLayout btnMyTeam, btnContestJoined;
        btnMyTeam = findViewById(R.id.btn_myTeam);
        btnContestJoined = findViewById(R.id.btn_myContest);

        btnMyTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MyTeamListActivity.class);
                intent.putExtra("game", gameMdl);
                intent.putExtra("isMyTeamButton", true);
                startActivity(intent);
            }
        });

        btnContestJoined.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getApplicationContext(), HistoryActivity.class);
                intent1.putExtra("is_league", true);
                intent1.putExtra("is_joined_contest", true);
                startActivity(intent1);
            }
        });

        TextView tvTeamQty = findViewById(R.id.tv_team_qty);
        TextView tvJoineContestQty = findViewById(R.id.tv_joined_contest_qty);


        RelativeLayout contestCodeHolder = findViewById(R.id.lay_contest_code);
        RelativeLayout myContestHolder = findViewById(R.id.lay_my_contest);
        contestCodeHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LeagueListsActivity.this, InviteCodeActivity.class));
            }
        });

        myContestHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LeagueListsActivity.this, MakeOwnContestActvity.class));
            }
        });

        if (NetworkUtils.isInNetwork(LeagueListsActivity.this)) {
            getFixtureLeaguesTask();
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }

    private void getFixtureLeaguesTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvLeagueTitle.setVisibility(View.GONE);
        errorText.setVisibility(View.GONE);
        VolleyHelper volleyHelper = VolleyHelper.getInstance(LeagueListsActivity.this);
        JSONObject postParam = new JSONObject();
        try {
            postParam.put("fixtureid", "55");//todo
//            postParam.put("fixtureid", gameMdl.getLeagueId());
            postParam.put("userid", prefHelper.getUserId());
        } catch (JSONException e) {
            Logger.e("getFixtureLeaguesTask json ex", e.getMessage() + " ");
        }
        volleyHelper.addVolleyRequestListeners(Api.getInstance().getFixtureLeagueUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray leagueArray = res.getJSONArray("leagueList");
                                if (leagueArray.length() == 0) {
                                    errorText.setText(" No leagues in this fixture at moment");
                                    errorText.setVisibility(View.VISIBLE);
                                    layBottom.setVisibility(View.GONE);
                                } else {
                                    for (int i = 0; i < leagueArray.length(); i++) {
                                        JSONObject eachObj = leagueArray.getJSONObject(i);
                                        LeagueModel leagueModel = new LeagueModel();
                                        leagueModel.setLeagueSize(eachObj.getString("league_size"));
                                        leagueModel.setLeagueMember(eachObj.getString("league_members"));
                                        leagueModel.setEntryFee(eachObj.getString("fee"));
                                        leagueModel.setLeagueId(eachObj.getString("id"));
                                        leagueModel.setTotalWinning(eachObj.getString("prize"));
                                        megaLeagueLists.add(leagueModel);
                                    }
                                    megaLeagueAdapter.notifyDataSetChanged();
                                    errorText.setVisibility(View.GONE);
                                    rvMegaLegLstngs.setVisibility(View.VISIBLE);
                                    layBottom.setVisibility(View.VISIBLE);
                                    tvLeagueTitle.setVisibility(View.VISIBLE);
                                }
                            } else {
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("getFixtureLeaguesTask ex", e.getMessage() + " ");
                        }


                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getFixtureLeaguesTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);

                    }
                }, "getFixtureLeaguesTask");

    }
}
