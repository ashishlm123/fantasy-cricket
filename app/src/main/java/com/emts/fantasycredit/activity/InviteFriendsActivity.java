package com.emts.fantasycredit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.emts.fantasycredit.R;
import com.emts.fantasycredit.helper.AlertUtils;

public class InviteFriendsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        final TextView inviteCode = findViewById(R.id.tv_invite_code);

        Button btnInviteViaWhatsApp = findViewById(R.id.btn_invite_via_whatsapp);
        btnInviteViaWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.whatsapp");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, inviteCode.getText().toString().trim());
                try {
                    startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    AlertUtils.showToast(InviteFriendsActivity.this, "Whatsapp has not been installed...");
                }
            }
        });

        TextView tvMoreOptions = findViewById(R.id.tv_more_options);
        tvMoreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, inviteCode.getText().toString().trim());
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name) + " Invitation");
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });
    }
}
