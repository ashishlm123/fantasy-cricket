package com.emts.fantasycredit.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.helper.AlertUtils;
import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.PreferenceHelper;
import com.emts.fantasycredit.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by User on 2018-03-23.
 */

public class VerifyAccountActivity extends AppCompatActivity {
    public static final int REQUEST_CODE_PAN_IMAGE = 124;
    public static final int REQUEST_CODE_BANK_PROOF = 123;

    public static final int CAMERA_CAPTURE_PAN_IMAGE_REQUEST_CODE = 9873;
    public static final int CAMERA_CAPTURE_BANK_IMAGE_REQUET_CODE = 9874;

    public static final int PERMISSION_REQUEST_CODE = 300;

    public static final int RESULT_LOAD_VIDEO = 124;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    public String panCardImage = "";
    public String bankProofImage = "";

    public static Uri panFileUri = null;
    public static Uri bankFileUri = null;

    public int label = 0;

    public int postYear;
    public int postMonth;
    public int postDay;

    TextView tvBankProofImageName, tvPanCardImageName;


    CardView cardPan, cardBank, cardMobile;
    CardView cardFormPan, cardFormBank, cardFormMobile;
    TextView tvPan, tvBank, tvMobile, tvMandatoryText, tvQuestionText;
    ImageView imgPan, imgBank, imgMobile;
    Button btnUploadPanCardImage, btnUploadBankProof;

    Button btnSaveAndContinue;
    PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_verify_account);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView customToolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        customToolbarTitle.setText("Verify Account");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        preferenceHelper = PreferenceHelper.getInstance(VerifyAccountActivity.this);
        cardPan = findViewById(R.id.cardPan);
        tvPan = findViewById(R.id.textPan);
        imgPan = findViewById(R.id.imgPan);
        cardBank = findViewById(R.id.cardBank);
        tvBank = findViewById(R.id.textBank);
        imgBank = findViewById(R.id.imgBank);
        cardMobile = findViewById(R.id.cardMobile);
        tvMobile = findViewById(R.id.textMobile);
        imgMobile = findViewById(R.id.imgMobile);

        tvMandatoryText = findViewById(R.id.mandatory_text);
        tvQuestionText = findViewById(R.id.question);

        cardFormPan = findViewById(R.id.formCardPan);
        cardFormBank = findViewById(R.id.formCardBank);
        cardFormMobile = findViewById(R.id.formCardMobile);

        tvBankProofImageName = findViewById(R.id.tv_bank_proof_image_name);
        tvPanCardImageName = findViewById(R.id.tv_pan_card_img_name);

        btnUploadBankProof = findViewById(R.id.btn_upload_bank_proof);
        btnUploadPanCardImage = findViewById(R.id.btn_upload_pan_card_image);

        final EditText etPanName = cardFormPan.findViewById(R.id.et_pan_name);
        final EditText etPanNum = cardFormPan.findViewById(R.id.et_pannumber);

        final Calendar myCalendar = Calendar.getInstance();
        final EditText etDob = cardFormPan.findViewById(R.id.et_dob);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, day);

                postYear = year;
                postMonth = month + 1;
                postDay = day;
                etDob.setText(year + "/" + (month + 1) + "/" + day);
            }
        };
        etDob.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                new DatePickerDialog(VerifyAccountActivity.this, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        final Spinner spinState = cardFormPan.findViewById(R.id.spin_branch);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>
                (this, R.layout.item_spinner_text,
                        getResources().getStringArray(R.array.state_list)); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        spinState.setAdapter(spinnerArrayAdapter);


        final EditText etAccountNum = cardFormBank.findViewById(R.id.et_ac_number);
        final EditText etIFSC = cardFormBank.findViewById(R.id.et_ifsc_code);
        final EditText etBankName = cardFormBank.findViewById(R.id.et_bank_name);
        final EditText etBankBranch = cardFormBank.findViewById(R.id.et_bnk_branch);

        final EditText etmobile = cardFormMobile.findViewById(R.id.et_mobile);


        selectCard(cardPan, tvPan, imgPan);
        cardFormPan.setVisibility(View.VISIBLE);
        tvQuestionText.setVisibility(View.VISIBLE);
        tvMandatoryText.setVisibility(View.VISIBLE);

        btnUploadBankProof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertForPhoto(REQUEST_CODE_BANK_PROOF);
            }
        });

        btnUploadPanCardImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertForPhoto(REQUEST_CODE_PAN_IMAGE);
            }
        });

        btnSaveAndContinue = findViewById(R.id.btn_save_and_continue);
        btnSaveAndContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                panState = spinState.getSelectedItem().toString();
                if (label == 0) {
                    if (panValidation(etPanName.getText().toString().trim(), etPanNum.getText().toString().trim(),
                            etDob.getText().toString().trim(), spinState.getSelectedItem().toString(), panCardImage)) {
                        accountTabClick(R.id.holderBank);
                    } else {
                        AlertUtils.showSnack(VerifyAccountActivity.this, tvBankProofImageName, "Please fill all information");

                    }
                } else if (label == 1) {
                    if (bankValidation(etAccountNum.getText().toString().trim(), etIFSC.getText().toString().trim(),
                            etBankName.getText().toString().trim(), etBankBranch.getText().toString().trim(), bankProofImage)) {
                        accountTabClick(R.id.holderMobile);
                    } else {
                        AlertUtils.showSnack(VerifyAccountActivity.this, tvBankProofImageName, "Please fill all information");

                    }
                } else if (label == 2) {
                    if (panValidation(etPanName.getText().toString().trim(),
                            etPanNum.getText().toString().trim(),
                            etDob.getText().toString().trim(),
                            "Goa", panCardImage) &&
                            (bankValidation(etAccountNum.getText().toString().trim(),
                                    etIFSC.getText().toString().trim(),
                                    etBankName.getText().toString().trim(),
                                    etBankBranch.getText().toString().trim(),
                                    bankProofImage)) &&
                            mobileValidation(etmobile.getText().toString().trim())) {
                        if (NetworkUtils.isInNetwork(VerifyAccountActivity.this)) {
                            verifyAccountTask(panCardImage, bankProofImage, etPanName.getText().toString().trim(),
                                    etPanNum.getText().toString().trim(), String.valueOf(postDay), String.valueOf(postMonth),
                                    String.valueOf(postYear), spinState.getSelectedItem().toString(), etAccountNum.getText().toString().trim(),
                                    etIFSC.getText().toString().trim(), etBankName.getText().toString().trim(),
                                    etBankBranch.getText().toString().trim(), etmobile.getText().toString().trim());
                            //TODO Verify Account Task
                        } else {
                            AlertUtils.showSnack(VerifyAccountActivity.this, tvBankProofImageName, getResources().getString(R.string.error_no_internet));
                        }
                    } else {
                        AlertUtils.showSnack(VerifyAccountActivity.this, tvBankProofImageName, "Please fill all information regarding pan,bank and mobile");
                    }
                } else {
                    AlertUtils.showSnack(VerifyAccountActivity.this, tvBankProofImageName, "Please fill all information regarding pan,bank and mobile");
                }
            }
        });
    }

    private boolean panValidation(String panName, String panNum, String panDob, String panState, String panImg) {
        boolean isValid = true;

        TextView tvErrPanName, tvErrPanNum, tvErrPanDob, tvErrPanState, tvErrPanImg;
        tvErrPanName = findViewById(R.id.tv_err_pan_name);
        tvErrPanNum = findViewById(R.id.tv_err_pan_num);
        tvErrPanDob = findViewById(R.id.tv_err_pan_dob);
        tvErrPanState = findViewById(R.id.tv_err_spin_state);
        tvErrPanImg = findViewById(R.id.tv_err_pan_card_img);

        if (panName.equals("")) {
            tvErrPanName.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrPanName.setVisibility(View.GONE);
        }

        if (panNum.equals("")) {
            tvErrPanNum.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrPanNum.setVisibility(View.GONE);
        }

        if (panDob.equals("")) {
            tvErrPanDob.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrPanDob.setVisibility(View.GONE);
        }

        if (panState.equals("")) {
            tvErrPanState.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrPanState.setVisibility(View.GONE);
        }

        if (panImg.equals("")) {
            isValid = false;
            tvErrPanImg.setVisibility(View.VISIBLE);
        } else {
            tvErrPanImg.setVisibility(View.GONE);
        }


        return isValid;
    }

    private boolean bankValidation(String accNum, String ifscCode, String bankName, String bankBranch, String bankImg) {
        boolean isValid = true;

        TextView tvErrAccNum, tvErrIsfcCode, tvErrBankName, tvErrBankBranch, tvErrBankImg;
        tvErrAccNum = findViewById(R.id.tv_err_acc_num);
        tvErrIsfcCode = findViewById(R.id.tv_err_isfc_code);
        tvErrBankName = findViewById(R.id.tv_err_bank_name);
        tvErrBankBranch = findViewById(R.id.tv_err_bank_branch);
        tvErrBankImg = findViewById(R.id.tv_err_bank_proof_image);

        if (accNum.equals("")) {
            tvErrAccNum.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrAccNum.setVisibility(View.GONE);
        }

        if (ifscCode.equals("")) {
            isValid = false;
            tvErrIsfcCode.setVisibility(View.VISIBLE);
        } else {
            tvErrIsfcCode.setVisibility(View.GONE);
        }

        if (bankName.equals("")) {
            isValid = false;
            tvErrBankName.setVisibility(View.VISIBLE);
        } else {
            tvErrBankName.setVisibility(View.GONE);
        }

        if (bankBranch.equals("")) {
            isValid = false;
            tvErrBankBranch.setVisibility(View.VISIBLE);
        } else {
            tvErrBankBranch.setVisibility(View.GONE);
        }

        if (bankImg.equals("")) {
            isValid = false;
            tvErrBankImg.setVisibility(View.VISIBLE);
        } else {
            tvErrBankImg.setVisibility(View.GONE);
        }

        return isValid;
    }

    private boolean mobileValidation(String mobNum) {
        boolean isValid = true;
        TextView tvErrPhn = findViewById(R.id.tv_err_mobile);
        if (mobNum.equals("")) {
            tvErrPhn.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrPhn.setVisibility(View.GONE);
        }
        return isValid;
    }

    private void selectCard(CardView card, TextView tv, ImageView img) {
        card.setCardBackgroundColor(Color.WHITE);
        card.setCardElevation(16);
        img.setBackgroundResource(0);
        img.setColorFilter(getResources().getColor(R.color.colorAccent));
        tv.setTextColor(getResources().getColor(R.color.colorAccent));
    }

    private void unSelectCard(CardView card, TextView tv, ImageView img) {
        card.setCardBackgroundColor(Color.TRANSPARENT);
        card.setCardElevation(0);
        img.setBackgroundResource(R.drawable.bg_dotted_gray);
        img.setColorFilter(getResources().getColor(R.color.greyBtnDetail));
        tv.setTextColor(getResources().getColor(R.color.greyBtnDetail));
    }

    public void accountTabClick(View view) {
        accountTabClick(view.getId());
    }

    public void accountTabClick(int id) {
        switch (id) {
            case R.id.holderPan:
                label = 0;
                selectCard(cardPan, tvPan, imgPan);
                unSelectCard(cardMobile, tvMobile, imgMobile);
                unSelectCard(cardBank, tvBank, imgBank);

                cardFormPan.setVisibility(View.VISIBLE);
                tvQuestionText.setVisibility(View.VISIBLE);
                tvMandatoryText.setVisibility(View.VISIBLE);

                cardFormBank.setVisibility(View.GONE);

                cardFormMobile.setVisibility(View.GONE);
                break;

            case R.id.holderBank:
                label = 1;
                unSelectCard(cardPan, tvPan, imgPan);
                unSelectCard(cardMobile, tvMobile, imgMobile);
                selectCard(cardBank, tvBank, imgBank);

                cardFormPan.setVisibility(View.GONE);
                tvQuestionText.setVisibility(View.GONE);
                tvMandatoryText.setVisibility(View.GONE);

                cardFormBank.setVisibility(View.VISIBLE);

                cardFormMobile.setVisibility(View.GONE);
                break;

            case R.id.holderMobile:
                label = 2;
                unSelectCard(cardPan, tvPan, imgPan);
                selectCard(cardMobile, tvMobile, imgMobile);
                unSelectCard(cardBank, tvBank, imgBank);

                cardFormPan.setVisibility(View.GONE);
                tvQuestionText.setVisibility(View.GONE);
                tvMandatoryText.setVisibility(View.GONE);

                cardFormBank.setVisibility(View.GONE);

                cardFormMobile.setVisibility(View.VISIBLE);
                break;
        }
    }

    //Photo ko lagi
    private void alertForPhoto(final int requestCode) {
        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(VerifyAccountActivity.this);
        builder.setTitle("Upload Profile Picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (ContextCompat.checkSelfPermission(VerifyAccountActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(VerifyAccountActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PERMISSION_REQUEST_CODE);
                    } else {
                        takePhoto();
                    }

                } else if (options[item].equals("Choose From Gallery")) {
                    if (ContextCompat.checkSelfPermission(VerifyAccountActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(VerifyAccountActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                requestCode);
                    } else {
                        chooseFrmGallery(false, requestCode);
                    }
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void chooseFrmGallery(boolean video, int requestCode) {
        Logger.e("choose from gallery ma pugis", "ah yaar");
        if (video) {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_VIDEO);
        } else {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, requestCode);
        }
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        if (label == 0) {
            panFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, panFileUri);

            // start the image capture Intent
            startActivityForResult(intent, CAMERA_CAPTURE_PAN_IMAGE_REQUEST_CODE);
        } else if (label == 1) {
            bankFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, bankFileUri);

            // start the image capture Intent
            startActivityForResult(intent, CAMERA_CAPTURE_BANK_IMAGE_REQUET_CODE);
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));

//        Uri photoURI = FileProvider.getUriForFile(RegistrationActivity.this,
//                BuildConfig.APPLICATION_ID + ".provider",
//                getOutputMediaFile(type));
//        Uri photoURI = FileProvider.getUriForFile(RegistrationActivity.this,
//                getApplicationContext().getPackageName() + ".provider", getOutputMediaFile(type));
//        return photoURI;
    }

    private static File getOutputMediaFile(int type) {
        // External sdcard location
//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "Alacer");
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                "file_fantasy");
//        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath());

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "file_fantasy" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_BANK_PROOF || requestCode == REQUEST_CODE_PAN_IMAGE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);
                Logger.e("selImgPath", selImgPath);
                String imgName = selImgPath.substring(selImgPath.lastIndexOf("/") + 1);

                if (requestCode == REQUEST_CODE_BANK_PROOF) {
                    tvBankProofImageName.setText(imgName);
                    bankProofImage = selImgPath;
                }
                if (requestCode == REQUEST_CODE_PAN_IMAGE) {
                    tvPanCardImageName.setText(imgName);
                    panCardImage = selImgPath;
                }

            } else if (requestCode == CAMERA_CAPTURE_BANK_IMAGE_REQUET_CODE || requestCode == CAMERA_CAPTURE_PAN_IMAGE_REQUEST_CODE) {
                if (requestCode == CAMERA_CAPTURE_BANK_IMAGE_REQUET_CODE) {
                    if (bankFileUri != null) {
                        String selImgPath = bankFileUri.getPath();
                        Logger.e("selImgPath", selImgPath);
                        String imgName = selImgPath.substring(selImgPath.lastIndexOf("/") + 1);
                        tvBankProofImageName.setText(imgName);
                        bankProofImage = selImgPath;
                    }
                }
                if (requestCode == CAMERA_CAPTURE_PAN_IMAGE_REQUEST_CODE) {
                    if (panFileUri != null) {
                        String selImgPath = panFileUri.getPath();
                        Logger.e("selImgPath", selImgPath);
                        String imgName = selImgPath.substring(selImgPath.lastIndexOf("/") + 1);
                        tvPanCardImageName.setText(imgName);
                        panCardImage = selImgPath;
                    }
                }
            }
        }
    }

    private void verifyAccountTask(String panImg, String banProofImg, String fullName, String panCardNo,
                                   String day, String month, String year, String state, String bankAcNo,
                                   String ifscCode, String bankName, String bankBranch, String mobileNo) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(VerifyAccountActivity.this, "Please Wait...");
        pDialog.show();
        VolleyHelper volleyHelper = VolleyHelper.getInstance(this);
        JSONObject postMap = new JSONObject();
        try {
            postMap.put("user_account_id", preferenceHelper.getUserId());
            postMap.put("pancard_image", "");
            postMap.put("bank_proof", "");
            postMap.put("full_name", fullName);
            postMap.put("pancard_no", panCardNo);
            postMap.put("get_date", day);
            postMap.put("get_month", month);
            postMap.put("get_year", year);
            postMap.put("state_list", state);
            postMap.put("account_no", bankAcNo);
            postMap.put("ifsc_code", ifscCode);
            postMap.put("bank_name", bankName);
            postMap.put("branch_name", bankBranch);
            postMap.put("mobile_no", mobileNo);
        } catch (JSONException ex) {
            Logger.e("verifyAccountTask postParams ex", ex.getMessage());
        }

        volleyHelper.addVolleyRequestListeners(Api.getInstance().verifyAccountUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.showAlertMessage(VerifyAccountActivity.this, "Success", res.getString("message"), "Ok", "", new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                VerifyAccountActivity.this.finish();
                            }
                        });
                    } else {
                        AlertUtils.showAlertMessage(VerifyAccountActivity.this, "Error", res.getString("message"), "OK", " ", null);
                    }
                } catch (JSONException e) {
                    Logger.e("normalRegistrationTask json res", e.getMessage() + " ");

                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                pDialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("normalRegistrationTask error res", e.getMessage() + " ");
                }
                AlertUtils.showSnack(VerifyAccountActivity.this, tvBankProofImageName, errorMsg);
            }
        }, "verifyAccountTask");
    }
}
