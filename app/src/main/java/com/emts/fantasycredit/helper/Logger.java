package com.emts.fantasycredit.helper;

import android.text.TextUtils;
import android.util.Log;

/**
 * Created by theone on 2016-03-04.
 */
public class Logger {

    public static void e(String tag, String message) {
        if (TextUtils.isEmpty(message)) {
            Log.e(tag, "******************EMPTY MESSAGE*********************");
        } else {
            Log.e(tag, message);
        }
    }

    public static void printLongLog(String tag, String message) {
        int maxLogSize = 1000;
        for (int i = 0; i <= message.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i + 1) * maxLogSize;
            end = end > message.length() ? message.length() : end;
            e(tag, message.substring(start, end));
        }
    }
}
