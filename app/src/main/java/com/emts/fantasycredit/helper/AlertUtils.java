package com.emts.fantasycredit.helper;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.emts.fantasycredit.R;

import net.steamcrafted.loadtoast.LoadToast;


/**
 * Created by User on 2016-10-19.
 */

public class AlertUtils {
    public static interface OnAlertButtonClickListener {
        void onAlertButtonClick(boolean isPositiveButton);
    }

    public static void showAlertMessage(Context context, String title, String message, String positiveText,
                                        String negativeText, final OnAlertButtonClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setMessage(Html.fromHtml(message));
        if (!TextUtils.isEmpty(positiveText)) {
            builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (listener != null) {
                        listener.onAlertButtonClick(true);
                    }
                }
            });
        }
        if (!TextUtils.isEmpty(negativeText)) {
            builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (listener != null) {
                        listener.onAlertButtonClick(false);
                    }
                }
            });
        }
//        builder.show();
        Dialog dialog = builder.create();
        try {
            if (Build.VERSION.SDK_INT < 16) {
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                View decorView = dialog.getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                decorView.setSystemUiVisibility(uiOptions);
            }
        } catch (Exception e) {
            Logger.e("showAlertMessage noStatus", e.getMessage() + " ");
        }
        dialog.show();
    }


    public static void showAlertMessage(Context context, boolean cancelable, String title, String message, String positiveText,
                                        String negativeText, final OnAlertButtonClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setCancelable(cancelable);
        builder.setMessage(Html.fromHtml(message));
        if (!TextUtils.isEmpty(positiveText)) {
            builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (listener != null) {
                        listener.onAlertButtonClick(true);
                    }
                }
            });
        }
        if (!TextUtils.isEmpty(negativeText)) {
            builder.setPositiveButton(negativeText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (listener != null) {
                        listener.onAlertButtonClick(false);
                    }
                }
            });
        }
        builder.show();
    }

    public static void showAlertMessage(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setMessage(Html.fromHtml(message));
        builder.show();
    }

    public static void showSnack(Context context, View view, String message) {
        try {
            if (TextUtils.isEmpty(message)) return;
            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            View snackBarView = snackbar.getView();
            TextView tv = snackBarView.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            tv.setGravity(Gravity.CENTER);
            snackBarView.setBackgroundColor(ContextCompat.getColor(context,
                    R.color.colorAccent));
            snackbar.show();
        } catch (Exception e) {
        }
    }

    public static void showToast(Context context, String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Logger.e("alertUtils showToast ex", e.getMessage() + "");
        }
    }

    public static ProgressDialog showProgressDialog(Context context, String message) {
        ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setMessage(message);
        pDialog.show();
        return pDialog;
    }

    public static void hideInputMethod(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
        }
    }

    public static void showInputMethod(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
            }
        } catch (Exception e) {
        }
    }


    public static LoadToast showToastProgress(Context context, String message) {
        LoadToast lProgress = new LoadToast(context);
        lProgress.setText(message);
        lProgress.setProgressColor(context.getResources().getColor(R.color.colorPrimaryDark));
        lProgress.setTextColor(context.getResources().getColor(R.color.green));
        lProgress.setBackgroundColor(context.getResources().getColor(R.color.appGrayDivider));
        // Calculate ActionBar's height
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            int actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,
                    context.getResources().getDisplayMetrics());
            lProgress.setTranslationY(actionBarHeight + 100);
        }
        return lProgress;
    }
}
