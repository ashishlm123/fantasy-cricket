package com.emts.fantasycredit.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by User on 2016-11-02.
 */

public class ImageHelper {

    public static String encodeToBase64(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public static String encodeToBase64Is(String filePath) {
        InputStream inputStream = null;//You can get an inputStream using any IO API
        try {
            inputStream = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            Logger.e("ImageHelper ex1", e.getMessage() + "");
        }
        byte[] bytes;
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            Logger.e("ImageHelper ex2", e.getMessage() + "");
        }
        bytes = output.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public static Bitmap bitmapFromFile(String filePath) {
        return BitmapFactory.decodeFile(filePath);
    }

    public static Bitmap decodeImageFileWithCompression(String filePath, int requiredWidth, int requiredHeight) {
        BitmapFactory.Options optionsBounds = new BitmapFactory.Options();
        optionsBounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, optionsBounds);

        if (requiredHeight == 0) requiredHeight = 320;
        if (requiredWidth == 0) requiredHeight = 320;

        int sampleSize = calculateInSampleSize(optionsBounds, requiredWidth, requiredHeight);

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = sampleSize;
            return BitmapFactory.decodeFile(filePath,
                    options);
        } catch (NullPointerException e) {
            Logger.e("exception lvl 324", e.getMessage() + " ");
        }
        return null;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap compressedBitmapFromFile(String filePath) {
        Bitmap original = BitmapFactory.decodeFile(filePath);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        original.compress(Bitmap.CompressFormat.JPEG, 100, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        return decoded;
    }

    public static Bitmap compressBitmap(Bitmap bitmap){
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        return decoded;
    }

    public static Bitmap resizeHeightWidth(String path) {
        int MAX_HEIGHT = 300;
        int MAX_WIDTH = 300;
        Bitmap bitmap;
        // create the options
        BitmapFactory.Options opts = new BitmapFactory.Options();

        //just decode the file
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, opts);

        //get the original size
        int orignalHeight = opts.outHeight;
        int orignalWidth = opts.outWidth;
        Logger.e("original height:width", orignalHeight + "::" + orignalWidth);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        bitmap = BitmapFactory.decodeFile(path, options);

        float newHeight = orignalHeight;
        float newWidth = orignalWidth;
        //change height of image to 2000 if more than that
        if (orignalHeight > MAX_HEIGHT) {
            newHeight = 300;
//            final float heightRatio = Math.round((float) orignalHeight / (float) MAX_HEIGHT);
            final float heightRatio = (float) orignalHeight / (float) MAX_HEIGHT;
            newWidth = orignalWidth / heightRatio;
            Logger.e("newWidth for height 600", newWidth + " *");

            bitmap = Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
            Logger.e("resize height is greater with ratio :" + heightRatio,
                    "<---------------------------->\nNew Height: " + bitmap.getHeight() + " New Width: " + bitmap.getWidth());
        }
        if (newWidth > MAX_WIDTH) {
//            final float widthRatio = Math.round((float) newWidth / (float) MAX_WIDTH);
            final float widthRatio = (float) newWidth / (float) MAX_WIDTH;
            newWidth = 300;
            newHeight = newHeight / widthRatio;

            bitmap = Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
            Logger.e("newHeight for width 600", newWidth + " *");

            Matrix matrix = new Matrix();

            matrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

            //   bitmap = Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
            Logger.e("resize width is greater with ratio :" + widthRatio,
                    "<---------------------------->\nNew Height: " + bitmap.getHeight() + " New Width: " + bitmap.getWidth());
        }
        return bitmap;
    }

    public static String encodeTobase64VII(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = null;
        try {
            System.gc();
            temp = Base64.encodeToString(b, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            baos = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 50, baos);
            b = baos.toByteArray();
            temp = Base64.encodeToString(b, Base64.DEFAULT);
            Logger.e("EWN", "Out of memory error catched");
        }
        return temp;
    }

    public static void saveImageToPath(String filePath, Bitmap bitmap){
        try {
            FileOutputStream out = new FileOutputStream(filePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
