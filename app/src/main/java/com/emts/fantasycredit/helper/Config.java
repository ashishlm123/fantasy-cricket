package com.emts.fantasycredit.helper;

/**
 * Created by User on 2016-07-07.
 */
public class Config {
    public static String IS_MEGA_LEAGUE = "is_mega_league";
    public static String IS_SUPREME_LEAGUE = "is_supreme_league";
    public static String IS_CASHLESS_LEAGUE = "is_cashless_league";
    public static String VERIFIED = "verified";
    public static String UNVERIFIED = "unverified";
}
