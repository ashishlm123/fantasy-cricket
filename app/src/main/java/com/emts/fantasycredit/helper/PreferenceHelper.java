package com.emts.fantasycredit.helper;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by User on 2016-03-04.
 */
public class PreferenceHelper implements SharedPreferences {
    public static final String APP_SHARED_PREFS = "rubids_prefs";

    public static final String GCM_TOKEN = "gcmToken";
    public static final String IS_LOGIN = "is_login";
    public static final String GCM_TOKEN_UPDATE = "gcm_token_update";

    //tokens
    public static final String TOKEN = "app_user_token";

    //user detail
    public static final String APP_USER_IMG = "app_user_img";
    public static final String APP_USER_ID = "app_user_id";
    public static final String APP_USER_EMAIL = "app_user_email";
    public static final String APP_USER_NAME = "app_user_name";
    public static final String APP_USER_DOB = "app_user_dob";
    public static final String APP_ABOUT_USER = "app_about_user";
    public static final String APP_USER_MOBILE = "app_user_mobile";
    public static final String APP_USER_TYPE = "app_user_type";
    public static final String APP_USER_PROFILE_URL = "app_user_pp_url";
    public static final String APP_CURRENCY = "app_curr";
    public static final String APP_USER_R_POINTS = "r_points";
    public static final String USER_MEMBERSHIP_LEVEL = "membership_level";
    public static final String MEMBERSHIP_LEVEL_JSON_DATA = "membership_json_DATA";
    public static final String AUCTION_TYPE = "auction_type";
    public static final String SITE_INFO = "site_info";
    public static final String CURRENCY_CODE = "currency_code";
    public static final String CURRENCY_SIGN = "currency_sign";
    public static final String USER_BIT_COIN = "user_bit_coin";
    public static final String USER_ALT_COIN = "user_alt_coin";
    public static final String BIDS = "bids";

    public static final String APP_USER_FIRST_NAME = "app_user_first_name";
    public static final String APP_USER_ADDRESS = "app_user_address";
    public static final String APP_USER_CITY = "app_user_city";
    public static final String APP_USER_LAST_NAME = "app_user_last_name";
    public static final String APP_USER_BID_CREDITS = "app_user_bid_credits";
    public static final String APP_USER_BONUS_CREDITS = "app_user_bonus_credits";
    public static final String APP_USER_PROFILE_COMPLETE = "app_user_profile_complete";
    public static final String SYSTEM_EMAIL = "system_email";
    public static final String CONTACT_EMAIL = "contact_email";

    public static final String FB_URL = "fb_url";
    public static final String FB_APP_ID = "fb_app_id";
    public static final String TWITTER_URL = "twitter_url";
    public static final String TWITTER_APP_KEY = "twitter_api_key";
    public static final String TWITTER_APP_SECRET = "twitter_app_secret";
    public static final String GOOGLE_URL = "google_url";
    public static final String GOOGLE_API_KEY = "google_api_key";
    public static final String INSTA_URL = "insta_url";
    public static final String LINKEDIN_URL = "linkedin_url";
    public static final String YOUTUBE_URL = "youtube_url";
    public static final String REVEAL_TIMER = "reveal_auc_show_time";
    public static final String REVEAL_BUY_NOW_TIMER = "reveal_auc_buy_now_time";

    //from cache
    public static final String DEALS_FROM_CACHE = "deals_from_cache";
    public static final String GAMES_FROM_CACHE = "games_from_cache";
    public static final String CATEGORY_ARRAY = "category_array";

    private SharedPreferences prefs;
    private static PreferenceHelper helper;

    public static PreferenceHelper getInstance(Context context) {
        if (helper != null) {
            return helper;
        } else {
            helper = new PreferenceHelper(context);
            return helper;
        }

    }

    private PreferenceHelper(Context context) {
        prefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }

    @Override
    public Map<String, ?> getAll() {
        return prefs.getAll();
    }

    @Override
    public String getString(String s, String s2) {
        return prefs.getString(s, s2);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public Set<String> getStringSet(String s, Set<String> strings) {
        return prefs.getStringSet(s, strings);
    }

    @Override
    public int getInt(String s, int i) {
        return prefs.getInt(s, i);
    }

    @Override
    public long getLong(String s, long l) {
        return prefs.getLong(s, l);
    }

    @Override
    public float getFloat(String s, float v) {
        return prefs.getFloat(s, v);
    }

    @Override
    public boolean getBoolean(String s, boolean b) {
        return prefs.getBoolean(s, b);
    }

    @Override
    public boolean contains(String s) {
        return prefs.contains(s);
    }

    @Override
    public Editor edit() {
        return prefs.edit();
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    public boolean isLogin() {
        return prefs.getBoolean(IS_LOGIN, false);
    }

    public boolean isProfileComplete() {
        return prefs.getBoolean(APP_USER_PROFILE_COMPLETE, false);
    }

    public String getUserType() {
        return prefs.getString(APP_USER_TYPE, "");
    }

    public String getUserId() {
        return prefs.getString(APP_USER_ID, "-1");
    }

    public String getUserEmail() {
        return prefs.getString(APP_USER_EMAIL, "");
    }

    public String getUserAddress() {
        return prefs.getString(APP_USER_ADDRESS, "");
    }

    public String getUserCity() {
        return prefs.getString(APP_USER_CITY, "");
    }

    public String getToken() {
        return prefs.getString(TOKEN, "");
    }

    public String getCurrency() {
        return prefs.getString(APP_CURRENCY, "$");
    }

    public HashMap<String, String> getMembershipInfo(String id) {
        String savedMembershipObject = prefs.getString(MEMBERSHIP_LEVEL_JSON_DATA, "");
        try {
            JSONObject memLevel = new JSONObject(savedMembershipObject);
            if (memLevel.getBoolean("status")) {
                JSONArray membershipArray = memLevel.getJSONArray("data");
                for (int i = 0; i < membershipArray.length(); i++) {
                    JSONObject eachLevel = membershipArray.getJSONObject(i);
                    if (eachLevel.getString("id").equals(id)) {
                        HashMap<String, String> memMap = new HashMap<>();
                        memMap.put("id", id);
                        memMap.put("name", eachLevel.getString("name"));
                        memMap.put("gpoints", eachLevel.getString("game_points"));
                        return memMap;
                    }
                }
            }
        } catch (JSONException ex) {
            Logger.e("membershipJsonTask ex ", ex.getMessage());
        }
        return null;
    }
}
