package com.emts.fantasycredit.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by User on 2016-10-19.
 */

public class DateUtils {

    //convert form dd/MM/YYYY HH:mm to dd MMM YYYY, hh:mm a
    public static String convertDate(String dateToConvert) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date testDate = null;
        try {
            testDate = sdf.parse(dateToConvert);
        } catch (Exception ex) {
            Logger.e("DateUtils ex 12", ex.getMessage() + "");
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        return formatter.format(testDate);
    }

    public static String convertUTC(String dateToConvert) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        SimpleDateFormat output = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date = sdf.parse(dateToConvert);
            return output.format(date);
        } catch (Exception e) {
            Logger.e("dateConvert ex ", e.getMessage());
        }
        return dateToConvert;

        //String dateStr = "2016-09-17T08:14:03+00:00";
//        String s = dateStr.replace("Z", "+00:00");
//        s = s.substring(0, 22) + s.substring(23);
//        Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(s);
//        Timestamp createdOn = new Timestamp(date.getTime());
//        mcList.setCreated_on(createdOn);
    }

    public static String convertDate1(String dateToConvert1) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss-Z");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd MMM yyyy");
        try {
            Date date = dateFormat.parse(dateToConvert1);
            String out = dateFormat2.format(date);
            Logger.e("date", out);
            return out;
        } catch (ParseException e) {
            Logger.e("dateConvert ex ", e.getMessage());
        }
        return dateToConvert1;
    }

    //convert form dd/MM/YYYY to dd MMM YYYY
    public static String convertDateWithoutTime(String dateToConvert) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date testDate = null;
        try {
            testDate = sdf.parse(dateToConvert);
        } catch (Exception ex) {
            Logger.e("DateUtils ex 12", ex.getMessage() + "");
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        return formatter.format(testDate);
    }

    public static String convertTime(String _24HourTime) {
        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);
            return _12HourSDF.format(_24HourDt);
        } catch (Exception e) {
            return "";
        }
    }


    public static String convertTimeZone(String date, String timezone) {
        try {
            if (date.equals("null")) {
                return "...";
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd-yyyy HH:mm");
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date convertedDate = sdf.parse(date);
                sdf1.setTimeZone(TimeZone.getTimeZone(timezone));
                return sdf1.format(convertedDate);
            }
        } catch (Exception e) {
            Logger.e("converttimezone ex", e.getMessage() + " ");
            return "...";
        }
    }

    public static String getPostTimeAgo(String createdDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date dateCreated = sdf.parse(createdDate);
            Date dateNow = new Date();
            dateNow.setTime(System.currentTimeMillis());
//            Logger.e("created VS current time", dateCreated.getTime() + " VS " + dateNow.getTime());

            long difference = dateNow.getTime() - dateCreated.getTime();
//            Logger.e("difference", difference + "");
//            int days = (int) (difference / (1000*60*60*24));
//            int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
//            int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
//            Logger.e("dd, hh, mm", days + " : " + hours + " : " + min);
//            hours = (hours < 0 ? -hours : hours);

//            if (days > 0){
//                return days + " days ago";
//            }else if (hours > 0){
//                return hours + " hours ago";
//            }else if (min > 0){
//                return min + " mins ago";
//            }else{
//                return "now";
//            }
            long seconds = difference / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (dateCreated.before(dateNow)) {

//                Logger.e("oldDate", "is previous date");
//                Logger.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
//                        + " hours: " + hours + " days: " + days);
            }

            if (days > 0) {
                if (days == 1)
                    return days + " day ago";
                return days + " days ago";
            } else if (hours > 0) {
                if (hours == 1)
                    return hours + " hour ago";
                return hours + " hours ago";
            } else if (minutes > 0) {
                return minutes + " mins ago";
            } else if (seconds > 0) {
                return seconds + "sec ago";
            } else {
                return "now";
            }


        } catch (ParseException ex) {
            Logger.e("date diff pares ex", ex.getMessage() + "");
            return "now";
        } catch (NullPointerException e) {
            Logger.e("date diff ex 11", e.getMessage() + "");
            return "";
        }
    }

    //yyyy-MM-dd hh:mm:ss
    public static String getPostTimeAgoV2(String createdDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date dateCreated = sdf.parse(createdDate);

//            //here datecreated is UTC date convert it to device timezone
//            Calendar mCalendar = new GregorianCalendar();
//            TimeZone mTimeZone = mCalendar.getTimeZone();
//            int mGMTOffset = mTimeZone.getRawOffset();
//            System.out.printf("GMT offset is %s hours", TimeUnit.HOURS.convert(mGMTOffset, TimeUnit.MILLISECONDS));

            Date dateNow = new Date();
            dateNow.setTime(System.currentTimeMillis());
//            Logger.e("created VS current time", dateCreated.getTime() + " VS " + dateNow.getTime());

            long difference = dateNow.getTime() - dateCreated.getTime();
//            Logger.e("difference", difference + "");
//            int days = (int) (difference / (1000*60*60*24));
//            int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
//            int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
//            Logger.e("dd, hh, mm", days + " : " + hours + " : " + min);
//            hours = (hours < 0 ? -hours : hours);

//            if (days > 0){
//                return days + " days ago";
//            }else if (hours > 0){
//                return hours + " hours ago";
//            }else if (min > 0){
//                return min + " mins ago";
//            }else{
//                return "now";
//            }


            long seconds = difference / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (dateCreated.before(dateNow)) {

//                Logger.e("oldDate", "is previous date");
//                Logger.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
//                        + " hours: " + hours + " days: " + days);
            }

            if (days > 0) {
                if (days > 6) {
                    return convertDate(createdDate);
                }
                return days + " days ago";
            } else if (hours > 0) {
                return hours + " hours ago";
            } else if (minutes > 0) {
                return minutes + " mins ago";
            } else if (seconds > 0) {
                return seconds + " sec ago";
            } else {
                return "now";
            }


        } catch (ParseException ex) {
            Logger.e("date diff pares ex", ex.getMessage() + "");
            return "now";
        }
    }

    public static String getPostTimeAgoV3(String createdDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date dateCreated = sdf.parse(createdDate);
            Date dateNow = new Date();
            dateNow.setTime(System.currentTimeMillis());

            long difference = dateNow.getTime() - dateCreated.getTime();

            long seconds = difference / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (dateCreated.before(dateNow)) {
            }

            if (days > 0) {
                if (days == 1) {
                    return days + " day ago";
                } else {
                    return days + " days ago";
                }
            } else if (hours > 0) {
                if (hours == 1)
                    return hours + " hour ago";
                return hours + " hours ago";
            } else if (minutes > 0) {
                return minutes + " mins ago";
            } else if (seconds > 0) {
                return seconds + " sec ago";
            } else {
                return "";
            }


        } catch (ParseException ex) {
            Logger.e("date diff pares ex", ex.getMessage() + "");
            return "now";
        } catch (NullPointerException e) {
            Logger.e("date diff ex 11", e.getMessage() + "");
            return "now";
        }
    }

    public static boolean dateIsAfter(int day, int month, int year) {
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.set(Calendar.DATE, day);
        selectedDate.set(Calendar.MONTH, month);
        selectedDate.set(Calendar.YEAR, year);

        Calendar today = Calendar.getInstance();
        today.setTime(new Date());

        return selectedDate.after(today);
    }

    public static Date getTime(String DateNTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date testDate = null;

        try {
            testDate = sdf.parse(DateNTime);

        } catch (Exception ex) {
            Logger.e("DateUtils ex 12", ex.getMessage() + "");
        }


        return testDate;

    }
}