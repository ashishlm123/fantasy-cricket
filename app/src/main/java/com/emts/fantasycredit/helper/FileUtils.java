package com.emts.fantasycredit.helper;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

/**
 * Created by User on 2017-05-09.
 */

public class FileUtils {
    public static final String FILE_CACHE_GAME = "fileCacheGame";
    public static final String FILE_CACHE_DEALS = "fileCacheDeals";


    static String getFileName(String path) {
        return path.substring(path.lastIndexOf(File.separator) + 1);
    }

    static byte[] readFile(String filePath) throws IOException {
        File file = new File(filePath);
        // Open file
        try (RandomAccessFile f = new RandomAccessFile(file, "r")) {
            // Get and check length
            long longlength = f.length();
            int length = (int) longlength;
            if (length != longlength)
                throw new IOException("File size >= 2 GB");
            // Read file and return data
            byte[] data = new byte[length];
            f.readFully(data);
            return data;
        }
    }

    public static void writeToCache(Context context, String fName, String fContent) {
        File file = new File(context.getCacheDir(), fName);
        FileWriter fw = null;
        try {
            fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(fContent);
            bw.close();
        } catch (IOException e) {
            Logger.e("cache write ex", e.getMessage() + " ");
        }
    }

    public static String readFromCache(Context context, String fName) {
        try {
            StringBuilder fData = new StringBuilder("");
            FileInputStream fis = new FileInputStream(new File(context.getCacheDir(), fName));
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader buffreader = new BufferedReader(isr);

            String readString = buffreader.readLine();
            while (readString != null) {
                fData.append(readString);
                readString = buffreader.readLine();
            }

            isr.close();
            return fData.toString();
        } catch (FileNotFoundException e) {
            Logger.e("cache read ex", e.getMessage() + " ");
        } catch (IOException e) {
            Logger.e("cache read ex 2", e.getMessage() + " ");
        }
        return "";
    }

    public static void clearCahe(Context context, String fName) throws IOException, ClassNotFoundException {
        File file = new File(context.getCacheDir(), fName);
        if (file.delete()) {
            Logger.e("file deleted", "success");
        } else {
            Logger.e("file deleted", "failed");
        }
    }

    public static boolean ifCacheFileExist(Context context, String fName){
        File file = new File(context.getCacheDir(), fName);
        return file.exists();
    }


}
