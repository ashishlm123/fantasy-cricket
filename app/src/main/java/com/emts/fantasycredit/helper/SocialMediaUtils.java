package com.emts.fantasycredit.helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by Srijana on 1/10/2017.
 */

public class SocialMediaUtils {
    public static void shareOnSocialMedia(Context context, String message) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, message);
        share.putExtra(Intent.EXTRA_SUBJECT, "RUBIDS");

        context.startActivity(Intent.createChooser(share, "Share with:"));
    }

    public static void openSocialPages(Context context, String pageUrl) {
        Logger.e("openSocialPages url", pageUrl + " **");
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(pageUrl));
            context.startActivity(intent);
        } catch (Exception e) {
            Logger.e("activity not found", e.getMessage() + " ");
        }
    }
}
