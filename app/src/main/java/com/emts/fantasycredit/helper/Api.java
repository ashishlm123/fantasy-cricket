package com.emts.fantasycredit.helper;

/**
 * Created by theone on 10/18/2016.
 */

public class Api {

    private static Api api;

    public static Api getInstance() {
        if (api != null) {
            return api;
        } else {
            api = new Api();
            return api;
        }
    }

    private String getHost() {
        return "http://spartansapi.sciflare.com";
    }

    public String registrationUrl = getApi() + "signup";
    public final String loginUrl = getApi() + "signin";
    public final String fbRegisterUrl = getApi() + "";
    public final String googleRegisterUrl = getApi() + "";

    public final String getFixtureUrl = getApi() + "fixtures";
    //    public final String getFixtureUrl = "http://spartans.sciflare.com/fixtureleague";//todo remove
    public final String getFixtureLeagueUrl = getApi() + "fixtureleague";
    public String getMatchInfoUrl = getApi() + "addteamdata";
    public String createLeague = getApi() + " ";

    public final String getMyLeagueUrl = getApi() + "myleagues";
    public final String teamSelectionUrl = getApi() + "teamselection";
    public final String accountBalanceInfoUrl = getApi() + "getbalance";
    public final String changePasswordUrl = getApi() + "changepassword";
    public final String forgotPasswordUrl = getApi() + "forgotpassword";
    public final String verifyAccountUrl = getApi() + "verifyaccount";
    public final String referralTrackingUrl = getApi() + "trackreferal";
    public final String getUserDetailUrl = getApi() + "editprofiledata";
    public final String updateUserProfileUrl = getApi() + "editprofile";
    public final String playerStatistics = getApi() + "playerstatistics";

    public final String createTeamUrl = getApi() + "addteam";
    public final String getMyTeamsUrl = getApi() + "getmyteams";
    public final String getTeamSelectionUrl = getApi() + "teamselection";

    private String getApi() {
        return getHost() + "/";
    }


}