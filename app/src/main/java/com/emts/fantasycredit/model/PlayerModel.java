package com.emts.fantasycredit.model;

import java.io.Serializable;

public class PlayerModel implements Serializable {
    public static final String WICKET_KEEPER = "3";
    public static final String BATSMAN = "1";
    public static final String ALL_ROUNDER = "4";
    public static final String BOWLER = "2";
    private String playerId;
    private String playerName;
    private boolean isCaptain;
    private boolean isVC;
    private String playingTeam;
    private String playingTeamId;
    private String image;
    private String fantasyPoints;
    private String playerType;
    private boolean isSelected;

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public boolean isCaptain() {
        return isCaptain;
    }

    public void setCaptain(boolean captain) {
        isCaptain = captain;
    }

    public boolean isVC() {
        return isVC;
    }

    public void setVC(boolean VC) {
        isVC = VC;
    }

    public String getPlayingTeam() {
        return playingTeam;
    }

    public void setPlayingTeam(String playingTeam) {
        this.playingTeam = playingTeam;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPlayingTeamId() {
        return playingTeamId;
    }

    public void setPlayingTeamId(String playingTeamId) {
        this.playingTeamId = playingTeamId;
    }

    public boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getPlayerType() {
        return playerType;
    }

    public void setPlayerType(String playerType) {
        this.playerType = playerType;
    }

    public String getFantasyPoints() {
        return fantasyPoints;
    }

    public void setFantasyPoints(String fantasyPoints) {
        this.fantasyPoints = fantasyPoints;
    }
}
