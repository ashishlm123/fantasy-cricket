package com.emts.fantasycredit.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Team implements Serializable {
    private String teamID;
    private ArrayList<PlayerModel> players;
    private boolean isTeamSelected;


    public String getTeamID() {
        return teamID;
    }

    public void setTeamID(String teamID) {
        this.teamID = teamID;
    }

    public ArrayList<PlayerModel> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<PlayerModel> players) {
        this.players = players;
    }

    public boolean isTeamSelected() {
        return isTeamSelected;
    }

    public void setTeamSelected(boolean teamSelected) {
        isTeamSelected = teamSelected;
    }
}
