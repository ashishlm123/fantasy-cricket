package com.emts.fantasycredit.model;

import java.io.Serializable;

/**
 * Created by Admin on 3/22/2018.
 */

public class Game implements Serializable {
    private String leagueId;
    private String team1name;
    private String team1Logo;
    private String team2name;
    private String team2Logo;
    private String leagueName;
    private String leagueDate;
    private String fixtureId;
    private String team1ShortCode;
    private String team2ShortCode;


    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }


    public String getTeam1name() {
        return team1name;
    }

    public void setTeam1name(String team1name) {
        this.team1name = team1name;
    }

    public String getTeam1Logo() {
        return team1Logo;
    }

    public void setTeam1Logo(String team1Logo) {
        this.team1Logo = team1Logo;
    }

    public String getTeam2name() {
        return team2name;
    }

    public void setTeam2name(String team2name) {
        this.team2name = team2name;
    }

    public String getTeam2Logo() {
        return team2Logo;
    }

    public void setTeam2Logo(String team2Logo) {
        this.team2Logo = team2Logo;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public String getLeagueDate() {
        return leagueDate;
    }

    public void setLeagueDate(String leagueDate) {
        this.leagueDate = leagueDate;
    }

    public String getFixtureId() {
        return fixtureId;
    }

    public void setFixtureId(String fixtureId) {
        this.fixtureId = fixtureId;
    }

    public String getTeam1ShortCode() {
        return team1ShortCode;
    }

    public void setTeam1ShortCode(String team1ShortCode) {
        this.team1ShortCode = team1ShortCode;
    }

    public String getTeam2ShortCode() {
        return team2ShortCode;
    }

    public void setTeam2ShortCode(String team2ShortCode) {
        this.team2ShortCode = team2ShortCode;
    }
}
