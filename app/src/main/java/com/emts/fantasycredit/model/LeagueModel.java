package com.emts.fantasycredit.model;

import java.io.Serializable;

public class LeagueModel implements Serializable {
    private String entryFee;
    private String leagueSize;
    private String leagueMember;
    private String totalWinning;
    private String leagueId;
    private String gainers;


    public String getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(String entryFee) {
        this.entryFee = entryFee;
    }

    public String getLeagueSize() {
        return leagueSize;
    }

    public void setLeagueSize(String leagueSize) {
        this.leagueSize = leagueSize;
    }

    public String getLeagueMember() {
        return leagueMember;
    }

    public void setLeagueMember(String leagueMember) {
        this.leagueMember = leagueMember;
    }

    public String getGainers() {
        return gainers;
    }

    public void setGainers(String gainers) {
        this.gainers = gainers;
    }

    public String getTotalWinning() {
        return totalWinning;
    }

    public void setTotalWinning(String totalWinning) {
        this.totalWinning = totalWinning;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }
}
