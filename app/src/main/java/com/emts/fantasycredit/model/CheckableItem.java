package com.emts.fantasycredit.model;

import java.io.Serializable;

/**
 * Created by User on 2018-03-22.
 */

public class CheckableItem implements Serializable {
    private boolean isChecked;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
