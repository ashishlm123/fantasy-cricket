package com.emts.fantasycredit.model;

import java.io.Serializable;

public class TransactionModel extends User implements Serializable{ private String transId;
    private String transType;
    private String transAmt;
    private String transTime;
    private String transMatchID;
    private String transLeagueID;
    private String transGameType;
    private String transLeagueName;
    private String transTypeText;
    private String transDebitCredit;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getTransAmt() {
        return transAmt;
    }

    public void setTransAmt(String transAmt) {
        this.transAmt = transAmt;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

    public String getTransMatchID() {
        return transMatchID;
    }

    public void setTransMatchID(String transMatchID) {
        this.transMatchID = transMatchID;
    }

    public String getTransLeagueID() {
        return transLeagueID;
    }

    public void setTransLeagueID(String transLeagueID) {
        this.transLeagueID = transLeagueID;
    }

    public String getTransGameType() {
        return transGameType;
    }

    public void setTransGameType(String transGameType) {
        this.transGameType = transGameType;
    }

    public String getTransLeagueName() {
        return transLeagueName;
    }

    public void setTransLeagueName(String transLeagueName) {
        this.transLeagueName = transLeagueName;
    }

    public String getTransTypeText() {
        return transTypeText;
    }

    public void setTransTypeText(String transTypeText) {
        this.transTypeText = transTypeText;
    }

    public String getTransDebitCredit() {
        return transDebitCredit;
    }

    public void setTransDebitCredit(String transDebitCredit) {
        this.transDebitCredit = transDebitCredit;
    }
}
