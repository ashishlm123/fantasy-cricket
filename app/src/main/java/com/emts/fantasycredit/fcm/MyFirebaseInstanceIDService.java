package com.emts.fantasycredit.fcm;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import com.emts.fantasycredit.helper.Api;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.helper.NetworkUtils;
import com.emts.fantasycredit.helper.PreferenceHelper;
import com.emts.fantasycredit.helper.VolleyHelper;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by User on 2016-09-30.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    PreferenceHelper prefsHelper;

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        prefsHelper = PreferenceHelper.getInstance(getApplicationContext());
        Logger.e(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        sendRegistrationToServer(refreshedToken);

    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        if (prefsHelper.isLogin()) {
            if (NetworkUtils.isInNetwork(this)) {
                updateDeviceTokenTask(this);
            } else {
                prefsHelper.edit().putBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, true).commit();
            }
        }
    }

      public void updateDeviceTokenTask(Context context) {
        prefsHelper = PreferenceHelper.getInstance(MyFirebaseInstanceIDService.this);
        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        JSONObject postParams = new JSONObject();
//        postParams.put("token", prefsHelper.getToken());
//        postParams.put("user_id", String.valueOf(prefsHelper.getUserId()));
//        if (!TextUtils.isEmpty(FirebaseInstanceId.getInstance().getToken()) || !FirebaseInstanceId.getInstance().getToken().equals(null)) {
//            postParams.put("device_id", FirebaseInstanceId.getInstance().getToken());
//            Logger.e("devic_update", postParams.get("device_id"));
//        }


        vHelper.addVolleyRequestListeners("", Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                prefsHelper.edit().putBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, false).commit();
                            } else {
                                prefsHelper.edit().putBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, true).commit();

                            }
                        } catch (JSONException e) {

                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        prefsHelper.edit().putBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, true).commit();
                    }
                }, " updateDeviceTokenTask");


    }
}