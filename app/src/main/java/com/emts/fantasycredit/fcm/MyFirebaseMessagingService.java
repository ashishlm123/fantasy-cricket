package com.emts.fantasycredit.fcm;

import com.emts.fantasycredit.helper.Logger;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by User on 2016-09-30.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]


        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Logger.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Logger.e(TAG, "Message data payload: " + remoteMessage.getData());
//            sendNotification(remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Logger.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

//    private void sendNotification(Map<String, String> data) {
//
//        PreferenceHelper preferenceHelper = PreferenceHelper.getInstance(this);
//
//        try {
//            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
//
//            String title = data.get("title");
//
//            String body = data.get("message");
////            String notify_type = data.get("notify_type");
////            int userId = 0;
//
//            String messageFrom = data.get("open_type");
//            String sendby = data.get("sent_by");
//            String fileDir = data.get("dir");
//            Notification notification = null;
//            Incident incident = null;
//            Report report = null;
//            String jsonData = data.get("data");
//            Logger.e("FRM : DATA", "FRM : " + messageFrom + "\nDATA : " + jsonData);
//            if (messageFrom.equalsIgnoreCase("super_user")) {
//
////                if (sendby.equalsIgnoreCase("admin")) {
//
////                } else {
//                notification = new Notification();
//                JSONObject jsonObject = new JSONObject(jsonData);
//                notification.setId(jsonObject.getString("id"));
//                notification.setTitle(jsonObject.getString("type_name"));
//                notification.setDate(jsonObject.getString("created_date"));
//                notification.setImageUrl(fileDir + jsonObject.getString("type_image"));
//                notification.setMesg(jsonObject.getString("message"));
//                String audioFileName = jsonObject.getString("voice_message_saved");
//                if (!TextUtils.isEmpty(audioFileName) && !audioFileName.equalsIgnoreCase("null")) {
//                    notification.setAudioUrl(fileDir);
//                } else {
//                    notification.setAudioUrl("");
//                }
//                notification.setIncident_id(jsonObject.getString("incident_id"));
//                String resId = jsonObject.getString("response_id");
//                notification.setResponded(!(resId.equals("0") || resId.equalsIgnoreCase("null")));
//                notification.setRespondType(jsonObject.getString("response_type"));
////                }
//                report = new Report();
//                if (jsonObject.has("type_id")) {
//                    report.setId(jsonObject.getString("type_id"));
//                }
//                report.setTypeName(jsonObject.getString("type_name"));
//
//            } else if (messageFrom.equalsIgnoreCase("user")) {
//                incident = new Incident();
//                JSONObject eachObj = new JSONObject(jsonData);
//                incident.setId(eachObj.getString("id"));
//                incident.setTypeId(eachObj.getString("type_id"));
//
//
//                if (!TextUtils.isEmpty(eachObj.getString("type_image"))) {
//                    incident.setImageUrl(fileDir + eachObj.getString("type_image"));
//                }
//                if (!TextUtils.isEmpty(eachObj.getString("file_saved_image"))) {
//                    incident.setMedia(fileDir + eachObj.getString("file_saved_image"));
//                }
//                if (!TextUtils.isEmpty(eachObj.getString("file_saved_voice"))) {
//                    incident.setAudioUrl(fileDir + eachObj.getString("file_saved_voice"));
////                    incident.setAudioUrl("http://nepaimpressions.com/dev/alacer/themes/admin//images//incidents/sample.mp3");
//                }
//
//                incident.setMesg(eachObj.getString("message"));
//                incident.setScale(eachObj.getString("scale"));
//                incident.setStatus(eachObj.getString("status"));
//                incident.setTypeName(eachObj.getString("type_name"));
//
//                incident.setCreatedDate(eachObj.getString("created_date"));
//                incident.setLaunchedDate(eachObj.getString("launched_date"));
//                incident.setName(eachObj.getString("action"));
//                incident.setLocation(eachObj.getString("location"));
//                incident.setReportedBy(eachObj.getString("reported_by"));
//                incident.setLatitude(eachObj.getString("latitude"));
//                incident.setLongitude(eachObj.getString("lognitude"));
//                Logger.e("user send notification", messageFrom + " *****");
//            }
//
//
//            //check if login to app else open splash
//
//            if (preferenceHelper.isLogin()) {
//                if (messageFrom.equalsIgnoreCase("super_user") && notification != null
//                        && preferenceHelper.getUserType().equals(PreferenceHelper.APP_USER_USER)) {
//
//
////                    if (notification.getIncident_id().equals("0")) {
////                        intent = new Intent(getApplicationContext(), MessageDetailActivity.class);
////                        intent.putExtra("notification", notification);
////                    } else {
//                        intent.putExtra("notification", notification);
////                    }
//
//                } else if (messageFrom.equalsIgnoreCase("user") && incident != null
//                        && preferenceHelper.getUserType().equals(PreferenceHelper.APP_USER_SUPER_USER)) {
//                    intent = new Intent(getApplicationContext(), IncidentReportDetail.class);
//                    intent.putExtra("incident", incident);
//                } else if (sendby.equalsIgnoreCase("admin") && messageFrom.equalsIgnoreCase("super_user") && report != null
//                        && preferenceHelper.getUserType().equals(PreferenceHelper.APP_USER_SUPER_USER)) {
//                    intent = new Intent(getApplicationContext(), NotificationStatusListing.class);
//                    intent.putExtra("report", report);
//                    Logger.e("Message", "u r here");
//                }
//            } else {
//                return;
//            }
//
//            intent.putExtra("from_push", true);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                    PendingIntent.FLAG_ONE_SHOT);
//
//            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.mipmap.app_icon)
//                    .setContentTitle(title)
//                    .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
//                    .setContentText(body)
//                    .setAutoCancel(true)
//                    .setSound(defaultSoundUri)
//                    .setContentIntent(pendingIntent);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                notificationBuilder.setSmallIcon(R.drawable.icon_noti);
//            }
//
//            NotificationManager notificationManager =
//                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//
////            notificationManager.notify(notiTag, notificationId, notificationBuilder.build());
//
//            int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
////            notificationManager.notify(notify_type, m /* ID of notification */, notificationBuilder.build());
//            notificationManager.notify(messageFrom, m /* ID of notification */, notificationBuilder.build());
//
//        } catch (Exception e) {
//            Logger.e(TAG + "data2 ex", e.getMessage() + " ");
//        }
//    }

}