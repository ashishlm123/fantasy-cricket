package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.model.PlayerModel;

import java.util.ArrayList;

public class CaptainVCAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<PlayerModel> playerLists;
    private final int ITEM_TYPE_HEADER = 12;
    private final int ITEM_TYPE_PLAYER = 34;
    public static final String TYPE_HEADER_DECIDER = "-9803681065";

    private RecyclerViewItemClickListerner recyclerViewItemClickListerner;

    int prevCapPos = -1;
    int prevVCPos = -1;

    public CaptainVCAdapter(Context context, ArrayList<PlayerModel> playerLists) {
        this.context = context;
        this.playerLists = playerLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE_HEADER) {
            return new ViewHolderHeader(LayoutInflater.from(context).inflate(R.layout.item_pick_c_vc_header,
                    parent, false));
        } else {
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_player, parent,
                    false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        PlayerModel player = playerLists.get(position);
        if (getItemViewType(position) == ITEM_TYPE_PLAYER) {
            ViewHolder viewHolder = (ViewHolder) holder;
            if (!TextUtils.isEmpty(player.getImage())) {
                Glide.with(context).load(player.getImage()).into(viewHolder.playerImage);
            }
            viewHolder.tvName.setText(player.getPlayerName());
            viewHolder.tvPoints.setText(" | " + player.getFantasyPoints() + " Points");

            if (player.isCaptain()) {
                viewHolder.tvCaptain.setBackgroundResource(R.drawable.bg_accent_circle);
                viewHolder.tvViceCaptain.setBackgroundResource(R.drawable.bg_circle_dark_grey);

                viewHolder.points1x.setVisibility(View.GONE);
                viewHolder.points2x.setVisibility(View.VISIBLE);
            } else if (player.isVC()) {
                viewHolder.tvViceCaptain.setBackgroundResource(R.drawable.bg_accent_circle);
                viewHolder.tvCaptain.setBackgroundResource(R.drawable.bg_circle_dark_grey);

                viewHolder.points2x.setVisibility(View.GONE);
                viewHolder.points1x.setVisibility(View.VISIBLE);
            } else {
                viewHolder.tvViceCaptain.setBackgroundResource(R.drawable.bg_circle_dark_grey);
                viewHolder.tvCaptain.setBackgroundResource(R.drawable.bg_circle_dark_grey);

                viewHolder.points2x.setVisibility(View.GONE);
                viewHolder.points1x.setVisibility(View.GONE);
            }

        } else {
            ViewHolderHeader viewHolderHeader = (ViewHolderHeader) holder;
            switch (player.getPlayerType()) {
                case PlayerModel.WICKET_KEEPER:
                    viewHolderHeader.tvHeader.setText("Wicket Keeper");
                    break;
                case PlayerModel.BATSMAN:
                    viewHolderHeader.tvHeader.setText("Batsman");
                    break;
                case PlayerModel.ALL_ROUNDER:
                    viewHolderHeader.tvHeader.setText("All Rounder");
                    break;
                case PlayerModel.BOWLER:
                    viewHolderHeader.tvHeader.setText("Bowler");
                    break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (playerLists.get(position).getPlayerId().equals(TYPE_HEADER_DECIDER)) {
            return ITEM_TYPE_HEADER;
        } else {
            return ITEM_TYPE_PLAYER;
        }
    }

    @Override
    public int getItemCount() {
        return playerLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView playerImage;
        TextView tvName, tvTeams, tvPoints, tvCaptain, tvViceCaptain;
        RelativeLayout points2x, points1x;

        public ViewHolder(View itemView) {
            super(itemView);

            playerImage = itemView.findViewById(R.id.player_icon);
            tvName = itemView.findViewById(R.id.tv_player_name);
            tvTeams = itemView.findViewById(R.id.tv_team_country);
            tvPoints = itemView.findViewById(R.id.tv_player_points);
            tvCaptain = itemView.findViewById(R.id.btn_captain);
            tvViceCaptain = itemView.findViewById(R.id.btn_vice_caption);

            points2x = itemView.findViewById(R.id.lay_2x_points_holder);
            points1x = itemView.findViewById(R.id.lay_1_5x_points_holder);

            tvCaptain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PlayerModel playerModel = playerLists.get(getLayoutPosition());
                    playerModel.setCaptain(true);
                    playerModel.setVC(false);
                    notifyItemChanged(getLayoutPosition());

                    if (prevCapPos > -1 && prevCapPos != getLayoutPosition()) {
                        playerLists.get(prevCapPos).setCaptain(false);
                        notifyItemChanged(prevCapPos);
                    }

                    prevCapPos = getLayoutPosition();
                    recyclerViewItemClickListerner.onRecyclerViewItemClickListener(view, -1);
                }
            });

            tvViceCaptain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PlayerModel playerModel = playerLists.get(getLayoutPosition());
                    playerModel.setCaptain(false);
                    playerModel.setVC(true);
                    notifyItemChanged(getLayoutPosition());

                    if (prevVCPos > -1 && prevVCPos != getLayoutPosition()) {
                        playerLists.get(prevVCPos).setVC(false);
                        notifyItemChanged(prevVCPos);
                    }

                    prevVCPos = getLayoutPosition();
                    recyclerViewItemClickListerner.onRecyclerViewItemClickListener(view, -1);
                }
            });

        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        TextView tvHeader;

        public ViewHolderHeader(View itemView) {
            super(itemView);

            tvHeader = itemView.findViewById(R.id.tvHeader);
        }
    }

    public interface RecyclerViewItemClickListerner {
        void onRecyclerViewItemClickListener(View view, int pos);
    }

    public void setRecyclerViewItemClickListerner(RecyclerViewItemClickListerner recyclerViewItemClickListerner) {
        this.recyclerViewItemClickListerner = recyclerViewItemClickListerner;
    }

}
