package com.emts.fantasycredit.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.fantasycredit.activity.TeamDetailActivity;
import com.emts.fantasycredit.model.User;
import com.emts.fantasycredit.R;

import java.util.ArrayList;

/**
 * Created by Admin on 3/22/2018.
 */

public class PlayerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<User> playerList;

    public PlayerAdapter(Context context, ArrayList<User> userList) {
        this.context = context;
        this.playerList = userList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_player_list, parent, false));
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        User user = playerList.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;
        if (position == 0) {
            viewHolder.tvName.setTextColor(context.getResources().getColor(R.color.colorAccentMid));
            viewHolder.tvRank.setTextColor(context.getResources().getColor(R.color.colorAccentMid));
            viewHolder.tvPoint.setTextColor(context.getResources().getColor(R.color.colorAccentMid));
            viewHolder.tvPrize.setTextColor(context.getResources().getColor(R.color.colorAccentMid));
        }
        if (position % 2 == 0) {
            viewHolder.itemView.setBackgroundColor(Color.WHITE);
        } else {
            viewHolder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_gray));
        }

    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvRank, tvPoint, tvPrize;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_username);
            tvRank = itemView.findViewById(R.id.tv_rank);
            tvPoint = itemView.findViewById(R.id.tv_points);
            tvPrize = itemView.findViewById(R.id.tv_price);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, TeamDetailActivity.class));
                }
            });
        }
    }
}
