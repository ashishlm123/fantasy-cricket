package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emts.fantasycredit.activity.JoinNowActivity;
import com.emts.fantasycredit.model.Game;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.activity.LeagueDetailActivity;

import java.util.ArrayList;

/**
 * Created by Admin on 3/22/2018.
 */

public class GameAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<Game> gameList;
    private boolean isMyLeague = false;
    private boolean isUpComing = false;

    public GameAdapter(Context context, ArrayList<Game> gameList) {
        this.context = context;
        this.gameList = gameList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_private_leagues, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return gameList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout privateLeagueHolder, myLeagueHolder, myLeagueTitleHolder, upComingHolder,
                upComingTitleHolder, joinNow;
        TextView privateLeagueTitle, tvTeam1, tvTeam2, leagueTitle, upcomingTitle;
        ImageView imgTeam1, imgTeam2;

        public ViewHolder(View itemView) {
            super(itemView);
            privateLeagueHolder = itemView.findViewById(R.id.pvt_league_holder);
            myLeagueHolder = itemView.findViewById(R.id.my_league_holder);
            myLeagueTitleHolder = itemView.findViewById(R.id.league_title);
            privateLeagueTitle = itemView.findViewById(R.id.game_title);
            upComingHolder = itemView.findViewById(R.id.upcoming_league_holder);
            upComingTitleHolder = itemView.findViewById(R.id.upcoming_league_title);
            joinNow = itemView.findViewById(R.id.lay_join);
            imgTeam1 = itemView.findViewById(R.id.img_team1);
            imgTeam2 = itemView.findViewById(R.id.img_team2);
            tvTeam1 = itemView.findViewById(R.id.tv_team1);
            tvTeam2 = itemView.findViewById(R.id.tv_team2);
            leagueTitle = myLeagueHolder.findViewById(R.id.my_league_title);
            upcomingTitle = upComingHolder.findViewById(R.id.upcoming_league_title);


            if (isMyLeague) {
                myLeagueHolder.setVisibility(View.VISIBLE);
                myLeagueTitleHolder.setVisibility(View.VISIBLE);
                privateLeagueTitle.setVisibility(View.GONE);
                privateLeagueHolder.setVisibility(View.GONE);
                upComingHolder.setVisibility(View.GONE);
                upComingTitleHolder.setVisibility(View.GONE);
            } else if (isUpComing) {
                myLeagueHolder.setVisibility(View.VISIBLE);
                myLeagueTitleHolder.setVisibility(View.VISIBLE);
                privateLeagueTitle.setVisibility(View.GONE);
                privateLeagueHolder.setVisibility(View.GONE);
                upComingHolder.setVisibility(View.GONE);
                upComingTitleHolder.setVisibility(View.GONE);
//                myLeagueHolder.setVisibility(View.GONE);
//                myLeagueTitleHolder.setVisibility(View.GONE);
//                privateLeagueTitle.setVisibility(View.GONE);
//                privateLeagueHolder.setVisibility(View.GONE);
//                upComingHolder.setVisibility(View.VISIBLE);
//                upComingTitleHolder.setVisibility(View.VISIBLE);
            } else {
                myLeagueHolder.setVisibility(View.GONE);
                myLeagueTitleHolder.setVisibility(View.GONE);
                privateLeagueTitle.setVisibility(View.VISIBLE);
                privateLeagueHolder.setVisibility(View.VISIBLE);
                upComingHolder.setVisibility(View.GONE);
                upComingTitleHolder.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, LeagueDetailActivity.class));
                }
            });

            joinNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, JoinNowActivity.class));
                }
            });

        }


    }


    public void setMyLeague(boolean isMyLeague) {
        this.isMyLeague = isMyLeague;
    }

    public void setUpComing(boolean isUpComing) {
        this.isUpComing = isUpComing;
    }
}
