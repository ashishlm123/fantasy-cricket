package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emts.fantasycredit.R;
import com.emts.fantasycredit.model.PlayerModel;

import java.util.ArrayList;

public class PlayerStatisticsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<PlayerModel> playerStatisticsLists;

    public PlayerStatisticsAdapter(Context context, ArrayList<PlayerModel> playerStatisticsLists) {
        this.context = context;
        this.playerStatisticsLists = playerStatisticsLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_each_player_match_statistics, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return playerStatisticsLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
