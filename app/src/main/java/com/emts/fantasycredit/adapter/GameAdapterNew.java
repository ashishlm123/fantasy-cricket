package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emts.fantasycredit.R;
import com.emts.fantasycredit.activity.LeagueListsActivity;
import com.emts.fantasycredit.activity.TeamDetailActivity;
import com.emts.fantasycredit.helper.DateUtils;
import com.emts.fantasycredit.model.Game;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Admin on 3/22/2018.
 */

public class GameAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<Game> gameList;
    private boolean isLive = false;
    private boolean isFinished = false;
    private SimpleDateFormat sdf1;
    private long serverTime;

    public void setServerTime(long serverTime) {
        this.serverTime = serverTime;
    }


    public GameAdapterNew(Context context, ArrayList<Game> gameList) {
        this.context = context;
        this.gameList = gameList;
        sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_game_new, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Game gameMdl = gameList.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;

        viewHolder.team1Name.setText(gameMdl.getTeam1ShortCode().toUpperCase());
        viewHolder.team2Name.setText(gameMdl.getTeam2ShortCode().toUpperCase());
        viewHolder.leagueName.setText(gameMdl.getLeagueName());
        viewHolder.leagueDate.setText(DateUtils.convertDate(gameMdl.getLeagueDate()));
        Picasso.with(context).load(gameMdl.getTeam1Logo()).into(viewHolder.team1Logo);
        Picasso.with(context).load(gameMdl.getTeam2Logo()).into(viewHolder.team2Logo);

        if (isLive || isFinished) {
            viewHolder.btnHolder.setVisibility(View.GONE);
        }

        showRemainingTime(getTime(gameMdl.getLeagueDate()), viewHolder.tvDay);
    }

    private String showRemainingTime(long endDate, TextView tvDay) {
        if (endDate == 0) {
            return "end date not available";
        }

        if (serverTime >= endDate) {
            return "Game is Closed";
        }
        long elapsedTime = Math.abs(endDate - serverTime);

        long days, hours, minute, second;
        days = TimeUnit.MILLISECONDS.toDays(elapsedTime);
        elapsedTime -= TimeUnit.DAYS.toMillis(days);
        hours = TimeUnit.MILLISECONDS.toHours(elapsedTime);
        elapsedTime -= TimeUnit.HOURS.toMillis(hours);
        minute = TimeUnit.MILLISECONDS.toMinutes(elapsedTime);
        elapsedTime -= TimeUnit.MINUTES.toMillis(minute);
        second = TimeUnit.MILLISECONDS.toSeconds(elapsedTime);
        boolean last5minute = days == 0 && hours == 0 && minute < 5;

//        SpannableString text = new SpannableString("  " + (int) days + "day" + " : " + String.format("%02d", (int) hours) + "h"
//                + " : " + String.format("%02d", (int) minute) + "m" + " : "
//                + String.format("%02d", (int) second) + "s");

//        text.setSpan(new ImageSpan(context, R.drawable.ic_item_timer), 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
//        tvDay.setText(text);

        tvDay.setText((int) days + "day" + " : " + String.format("%02d", (int) hours) + "h"
                + " : " + String.format("%02d", (int) minute) + "m" + " : "
                + String.format("%02d", (int) second) + "s");

        return "";
    }

    private long getTime(String endDate) {
        try {
            Date endTime = sdf1.parse(endDate);
            return endTime.getTime();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public int getItemCount() {
        return gameList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView team1Name, team2Name, leagueDate, leagueName, tvDay;
        ImageView team1Logo, team2Logo;
        LinearLayout btnHolder;
        Button btnCash, btnFree;

        ViewHolder(View itemView) {
            super(itemView);
            team1Name = itemView.findViewById(R.id.team1_name);
            team2Name = itemView.findViewById(R.id.team2_name);
            tvDay = itemView.findViewById(R.id.tv_day);
            team1Logo = itemView.findViewById(R.id.team1_logo);
            team2Logo = itemView.findViewById(R.id.team2_logo);
            leagueName = itemView.findViewById(R.id.league_name);
            leagueDate = itemView.findViewById(R.id.league_date);
            btnHolder = itemView.findViewById(R.id.holder_btn);
            btnCash = itemView.findViewById(R.id.btn_cash_contest);
            btnFree = itemView.findViewById(R.id.btn_free_contest);
            if (isLive) {
                //TODO isLive
            } else if (isFinished) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, TeamDetailActivity.class);
                        intent.putExtra("finishedLeague", gameList.get(getLayoutPosition()));
                        context.startActivity(intent);
                    }
                });

            } else {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, LeagueListsActivity.class);
                        intent.putExtra("league", gameList.get(getLayoutPosition()));
                        context.startActivity(intent);
                    }
                });
            }

            btnCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, LeagueListsActivity.class);
                    intent.putExtra("league", gameList.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });
            btnFree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, LeagueListsActivity.class);
                    intent.putExtra("league", gameList.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });
        }

    }

    public void setLive(boolean isLive) {
        this.isLive = isLive;
    }

    public void setFinished(boolean isFinished) {
        this.isFinished = isFinished;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
