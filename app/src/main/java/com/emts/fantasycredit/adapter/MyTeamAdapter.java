package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emts.fantasycredit.R;
import com.emts.fantasycredit.model.Team;

import java.util.ArrayList;

public class MyTeamAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<Team> myTeams;

    private int prevSelection = -1;

    private boolean myTeamButtonClicked;

    private TeamSelectListener teamSelectListener;


    public MyTeamAdapter(Context context, ArrayList<Team> myTeams) {
        this.context = context;
        this.myTeams = myTeams;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_my_team, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final Team team = myTeams.get(position);
        final ViewHolder viewHolder = (ViewHolder) holder;

        if (team.isTeamSelected()) {
            viewHolder.wholeTeam.setBackgroundResource(R.drawable.bg_transparent_rect_color_accent1);
            viewHolder.checkTeam.setChecked(true);
            prevSelection = position;
        } else {
            viewHolder.checkTeam.setChecked(false);
            viewHolder.wholeTeam.setBackgroundResource(0);
        }

        viewHolder.checkTeam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                teamSelectListener.isTeamSelected(position, true);
                if (b) {
                    team.setTeamSelected(true);
                    viewHolder.wholeTeam.setBackgroundResource(R.drawable.bg_transparent_rect_color_accent1);
                    if (prevSelection >= 0) {
                        myTeams.get(prevSelection).setTeamSelected(false);
                        notifyItemChanged(prevSelection);
                    }
                    prevSelection = position;
                } else {
                    team.setTeamSelected(false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return myTeams.size();
    }

    public void setMyTeamButtonClicked(boolean myTeamButtonClicked) {
        this.myTeamButtonClicked = myTeamButtonClicked;
    }

    public void setTeamSelectListener(TeamSelectListener teamSelectListener) {
        this.teamSelectListener = teamSelectListener;
    }

    public interface TeamSelectListener {
        void isTeamSelected(int pos, boolean isSelected);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout wholeTeam;
        RelativeLayout captainHolder, viceCaptainHolder;
        CheckBox checkTeam;
        TextView tvTeamName, tvCaptainName, tvCaptain, tvVCapName, tvVC, tvWcktKeeper, tvBatsMen, tvAllRounder, tvBowler;
        ImageView edtTeam, viewTeam, copyTeam;


        public ViewHolder(View itemView) {
            super(itemView);
            wholeTeam = itemView.findViewById(R.id.whole_team);
            checkTeam = itemView.findViewById(R.id.cb_team);
            if (myTeamButtonClicked) {
                checkTeam.setVisibility(View.GONE);
            } else {
                checkTeam.setVisibility(View.VISIBLE);
            }
//            checkTeam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                    if (b) {
//                        myTeams.get(getLayoutPosition()).setTeamSelected(true);
//                    } else {
//                        myTeams.get(getLayoutPosition()).setTeamSelected(false);
//                    }
//                    notifyItemChanged(getLayoutPosition());
//                }
//            });

            tvTeamName = itemView.findViewById(R.id.tv_team_name);

            captainHolder = itemView.findViewById(R.id.captain_holder);
            tvCaptainName = captainHolder.findViewById(R.id.tv_player_name);
            tvCaptain = captainHolder.findViewById(R.id.tv_player_type);

            viceCaptainHolder = itemView.findViewById(R.id.vc_holder);
            tvVCapName = viceCaptainHolder.findViewById(R.id.tv_player_name);
            tvVC = viceCaptainHolder.findViewById(R.id.tv_player_type);

            tvWcktKeeper = itemView.findViewById(R.id.tv_wckt_keeper);
            tvBatsMen = itemView.findViewById(R.id.tv_bats_men);
            tvAllRounder = itemView.findViewById(R.id.tv_all_rounder);
            tvBowler = itemView.findViewById(R.id.tv_baller);

            edtTeam = itemView.findViewById(R.id.edt_team);
            viewTeam = itemView.findViewById(R.id.view_team);
            copyTeam = itemView.findViewById(R.id.copy_team);
        }
    }
}
