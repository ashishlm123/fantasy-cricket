package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.fantasycredit.R;
import com.emts.fantasycredit.model.TransactionModel;

import java.util.ArrayList;

/**
 * Created by User on 2018-03-23.
 */

public class AccountTransactionHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<TransactionModel> transactionlists;
    private LayoutInflater inflater;

    public AccountTransactionHistoryAdapter(Context context, ArrayList<TransactionModel> transactionlists) {
        this.context = context;
        this.transactionlists = transactionlists;

        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.item_account_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        TransactionModel transactionModel = transactionlists.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.tvAmt.setText(transactionModel.getTransAmt());
        viewHolder.tvBal.setText(transactionModel.getUserBalance());
        viewHolder.tvLeague.setText(transactionModel.getTransLeagueName());
        viewHolder.tvTime.setText(transactionModel.getTransTime());
        viewHolder.tvType.setText(transactionModel.getTransTypeText());

        if (position % 2 == 0) {
            viewHolder.itemView.setBackgroundColor(Color.WHITE);
        } else {
            viewHolder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_gray));
        }

    }

    @Override
    public int getItemCount() {
        return transactionlists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvAmt, tvLeague, tvTime, tvType, tvBal;

        ViewHolder(View itemView) {
            super(itemView);

            tvAmt = itemView.findViewById(R.id.tvAmt);
            tvLeague = itemView.findViewById(R.id.tvLeague);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvType = itemView.findViewById(R.id.tvType);
            tvBal = itemView.findViewById(R.id.tvBal);

        }
    }
}
