package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.emts.fantasycredit.R;
import com.emts.fantasycredit.activity.CreateTeamActivity;
import com.emts.fantasycredit.model.LeagueModel;

import java.util.ArrayList;

public class LeaguesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<LeagueModel> leaguesLists;

    public LeaguesAdapter(Context context, ArrayList<LeagueModel> leaguesLists) {
        this.context = context;
        this.leaguesLists = leaguesLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_league_requirements, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        LeagueModel leagueModel = leaguesLists.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.tvWining.setText("Rs " + leagueModel.getTotalWinning());
        viewHolder.tvEntryFee.setText("Rs " + leagueModel.getEntryFee());
//        viewHolder.tvGainer.setText(leagueModel.getGainers());
        viewHolder.tvTotal.setText("Total " + leagueModel.getLeagueSize() + " Teams");
        viewHolder.tvRemainingSlots.setText("Only " +
                String.valueOf(Integer.valueOf(leagueModel.getLeagueSize()) - Integer.valueOf(leagueModel.getLeagueMember())) +
                " slots left");
    }

    @Override
    public int getItemCount() {
        return leaguesLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvWining, tvEntryFee, tvTotal, tvRemainingSlots;
        Button btnJoin;

        public ViewHolder(View itemView) {
            super(itemView);
            tvWining = itemView.findViewById(R.id.tv_total_winning);
            tvEntryFee = itemView.findViewById(R.id.tv_entryfee);
            tvTotal = itemView.findViewById(R.id.total_member);
            tvRemainingSlots = itemView.findViewById(R.id.remaining_slot);
            btnJoin = itemView.findViewById(R.id.btn_join_league);

            btnJoin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        listener.onRecyclerItemClick(view, getLayoutPosition());
                    }
                }
            });
        }
    }

    public interface RecyclerItemClickListener{
        void onRecyclerItemClick(View view, int position);
    }

    private RecyclerItemClickListener listener;

    public void setRecyclerItemClickListener(RecyclerItemClickListener listener) {
        this.listener = listener;
    }
}
