package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.fantasycredit.R;
import com.emts.fantasycredit.helper.Config;
import com.emts.fantasycredit.helper.Logger;
import com.emts.fantasycredit.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 3/26/2018.
 */

public class ReferralAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<User> referralList;
    public Context context;

    public ReferralAdapter(ArrayList<User> referralList, Context context) {
        this.referralList = referralList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_referral, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        User userMdl = referralList.get(position);
        viewHolder.tvUserName.setText(userMdl.getUserName());
        viewHolder.tvEmail.setText(userMdl.getUserEmail());
        viewHolder.tvStatus.setText(userMdl.getStatus());
        if (userMdl.getStatus().equals(Config.VERIFIED)) {
            viewHolder.statusImage.setImageResource(R.drawable.ic_check_circle_black_24dp);
        } else if (userMdl.getStatus().equals(Config.UNVERIFIED)) {
            viewHolder.statusImage.setImageResource(R.drawable.ic_cancel_black_24dp);
        }
        if (!TextUtils.isEmpty(userMdl.getUserImage())) {
            Picasso.with(context).load(userMdl.getUserImage()).into(viewHolder.userImage);
        }

    }

    @Override
    public int getItemCount() {
        return referralList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView userImage, statusImage;
        TextView tvUserName, tvEmail, tvStatus;

        public ViewHolder(View itemView) {
            super(itemView);

            userImage = itemView.findViewById(R.id.user_image);
            statusImage = itemView.findViewById(R.id.img_status);
            tvUserName = itemView.findViewById(R.id.user_name);
            tvEmail = itemView.findViewById(R.id.user_email);
            tvStatus = itemView.findViewById(R.id.tv_status);
        }
    }
}
