package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.fantasycredit.R;

import java.util.ArrayList;

/**
 * Created by User on 2018-03-22.
 */

public class TeamCardSwipeAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Object> gameList;

    public TeamCardSwipeAdapter(Context context, ArrayList<Object> gameList) {
        this.context = context;
        this.gameList = gameList;
    }

    @Override
    public int getCount() {
        return gameList.size();
    }

    @Override
    public Object getItem(int i) {
        return gameList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View contentView, ViewGroup viewGroup) {
        ViewHolder holder;

        if (contentView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            contentView = inflater.inflate(R.layout.item_game_swipe_card, viewGroup, false);
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }

//        TouristSpot spot = getItem(position);
//
//        holder.name.setText(spot.name);
//        holder.city.setText(spot.city);
//        Glide.with(getContext()).load(spot.url).into(holder.image);

        return contentView;
    }


    private static class ViewHolder {
        ImageView imgTeam1, imgTeam2;
        TextView tvTeam1, tvTeam2, tvfirstTeam, tvsecondTeam;

        public ViewHolder(View view) {
            imgTeam1 = view.findViewById(R.id.flag_team1);
            imgTeam2 = view.findViewById(R.id.flag_team2);
            tvTeam1 = view.findViewById(R.id.tvNameTeam1);
            tvTeam2 = view.findViewById(R.id.tvNameTeam2);
            tvfirstTeam = view.findViewById(R.id.tv_firstteam);
            tvsecondTeam = view.findViewById(R.id.tv_secondteam);
        }
    }
}
