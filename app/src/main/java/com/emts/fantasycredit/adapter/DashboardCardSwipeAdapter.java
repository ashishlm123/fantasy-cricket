package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.emts.fantasycredit.R;

import java.util.ArrayList;

/**
 * Created by Admin on 3/26/2018.
 */

public class DashboardCardSwipeAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Object> gameList;

    public DashboardCardSwipeAdapter(Context context, ArrayList<Object> gameList) {
        this.context = context;
        this.gameList = gameList;
    }

    @Override
    public int getCount() {
        return gameList.size();
    }

    @Override
    public Object getItem(int i) {
        return gameList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View contentView, ViewGroup viewGroup) {
        ViewHolder holder;

        if (contentView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            contentView = inflater.inflate(R.layout.item_dashboard_swipe_card, viewGroup, false);
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }

//        TouristSpot spot = getItem(position);
//
//        holder.name.setText(spot.name);
//        holder.city.setText(spot.city);
//        Glide.with(getContext()).load(spot.url).into(holder.image);

        return contentView;
    }


    private static class ViewHolder {

        public ViewHolder(View view) {
        }
    }
}
