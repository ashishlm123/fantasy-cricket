package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.model.PlayerModel;

import java.util.ArrayList;

/**
 * Created by Admin on 3/23/2018.
 */

public class PlayerDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public Context context;
    private ArrayList<PlayerModel> playerList;
    private boolean isCreateTeam = false;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;

    public PlayerDetailAdapter(Context context, ArrayList<PlayerModel> playerList) {
        this.context = context;
        this.playerList = playerList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_teamplayer_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        PlayerModel player = playerList.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;
        if (position % 2 == 0) {
            viewHolder.itemView.setBackgroundColor(Color.WHITE);
        } else {
            viewHolder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_grey1));
        }

        if (!TextUtils.isEmpty(player.getImage())) {
            Glide.with(context).load(player.getImage()).into(viewHolder.playerImage);
        }
        viewHolder.tvPlayerName.setText(player.getPlayerName());
        viewHolder.playerType.setPadding(0, 0, 0, 0);
        if (player.getPlayerType().equalsIgnoreCase(PlayerModel.WICKET_KEEPER)) {
            viewHolder.playerType.setImageResource(R.drawable.wicket);
        } else if (player.getPlayerType().equalsIgnoreCase(PlayerModel.ALL_ROUNDER)) {
            viewHolder.playerType.setImageResource(R.drawable.batnball);
        } else if (player.getPlayerType().equalsIgnoreCase(PlayerModel.BATSMAN)) {
            viewHolder.playerType.setImageResource(R.drawable.bat);
        } else if (player.getPlayerType().equalsIgnoreCase(PlayerModel.BOWLER)) {
            viewHolder.playerType.setImageResource(R.drawable.ball);
            viewHolder.playerType.setPadding(12, 12, 12, 12);
        }
        viewHolder.tvPlayerPoints.setText(player.getFantasyPoints());
        if (player.getIsSelected()) {
            viewHolder.imgStatus.setImageResource(R.drawable.ic_cancel_black_24dp);
        } else {
            viewHolder.imgStatus.setImageResource(R.drawable.ic_add_circle_black_24dp);
        }
        viewHolder.tvPlayerTeam.setText(player.getPlayingTeam());
    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView playerImage, playerType, imgStatus;
        TextView tvPlayerName, tvPlayerTeam, tvPlayerPoints;

        public ViewHolder(final View itemView) {
            super(itemView);
            playerImage = itemView.findViewById(R.id.player_image);
            playerType = itemView.findViewById(R.id.img_player_type);
            tvPlayerName = itemView.findViewById(R.id.tv_player_name);
            tvPlayerTeam = itemView.findViewById(R.id.tv_team);
            tvPlayerPoints = itemView.findViewById(R.id.tv_point);
            imgStatus = itemView.findViewById(R.id.img_add);

            playerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerViewItemClickListener.onRecyclerViewItemClickListener(view, getLayoutPosition());
                }
            });

            tvPlayerName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerViewItemClickListener.onRecyclerViewItemClickListener(view, getLayoutPosition());
                }
            });

            if (isCreateTeam) {
                imgStatus.setVisibility(View.VISIBLE);
                imgStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        boolean isSelected = playerList.get(getLayoutPosition()).getIsSelected();
//                        playerList.get(getLayoutPosition()).setIsSelected(!isSelected);
//                        notifyItemChanged(getLayoutPosition());
                        recyclerViewItemClickListener.onRecyclerViewItemClickListener(view, getLayoutPosition());
                    }
                });
            } else {
                imgStatus.setVisibility(View.GONE);
            }

        }
    }

    public void setCreateTeam(boolean isCreateTeam) {
        this.isCreateTeam = isCreateTeam;
    }

    public interface RecyclerViewItemClickListener {
        void onRecyclerViewItemClickListener(View view, int position);
    }

    public void setRecyclerViewItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
    }

}