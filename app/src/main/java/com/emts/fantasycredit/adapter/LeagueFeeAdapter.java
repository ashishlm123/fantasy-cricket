package com.emts.fantasycredit.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.fantasycredit.model.CheckableItem;
import com.emts.fantasycredit.R;
import com.emts.fantasycredit.helper.Logger;

import java.util.ArrayList;

/**
 * Created by User on 2018-03-22.
 */

public class LeagueFeeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<CheckableItem> checkableItems;
    private LayoutInflater inflater;

    private int defTextColor;

    public LeagueFeeAdapter(Context context, ArrayList<CheckableItem> checkableItems) {
        this.context = context;
        this.checkableItems = checkableItems;
        inflater = LayoutInflater.from(context);
        Logger.e("List size", "-> " + checkableItems.size());

        defTextColor = new TextView(context).getCurrentTextColor();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.item_fee, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CheckableItem item = checkableItems.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;

        if (item.isChecked()) {
            viewHolder.itemView.setBackgroundResource(R.drawable.rounded_bg_colorprimary);
            viewHolder.tvTitle.setTextColor(Color.WHITE);
        } else {
            viewHolder.itemView.setBackgroundResource(R.drawable.rounded_white_gray_strok);
            viewHolder.tvTitle.setTextColor(defTextColor);
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;

        ViewHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvText);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (int i = 0; i < checkableItems.size(); i++) {
                        if (i == getLayoutPosition()) {
                            checkableItems.get(i).setChecked(true);
                        } else {
                            checkableItems.get(i).setChecked(false);
                        }
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }
}
